from tofhed import hinuma
from tofhed import utils
import csv
import numpy as np
import pandas as pd
from MDAnalysis.lib import distances as mda_distances
import mendeleev
import collections
from itertools import product, combinations
from collections import defaultdict
from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from ase import Atoms
from ase.io import read
from ase.visualize.plot import plot_atoms
from matplotlib.gridspec import GridSpec
from matplotlib import colormaps
from matplotlib.colors import ListedColormap


def matplotlib_debug_viz(experiment, frame=0, rotation='90x, 0y, 0z', cut=False, figsize=(10, 8), dpi=150):
    # Loading path of the xyz file
    path_xyz = experiment.system.path
    atoms = read(path_xyz, index=frame)

    # Generate the color list based on the tags of the atoms
    color_tags = [0 if experiment.base_types_dict[atom_number] == 'catalyst' else 1 for atom_number in experiment.atoms.ids_sorted_by_number()]
    
    color_legend = ['Catalyst Atoms', 'Chemical Atoms']

    # Selecting latest determined surfaces
    if experiment.surface_groups:
        surfaces = experiment.surface_groups.latest

        # Assigning tags for surfaces
        for i, surface in enumerate(surfaces):
            color_legend.append(f'Surface {i+1} Atoms')
            for a_id in surface.atom_ids:
                atom_number = experiment.atoms.id_to_number[a_id]
                color_tags[atom_number] = i + 2

    number_of_colors = len(set(color_tags))

    # Create a colormap
    cmap_base = colormaps.get_cmap('Spectral')
    cmap = ListedColormap(cmap_base(np.linspace(0, 1, number_of_colors)))

    # Create a dictionary of colors
    color_dict = {i: cmap(i) for i in range(number_of_colors)}

    # Assigning colors to tags
    colors = [color_dict[tag] for tag in color_tags]

    fig = plt.figure(figsize=figsize, dpi=dpi)
    gs = GridSpec(1, 2, width_ratios=[1, 0.05])  # 1 row, 2 columns

    ax = fig.add_subplot(gs[0])  # First column for atoms visualization

    if cut:
        # Cut the atoms in half
        positions = atoms.get_positions()
        center = (positions.max(axis=0) - positions.min(axis=0)) / 2
        atom_indexes = []

        for i, position in enumerate(positions):
            if position[1] > center[1]:
                atom_indexes.append(i)

        for i in sorted(atom_indexes, reverse=True):
            del atoms[i]
            del colors[i]

    plot_atoms(atoms, rotation=rotation, colors=colors, ax=ax)
    ax.axis('off')

    legend_elements = [plt.Line2D([0], [0], marker='o', color='w', label=label, markerfacecolor=color, markersize=10) for label, color in zip(color_legend, color_dict.values())]
    ax_legend = fig.add_subplot(gs[1])
    ax_legend.axis('off')
    ax_legend.legend(handles=legend_elements, loc='center', fontsize='small')

    plt.tight_layout()
    plt.show()


def matplotlib_hinuma_viz(experiment, hinuma_id=None, frame=0, rotation='90x, 0y, 0z', color_range=(0, 4*np.pi), cut=False, 
                          centering=False, figsize=(10, 8), dpi=150, plot_histogram=False, n_bins=16, solid_angles=None):

    
    # Loading path of the xyz file
    path_xyz = experiment.system.path
    atoms = read(path_xyz, index=frame)
    
    if not hinuma_id is None:
        # Get the solid angles
        solid_angles = experiment.hinumas[hinuma_id].solid_angles
    else:
        if solid_angles is None:
            raise ValueError('solid_angles or hinuma_id must be provided.')

    # Convert atom IDs to represent the index of the positions, and filter out None values
    solid_angles_n = {experiment.atoms.id_to_number[n]: solid_angle for n, solid_angle in solid_angles.items() if solid_angle is not None}
    
    # Identify indices of atoms that are not in solid_angles_n dictionary
    atoms_to_remove = [i for i in range(len(atoms)) if i not in solid_angles_n]
    
    # Normalize solid angles for color mapping based on the provided color_range
    min_angle, max_angle = color_range
    norm = plt.Normalize(min_angle, max_angle)
    
    # Create a colormap
    cmap = colormaps['Spectral']
    
    # Assign colors based on normalized solid_angles values for all atoms
    colors = [cmap(norm(solid_angles_n.get(i, min_angle))) for i in range(len(atoms))]

    # Delete atoms not in solid_angles_n and atoms with None value for solid angles
    for i in sorted(atoms_to_remove, reverse=True):
        del atoms[i]
        del colors[i]
    
    if cut:
        # Cut the atoms in half
        positions = atoms.get_positions()
        center = (positions.max(axis=0) - positions.min(axis=0)) / 2
        atom_indexes = []

        for i, position in enumerate(positions):
            if position[1] > center[1]:
                atom_indexes.append(i)

        for i in sorted(atom_indexes, reverse=True):
            del atoms[i]
            del colors[i]

    if centering:
        # Centering remaining atoms
        positions = atoms.get_positions()
        center = positions.mean(axis=0)
        new_positions = positions - center
        atoms.set_positions(new_positions)

    fig = plt.figure(figsize=figsize, dpi=dpi)
    gs = GridSpec(1, 2, width_ratios=[3, 1])  # 1 row, 2 columns, each occupying equal width
    
    ax = fig.add_subplot(gs[0])  # First column for atoms visualization and colorbar
    
    plot_atoms(atoms, rotation=rotation, colors=colors, ax=ax)

    ax.axis('off')

    cbar_ticks = [i * np.pi for i in range(int(max_angle/np.pi)+1)]
    cbar = plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, orientation='vertical', ticks=cbar_ticks)
    cbar.set_label('Solid Angle Value')
    cbar.ax.set_yticklabels([f'{i}π' for i in range(len(cbar_ticks))])
    
    if plot_histogram:
        ax_hist = fig.add_subplot(gs[1])  # Second column for histogram
        
        # Create histogram data
        hist_values = list(solid_angles_n.values())
        hist_bins = np.linspace(min_angle, max_angle, n_bins)

        # Compute histogram
        counts, bin_edges = np.histogram(hist_values, bins=hist_bins)

        # Plot histogram (horizontally, but rotated 90 degrees to the right)
        for i in range(n_bins - 1):
            ax_hist.barh(bin_edges[i], counts[i], align='edge', height=np.diff(bin_edges)[0], color=cmap(norm((bin_edges[i] + bin_edges[i+1])/2)), edgecolor='black')

        ax_hist.set_xlabel('Counts')
        ax_hist.set_ylabel('Solid Angle Value')
        ax_hist.set_yticks(cbar_ticks)
        ax_hist.set_yticklabels([f'{i}π' for i in range(len(cbar_ticks))])
        ax_hist.set_ylim(min_angle, max_angle)

    plt.tight_layout()
    plt.show()


def get_top_bottom_surface_atom_ids(pos, cell, pbc, box, cat_ids, minimum=np.pi*0.75,
                                    hinuma_cutoff=3.5, xyz_filter=None, mic=True, **kwargs):
    '''
    Determines which atoms belong to the surface, splitted in top and bottom.

    Parameters
    ----------
    pos : ndarray
        list of xyz-positions
        Atomic positions.  Anything that can be converted to an
        ndarray of shape (n, 3) will do: [(x1,y1,z1), (x2,y2,z2),
        ...].
    cell : ndarray


    '''

    surfaces, r_dashs, hulls = hinuma.surface_classifier(pos[cat_ids],
                                                             cell, pbc, box,
                                                             cutoff=hinuma_cutoff,
                                                             mic=mic,
                                                             xyz_filter=xyz_filter)
    surface_ids = simple_surface_atom(surfaces, minimum=np.pi*0.75)
    surface_atom_ids = simple_surface_atom(surfaces, minimum=np.pi*0.75, atom_ids=cat_ids)

    # surface_hull_lists = create_filtered_list(hulls, surface_ids)
    # connection_list = get_connection_list(surface_hull_lists, ids=cat_ids)
    surface_r_dashs = create_filtered_list(r_dashs, surface_ids)

    top_surface_atom_ids, bottom_surface_atom_ids = top_bottom_ids_z(surface_r_dashs,
                                                                     ids=surface_atom_ids)

    return top_surface_atom_ids, bottom_surface_atom_ids


def get_surface_atom_ids_mda(pos, cell, pbc, box, cat_ids, minimum=np.pi*0.75,
                             hinuma_cutoff=3.5, xyz_filter=None, mic=True, **kwargs):
    """Get all surface atom ID's using MDAnalysis and the Hinuma algorhithm

    Parameters
    ----------
    pos : ndarray
        list of xyz-positions [[x1, y1, z1], [x2, y2, z2], ...]
    cell : ndarray
        list of cell vectors
    pbc : ndarray
        list of boolen values for periodic boundary conditions
    box : ndarray
        list of box vectors
    cat_ids : ndarray
        list of catalyst atom IDs
    minimum : float
        Minimal value for the surface angle to be considered a surface atom.
        Default is np.pi*0.75
    hinuma_cutoff : float
        Distance for hinuma to calculate the surface angle.
        Default is 3.5
    xyz_filter : ndarray
        None or 'z-filter' supported at the moment. Default is None
    mic : bool
        Apply minimum image convention. Default is True
    

    Returns
    -------
    surface_atom_ids : ndarray
        list of surface atom IDs

    """
    
    surfaces, r_dashs, hulls = hinuma.surface_classifier(pos[cat_ids],
                                                             cell, pbc, box,
                                                             cutoff=hinuma_cutoff,
                                                             mic=mic,
                                                             xyz_filter=xyz_filter)
    surface_atom_ids = simple_surface_atom(surfaces, minimum=np.pi*0.75, atom_ids=cat_ids)

    return surface_atom_ids


def get_surface_atom_ids(pos, cell, pbc, cat_ids, minimum=np.pi*0.75,
                         hinuma_cutoff=6.0, expand=[1, 1, 0], xyz_filter=None, **kwargs):

    """Get all surface atom ID's using the Hinuma algorhithm
    """
    surfaces, r_dashs, hulls, distance_cache = hinuma.surface_classifier(pos[cat_ids],
                                                                         cell, pbc,
                                                                         cutoff=hinuma_cutoff,
                                                                         expand=expand,
                                                                         xyz_filter=xyz_filter)
    surface_ids = simple_surface_atom(surfaces, minimum=np.pi*0.75)
    surface_atom_ids = simple_surface_atom(surfaces, minimum=np.pi*0.75, atom_ids=cat_ids)

    # surface_hull_lists = create_filtered_list(hulls, surface_ids) #  NOT USED ANYMORE
    # connection_list = get_connection_list(surface_hull_lists, ids=cat_ids) #  NOT USED ANYMORE
    surface_r_dashs = create_filtered_list(r_dashs, surface_ids)

    top_surface_atom_ids, bottom_surface_atom_ids = top_bottom_ids_z(surface_r_dashs,
                                                                     ids=surface_atom_ids)
    return top_surface_atom_ids, bottom_surface_atom_ids


def get_all_pair_distances_top_bottom(pos, top_surface_atom_ids,
                                      bottom_surface_atom_ids,
                                      chem_ids, approach_cutoff,
                                      bond_cutoff, box=None,
                                      approach_zones=None, **kwargs):
    """
    :param pos:
    :param top_surface_atom_ids:
    :param bottom_surface_atom_ids:
    :param chem_ids:
    :param approach_cutoff:
    :param bond_cutoff:
    :param box:
    :param approach_zones:

    :returns: Dictionary of all pair distances
    """

    approach_ids = get_approach_ids(pos, top_surface_atom_ids,
                                    bottom_surface_atom_ids, chem_ids,
                                    approach_cutoff,
                                    approach_zones=approach_zones)
    top_chem_approach_ids, bottom_chem_approach_ids = approach_ids

    top_pair_distances = get_distance_pairs(pos, top_surface_atom_ids,
                                            top_chem_approach_ids,
                                            bond_cutoff=bond_cutoff,
                                            box=box)
    bottom_pair_distances = get_distance_pairs(pos, bottom_surface_atom_ids,
                                               bottom_chem_approach_ids,
                                               bond_cutoff=bond_cutoff,
                                               box=box)

    pair_distances = {**top_pair_distances, **bottom_pair_distances}

    return pair_distances


def get_pair_distances_bond(pos, top_surface_atom_ids, bottom_surface_atom_ids,
                            chem_ids, chem_sym, approach_cutoff, bond_cutoff,
                            bond_distance, bond_fudge, box=None,
                            approach_zones=None, **kwargs):
    '''
    :param pos:
    :param top_surface_atom_ids:
    :param bottom_surface_atom_ids:
    :param chem_ids:
    :param chem_sym:
    :param approach_cutoff:
    :param bond_cutoff:
    :param bond_distance:
    :param bond_fudge:
    :param box:
    :param approach_zones:

    :returns: Dictionary of all pair distances and bond pairs
    '''
    pair_distances = get_all_pair_distances_top_bottom(pos, top_surface_atom_ids,
                                                       bottom_surface_atom_ids,
                                                       chem_ids, approach_cutoff,
                                                       bond_cutoff, box=box,
                                                       approach_zones=approach_zones)

    bond_pairs = get_bond_pairs(pair_distances, chem_sym,
                                bond_distance=bond_distance,
                                bond_fudge=bond_fudge)

    return pair_distances, bond_pairs


def get_approach_zone(pos, top_surface_atom_ids, bottom_surface_atom_ids,
                      approach_cutoff=5.0, **kwargs):
    '''
    :param pos:
    :param top_surface_atom_ids:
    :param bottom_surface_atom_ids:
    :param approach_cutoff:
    :returns: Tuple of top and bottom approach zone
    '''
    top_min_max = min_max_surface(pos, filter_ids=top_surface_atom_ids, axis='z')
    bottom_min_max = min_max_surface(pos, filter_ids=bottom_surface_atom_ids, axis='z')

    top_approach_zone, bottom_approach_zone = calc_approach_zones(top_min_max, bottom_min_max,
                                                                  cutoff=approach_cutoff)
    return top_approach_zone, bottom_approach_zone


def get_approach_ids(pos, top_surface_atom_ids, bottom_surface_atom_ids,
                     chem_ids, approach_cutoff=5.0, approach_zones=None, **kwargs):
    if approach_zones is None:
        top_approach_zone, bottom_approach_zone = get_approach_zone(pos, top_surface_atom_ids, bottom_surface_atom_ids, approach_cutoff)
    else:
        top_approach_zone, bottom_approach_zone = approach_zones
    top_limits = [None, None, top_approach_zone]
    bottom_limits = [None, None, bottom_approach_zone]

    chem_approach_top_pos, chem_approach_top_ids = hinuma.cubic_limit(pos[chem_ids],
                                                                      top_limits,
                                                                      chem_ids)
    chem_approach_bottom_pos, chem_approach_bottom_ids = hinuma.cubic_limit(pos[chem_ids],
                                                                           bottom_limits,
                                                                           chem_ids)

    return chem_approach_top_ids, chem_approach_bottom_ids


def get_distance_pairs(pos, cat_ids, chem_ids, bond_cutoff=4.0, box=None, **kwargs):
    '''
    Returns
    '''
    pairs, dist = mda_distances.capped_distance(pos[cat_ids],
                                                pos[chem_ids],
                                                max_cutoff=bond_cutoff,
                                                min_cutoff=None,
                                                box=box)
    pair_ids = convert_2d_ids(pairs, cat_ids, chem_ids)
    pair_distance = create_pair_distance(pair_ids, dist)

    return pair_distance


def get_distance_pairs_sorted(pos, cat_ids, chem_ids,
                              box=None, bond_cutoff=4.0, **kwargs):
    pair_distance = get_distance_pairs(pos, cat_ids, chem_ids, box=box,
                                       bond_cutoff=bond_cutoff)
    return sort_dict_2d_lists(pair_distance)


def get_closest_pairs(pos, cat_ids, chem_ids, bond_cutoff=4.0, box=None, **kwargs):
    """
    Returns a dictionary: dict[chem_atom_id] = (cat_id, distance_float)
    Dictionary key: chemical atom ID
    Dictionary value: Tuple containing the closest catalyst atom ID and its distance
    """
    pair_distances = get_distance_pairs(pos, cat_ids, chem_ids,
                                        bond_cutoff=bond_cutoff, box=box)
    closest_dist_dict = {}
    for key, dist_list in pair_distances.items():
        closest_i = np.array(dist_list).T[1].argmin()
        closest_dist_dict[key] = dist_list[closest_i]
    return closest_dist_dict


def get_bond_pairs(pair_distances, chem_sym, bond_distance='auto', bond_fudge=1.0, **kwargs):
    """
    Returns a dictionary: dict[chem_atom_id] = (cat_id, distance_float)
    Dictionary key: chemical atom ID
    Dictionary value: Tuple containing the closest catalyst atom ID and its distance
    """
    if bond_distance == 'auto':
        bond_distance = get_bond_distance_library()

    elif isinstance(bond_distance, (int, float)):
        closest_dist_dict = {}
        for key, dist_list in pair_distances.items():
            closest_i = np.array(dist_list).T[1].argmin()
            closest_id, dist = dist_list[closest_i]
            if bond_distance*bond_fudge >= dist:
                closest_dist_dict[key] = (closest_id, dist)
        return closest_dist_dict

    elif not isinstance(bond_distance, (BondDistances, PyykkoDistances)):
        raise TypeError(f'Expected int, float, BondDistances or PyykkoDistances but got {type(bond_distance)}')

    closest_dist_dict = {}
    for key, dist_list in pair_distances.items():
        closest_i = np.array(dist_list).T[1].argmin()
        closest_id, dist = dist_list[closest_i]
        e1, e2 = chem_sym[key], chem_sym[closest_id]
        temp_bond_dist = bond_distance.get_distance(e1, e2)
        if temp_bond_dist is None:
            raise ValueError('No value in BondDistances for {e1, e2}.')
        if temp_bond_dist*bond_fudge >= dist:
            closest_dist_dict[key] = (closest_id, dist)
    return closest_dist_dict


def get_chemical_symbols_set(atoms):
    return set(atoms.get_chemical_symbols())


def get_atomic_numbers(atoms):
    chemical_symbols = atoms.get_chemical_symbols()
    return [mendeleev.element(chem_sym).atomic_number for chem_sym in chemical_symbols]


def create_pair_distance(top_pair_ids, top_dist):
    chem_cat_dist = collections.defaultdict(list)
    for ((i, j), dist) in zip(top_pair_ids, top_dist):
        chem_cat_dist[j].append((i, dist))
    return chem_cat_dist


def convert_2d_ids(_list, ids_1, ids_2):
    """
    Converts a list of pairs into a list of pairs with the original ids
    """
    new_ids = []
    for i, j in _list:
        new_ids.append([ids_1[i], ids_2[j]])
    return new_ids


def sort_2d_list(list_to_sort, by_column=1):
    return sorted(list_to_sort, key=lambda l: l[by_column])


def sort_dict_2d_lists(pair_dict, by_column=1):
    new_pair_dict = {}
    for key, _list in pair_dict.items():
        new_pair_dict[key] = sort_2d_list(_list, by_column=by_column)
    return new_pair_dict


def generate_corner_points():
    return np.array(list(product(*zip([0, 0, 0], [1, 1, 1]))))


def calc_cell_corners(cell):
    return hinuma.generate_corner_points()@cell.T


def calc_approach_zones(top_min_max, bottom_min_max, cutoff=7.0):
    top_approach_zone = calc_approach_zone_top(top_min_max, cutoff=cutoff)
    bottom_approach_zone = calc_approach_zone_bottom(bottom_min_max, cutoff=cutoff)
    return top_approach_zone, bottom_approach_zone


def calc_approach_zone_top(top_min_max, cutoff=7.0):
    if top_min_max is None:
        return None
    top_min, top_max = top_min_max
    return (np.floor(top_min), np.ceil(top_max+cutoff))


def calc_approach_zone_bottom(bottom_min_max, cutoff=7.0):
    if bottom_min_max is None:
        return None
    bottom_min, bottom_max = bottom_min_max
    return (np.floor(bottom_min-cutoff), np.ceil(bottom_max))


def min_max_surface(pos, filter_ids=None, axis='z'):
    if filter_ids is not None and not filter_ids:
        return None

    if axis == 'x':
        axis = 0
    elif axis == 'y':
        axis = 1
    elif axis == 'z':
        axis = 2

    if filter_ids is None:
        filtered_pos = np.array(pos.copy())
    else:
        filtered_pos = np.array([pos[i] for i in filter_ids])

    values = np.reshape(filtered_pos.T, (3, len(filtered_pos)))[axis]
    return np.min(values), np.max(values)


def min_max_surface_z(pos, filter_ids=None):
    return min_max_surface(pos, filter_ids, axis='z')


def get_surface_vec(r_dashs, filter_ids=None):
    if filter_ids is None:
        filtered_r_dashs = r_dashs.copy()
    else:
        filtered_r_dashs = create_filtered_list(r_dashs, filter_ids)

    r_dashs_sum = -np.sum(filtered_r_dashs, axis=0)
    print(len(r_dashs_sum), r_dashs_sum)
    r_dashs_sum_norm = hinuma.fast_2_norm(r_dashs_sum)
    return r_dashs_sum/r_dashs_sum_norm


def top_bottom_ids_z(r_dashs, ids=None):
    top, bottom = [], []
    if ids is None:
        ids = list(range(len(r_dashs)))
    for i, (x, y, z) in enumerate(r_dashs):
        if z > 0:
            top.append(ids[i])
        else:
            bottom.append(ids[i])
    return (top, bottom)


def convert_list_ids(_list, ids):
    new_ids = []
    for i in _list:
        if isinstance(i, int) or isinstance(i, np.int64) or isinstance(i, np.int32):
            new_ids.append(ids[i])
        elif isinstance(i, list) or isinstance(i, np.ndarray):
            new_ids.append(convert_list_ids(i, ids))

        else:
            print(f'Type error! {type(i)} is not supported.')
    return new_ids


def get_connection_list(hull_lists, ids=None):
    """
    Returns a list with all connecting atoms

    """
    connection_list = []
    if ids is None:
        ids = list(range(len(hull_lists)))

    for hulls in hull_lists:
        connecting_atoms = []
        for con in hulls:
            for x in con:
                atom_id = ids[x]
                if atom_id not in connecting_atoms:
                    connecting_atoms.append(atom_id)
        connection_list.append(connecting_atoms)
    return connection_list


def create_filtered_list(list_to_filter, ids):
    """
    Returns the the input list filtered by the ids.
    The input 'ids' has to be a list.
    """
    return [list_to_filter[i] for i in ids]


def split_by_tag(tags, chemical_tag=2):
    '''
    Splits the input list by its values.
    Returns two lists containing the corresponding index of the input list.
    '''
    chemical_tag = np.array(chemical_tag)

    chem_ids, cat_ids = [], []
    for i, tag in enumerate(tags):
        if tag in chemical_tag:
            chem_ids.append(i)
        else:
            cat_ids.append(i)

    return chem_ids, cat_ids


def split_by_chemicals_list(elements, filter_elements):
    '''
    Splits the input data by chemical elements.
    Returns a tuple of two lists containing atom ids.
    (chemical atom IDs, catalyst atom IDs)
    '''
    chem_ids, cat_ids = [], []
    for id_, chem_sym in enumerate(elements):
        if chem_sym in filter_elements:
            chem_ids.append(id_)
        else:
            cat_ids.append(id_)
    return chem_ids, cat_ids


def split_by_chemicals(atoms, chemical_elements):
    '''
    Splits the input data by chemical elements.
    Returns a tuple of two lists containing atom ids.
    (chemical atom IDs, catalyst atom IDs)
    '''
    chem_ids, cat_ids = [], []
    for id_, chem_sym in enumerate(atoms.get_chemical_symbols()):
        if chem_sym in chemical_elements:
            chem_ids.append(id_)
        else:
            cat_ids.append(id_)
    return chem_ids, cat_ids


def split_by_chemical_atoms(atoms, chemical_elements):
    '''
    Splits the input data by chemical elements.
    Returns  a copy of the ase atoms object with new tags.
    '''
    atoms_copy = atoms.copy()
    new_tag_list = []
    for chem_sym in atoms.get_chemical_symbols():
        if chem_sym in chemical_elements:
            new_tag_list.append(2)
        else:
            new_tag_list.append(1)

    atoms_copy.set_tags(new_tag_list)
    return atoms_copy


def simple_surface_atom(surfaces, minimum=np.pi*0.75, atom_ids=None):
    """
    """
    if atom_ids is None:
        atom_ids = list(range(len(surfaces)))
    surface_ids = []
    for i, surface in enumerate(surfaces):
        if isinstance(surface, str) or surface is None:
            continue
        if surface > minimum:
            surface_ids.append(atom_ids[i])
    return surface_ids


def distance_plane_point(p, normal_vector, o):
    """Return the distance between a plane and point 'o'.
    The plane is defined by 'p' (point on the plane) and 
    the 'normal_vector' (perpendicular to the plane)"""
    a = o if isinstance(o, np.ndarray) else np.array(o)
    n = normalize_vec(normal_vector)
    
    return np.dot(a-p, n)


def normalize_vec(o, axis=0):
    a = o if isinstance(o, np.ndarray) else np.array(o)
    norm_a = np.linalg.norm(a)
    if norm_a == 0:
        return a
    return a/norm_a


def normalize_vec_list(a, axis=0):
    return np.array([normalize_vec(i) for i in a])

   
def get_cutoffs(offsets, cell, cutoff=1, use_perpendicular=True):
    cutoffs_norm = normalize_vec_list(offsets)
    if use_perpendicular:
        cell_norm = get_perpendicular_cell_vecs(cell, normalize=True)
    else:
        cell_norm = normalize_vec_list(cell)
   
    return normalize_vec_list(np.dot(cutoffs_norm, cell_norm)) * cutoff


def get_perpendicular_cell_vecs(cell, normalize=False, reverse=True):
    '''
    If input order is x, y, z for each vector the return will 
    be the perpendicuar vector to the yz, xz, xy plane.
    If reverse == False the return order will be xy, xz, yz
    '''
    perp_vecs = [np.cross(a, b) for a, b in combinations(cell, 2)]

    if reverse:
        perp_vecs = np.flip(perp_vecs, axis=0)

    if normalize:
        return normalize_vec_list(perp_vecs)

    return np.array(perp_vecs)


def ase_load_init(frames, selected_frame=0):
    atoms = frames[selected_frame]
    pos = atoms.get_positions()
    tags = atoms.get_tags()
    pbc = atoms.get_pbc()
    cell = atoms.cell
    box = atoms.cell.cellpar()
    chem_sym = atoms.get_chemical_symbols()
    return pos, tags, pbc, cell, box, chem_sym


def ase_load_frame_positions(frames, wanted_frames=[0]):
    new_pos_list = []
    for wanted_frame in wanted_frames:
        atoms = frames[wanted_frame]
        new_pos_list.append(atoms.get_positions())
    return new_pos_list


def new_tag_list(atom_ids, tag, size):
    new_tags = [0] * size
    for atom_id in atom_ids:
        new_tags[atom_id] = tag
    return new_tags


def retag_list_from_ids(tag_list, atom_ids, tag):
    new_tags = tag_list.copy()
    for atom_id in atom_ids:
        new_tags[atom_id] = tag
    return new_tags


def view_tag_lists_from_ids(atoms, tag_lists):
    new_atoms = atoms.copy()
    for i, tag_list in enumerate(tag_lists):
        if i == 0:
            new_tags = new_tag_list(tag_list, i+1, len(new_atoms))
        else:
            new_tags = retag_list_from_ids(new_tags, tag_list, i+1)
    new_atoms.set_tags(new_tags)
    utils.view(new_atoms)


def view_tag_lists_from_ids_frames(frames, tag_lists, **kwargs):
    new_frames = []
    for atoms in frames:
        new_atoms = atoms.copy()
        for i, tag_list in enumerate(tag_lists):
            if i == 0:
                new_tags = new_tag_list(tag_list, i+1, len(new_atoms))
            else:
                new_tags = retag_list_from_ids(new_tags, tag_list, i+1)
        new_atoms.set_tags(new_tags)
        new_frames.append(new_atoms)
    utils.view(new_frames, **kwargs)


def view_bond_tags(frames, bond_pairs_cache):
    new_frames = []
    for frame_id, atoms in enumerate(frames):
        new_atoms = atoms.copy()
        new_tags = [0 for _ in new_atoms.get_tags()]
        if bond_pairs_cache[frame_id]:
            for chem_atom_id, (cat_atom_id, distance) in bond_pairs_cache[frame_id].items():
                new_tags[chem_atom_id] = 2
                new_tags[cat_atom_id] = 1
        new_atoms.set_tags(new_tags)
        new_frames.append(new_atoms)
    utils.view(new_frames)


def generate_frame_range(selected_frame_id, min_bond_duration, extend_lookup, check_forwards=True, check_backwards=False):

    if not check_forwards and check_backwards:
        start_frame = selected_frame_id - (min_bond_duration + extend_lookup)
        end_frame = selected_frame_id
    elif check_forwards and check_backwards:
        half_duration = int(round((min_bond_duration+extend_lookup)/2))
        start_frame = selected_frame_id - half_duration
        end_frame = selected_frame_id + half_duration
    elif check_forwards and not check_backwards:
        start_frame = selected_frame_id
        end_frame = selected_frame_id + (min_bond_duration + extend_lookup)
    else:
        raise ValueError('Direction Error.')

    return range(start_frame, end_frame)


def convert_chem_sym_to_atomic_n(chem_sym):
    chem_sym_set = set(chem_sym)
    atomic_set = [mendeleev.element(chem).atomic_number for chem in chem_sym_set]
    conversion_dict = dict(zip(chem_sym_set, atomic_set))
    return [conversion_dict[symbol] for symbol in chem_sym]


def split_bond_durations(atom_bonded_frames, min_bond_duration=1):
    bond_status = collections.defaultdict(list)

    for atom_id, bonded_frames in atom_bonded_frames.items():
        sorted_frames = sorted(bonded_frames)
        current_frame = sorted_frames[0]
        bond_status_cache = [current_frame]

        for each_frame in bonded_frames[1:]:
            if each_frame - current_frame == 1:
                bond_status_cache.append(each_frame)
            else:
                if len(bond_status_cache) >= min_bond_duration:
                    bond_status[atom_id].append(bond_status_cache)
                    bond_status_cache = [each_frame]
            current_frame = each_frame

        if len(bond_status_cache) >= min_bond_duration:
            bond_status[atom_id].append(bond_status_cache)
            bond_status_cache = [each_frame]

    return bond_status


def check_for_bonds(frame_ids, bond_atoms, pos_cache, top_surface_atom_ids, bottom_surface_atom_ids,
                    chem_sym, approach_cutoff=5.0, bond_cutoff=4.0, bond_distance='auto', bond_fudge=1.0,
                    box=None, pair_distances_cache={}, bond_pairs_cache={}, atom_bonded_frames={},
                    approach_zone=None, return_cache=True, **kwargs):

    for frame_id in frame_ids:
        if frame_id in pair_distances_cache:
            do_calc_for = []
            for bond_atom_id in bond_atoms:
                if bond_atom_id not in pair_distances_cache[frame_id]:
                    do_calc_for.append(bond_atom_id)
        else:
            do_calc_for = bond_atoms
            pair_distances_cache[frame_id] = {}
            bond_pairs_cache[frame_id] = {}

        if do_calc_for:
            pos = pos_cache.get_positions(frame_id)
            pair_distances, bond_pairs = get_pair_distances_bond(pos,
                                                                 top_surface_atom_ids,
                                                                 bottom_surface_atom_ids,
                                                                 do_calc_for, chem_sym,
                                                                 approach_cutoff, bond_cutoff,
                                                                 bond_distance=bond_distance,
                                                                 bond_fudge=bond_fudge,
                                                                 box=box, approach_zone=approach_zone)

            pair_distances_cache[frame_id] = {**pair_distances_cache[frame_id], **pair_distances}
            bond_pairs_cache[frame_id] = {**bond_pairs_cache[frame_id], **bond_pairs}

        for bond_atom_id in bond_pairs_cache[frame_id].keys():
            if bond_atom_id in atom_bonded_frames:
                if frame_id not in atom_bonded_frames[bond_atom_id]:
                    atom_bonded_frames[bond_atom_id].append(frame_id)
            else:
                atom_bonded_frames[bond_atom_id] = [frame_id]
    if return_cache:
        return atom_bonded_frames, pair_distances_cache, bond_pairs_cache
    else:
        return atom_bonded_frames


def get_bonded_atom_ids(bond_status, bond_pairs_cache):
    # bonded_atom_ids[frame_id] -> list of atom ids which are bonded in the frame
    bonded_atom_ids = collections.defaultdict(list)

    for chemical_id, bond_lists in bond_status.items():
        for bond_list in bond_lists:
            for t_frame_id in bond_list:
                catalyst_id, t_distance = bond_pairs_cache[t_frame_id][chemical_id]
                for t_atom_id in [chemical_id, catalyst_id]:
                    if t_atom_id not in bonded_atom_ids[t_frame_id]:
                        bonded_atom_ids[t_frame_id].append(t_atom_id)

    return bonded_atom_ids


def get_bond_distance_library(path='library/bond_distances.csv'):
    return BondDistances(path)


def get_pyykoo_distance_library():
    return PyykkoDistances()


def get_bond_distances(library_type='pyykko', path='library/bond_distances.csv'):
    if library_type == 'pyykko' or library_type == 'PyykkoDistances':
        return get_pyykoo_distance_library()
    elif library_type == 'csv' or library_type == 'BondDistances':
        return get_bond_distance_library(path)


class PyykkoDistances:
    """
    Class for storing and retrieving Pyykko distances.
    """
    def __init__(self):
        self.__pyykko_distances = {}
        self.__element_set = set()

    def pair(self, e1, e2):
        return tuple(sorted([e1, e2]))

    def get_distance(self, e1, e2):
        """
        Get the Pyykko distance between two elements.

        Parameters
        ----------
        e1 : int or str
            First element as atomic number or chemical symbol.
        e2 : int or str
            Second element as atomic number or chemical symbol.

        Returns
        -------
        float
            Pyykko distance between the two elements.

        """
        return self.__get_distance(self.pair(e1, e2))

    def __get_distance(self, pair):
        if pair not in self.__pyykko_distances:
            self.__set_distance(pair)
        return self.__pyykko_distances[pair]

    def __set_distance(self, pair):
        self.__element_set.update(pair)
        self.__pyykko_distances[pair] = get_pyykoo_distance(*pair)

    def __getitem__(self, key):
        if not len(key) == 2:
            raise KeyError('Key has to be a list or tuple of lenght two.')
        return self.get_distance(*key)

    def all(self):
        """
        Get all Pyykko distances.

        Returns
        -------
        dict
            Dictionary of all Pyykko distances.

        """

        return self.__pyykko_distances

    def __len__(self):
        return len(self.__pyykko_distances)

    def __contains__(self, key):
        return self.pair(*key) in self.__pyykko_distances

    def __repr__(self):
        return f'{self.all()}'


def get_pyykoo_distance(e1, e2):
    """
    Get the Pyykko distance between two elements.

    Parameters
    ----------
    e1 : int or str
        First element as atomic number or chemical symbol.
    e2 : int or str
        Second element as atomic number or chemical symbol.

    Returns
    -------
    float
        Pyykko distance between the two elements.

    """
    
    return (mendeleev.element(e1).covalent_radius_pyykko +
            mendeleev.element(e2).covalent_radius_pyykko)*0.01


def debug_view_split(experiment, path_xyz=None, **kwargs):
    system = experiment.system
    if path_xyz is None:
        path_xyz = system.file.path
    
    view_tags = defaultdict(list)
    for atom_id, base_type in experiment.base_types_dict.items():
        view_tags[base_type].append(system.atoms.id_to_number[atom_id])
    view_tag_lists_from_ids_frames(utils.read(path_xyz), list(view_tags.values()), **kwargs)


def debug_view_surfaces(experiment, surface_group_id=None, path_xyz=None):
    system = experiment.system
    if path_xyz is None:
        path_xyz = system.file.path

    if surface_group_id is None:
        surface_group_id = sorted(experiment.surface_groups.keys())[-1]
    
    surfaces = experiment.surface_groups[surface_group_id]

    surface_atom_numbers = [[system.atoms.id_to_number[atom_id] for atom_id in surface.atom_ids] for surface in surfaces]

    view_tag_lists_from_ids_frames(utils.read(path_xyz), surface_atom_numbers)


class EventAnalysis:
    def __init__(self, experiment):
        self.experiment = experiment
        self.system = experiment.system

    def extended_analysis(self, duration_cutoff=5):
        connected = self.experiment.events.connected
        all_events, hoppings, adsorbed, ping, connection_durations = self.experiment.events._event_detection_extended(connected, duration_cutoff)
        return self.collect_event_dataframe(connected, hoppings, adsorbed, ping, connection_durations)

    def get_closest_catalyst_to_chemical(self, frame_number, chem_id, catalyst_ids):
        frame_id = self.experiment.frames.number_to_id[frame_number]
        catalyst_ids = tuple(catalyst_ids)
        connected_distances = []
        for catalyst_id in catalyst_ids:
            available, dist_ = self.system.distances.get_distance_if_available(frame_id, chem_id, catalyst_id)
            if not available:
                chem_number = self.system.atoms.id_to_number[chem_id]
                catalyst_number = self.system.atoms.id_to_number[catalyst_id]
                pos_chem = self.system.positions[frame_number][chem_number]
                pos_catalyst = self.system.positions[frame_number][catalyst_number]
                dist_ = np.linalg.norm(pos_chem - pos_catalyst)
            connected_distances.append(dist_)
        closest_catalyst = np.argmin(connected_distances)
        closest_distance = connected_distances[closest_catalyst]
        closest_catalyst_id = catalyst_ids[closest_catalyst]
        return closest_catalyst_id, closest_distance

    def get_solid_angle(self, catalyst_id, frame_number, hinuma_id=None, calculate=False):
        frame_id = self.experiment.frames.number_to_id[frame_number]
        # Check if the hinuma exists for the frame
        if hinuma_id is None:
            for hinuma in self.experiment.hinumas:
                if hinuma.frame_id == frame_id:
                    hinuma_id = hinuma.hinuma_id
                    break
        
        if hinuma_id is None and calculate:
            # TODO: Calculate solid angle
            raise NotImplementedError('Hinuma calculation not implemented')
        
        if hinuma_id is None and not calculate:
            # Check if there are any hinumas
            if len(self.experiment.hinumas) == 0:
                raise ValueError('No hinumas in experiment')
        
            # Try get closest hinuma
            closest_hinuma = None
            for hinuma in self.experiment.hinumas:
                hinuma_infos = {}
                if catalyst_id in hinuma.solid_angles:
                    hinuma_infos['hinuma_id'] = hinuma.hinuma_id
                    hinuma_infos['solid_angle'] = hinuma.solid_angles[catalyst_id]
                    hinuma_infos['frame_id'] = hinuma.frame_id
                    hinuma_infos['frame_number'] = self.experiment.frames.id_to_number[hinuma.frame_id]
                    hinuma_infos['frame_delta'] = abs(frame_number - hinuma_infos['frame_number'])
                    if closest_hinuma is None:
                        closest_hinuma = hinuma_infos
                    else:
                        if hinuma_infos['frame_delta'] < closest_hinuma['frame_delta']:
                            closest_hinuma = hinuma_infos
            return closest_hinuma['hinuma_id'], closest_hinuma['solid_angle']
        
        else:
            hinuma = self.experiment.hinumas[hinuma_id]
            
            # check if catalyst is in hinuma
            if catalyst_id in hinuma.solid_angles:
                return hinuma_id, hinuma.solid_angles[catalyst_id]
            else:
                raise ValueError('Catalyst not in hinuma, and hinuma not calculated')

    def collect_event_dataframe(self, connected, hoppings, adsorbed, ping, connection_durations):
        columns = ['frame_number', 'chem_number', 'catalyst_number', 'duration', 'distance', 'hopping', 'adsorbed', 'solid_angle']
        rows = []
        for frame_number in connection_durations:
            hinuma_id = None
            for chem_id in connection_durations[frame_number]:
                # Get duration
                duration = connection_durations[frame_number][chem_id]        
                
                # Check if hopping
                if frame_number in hoppings and chem_id in hoppings[frame_number]:
                    is_hopping = True
                else:
                    is_hopping = False

                # Check if adsorbed
                if frame_number in adsorbed and chem_id in adsorbed[frame_number]:
                    is_adsorbed = True
                else:
                    is_adsorbed = False

                # Check if ping
                if frame_number in ping and chem_id in ping[frame_number]:
                    is_ping = True
                else:
                    is_ping = False

                # Check if ping and adsorbed at the same time (should not happen)
                if is_ping and is_adsorbed:
                    raise ValueError('Ping and adsorbed at the same time')
                
                # Get closest catalyst_id
                catalyst_ids = connected[chem_id][frame_number]
                if len(catalyst_ids) == 0:
                    raise ValueError('No catalysts connected')
                else:
                    catalyst_id, distance = self.get_closest_catalyst_to_chemical(frame_number, chem_id, catalyst_ids)

                # Get Solid angle for catalyst
                hinuma_id, solid_angle = self.get_solid_angle(catalyst_id, frame_number, hinuma_id=hinuma_id, calculate=False)
                
                # Get chem_number
                chem_number = self.experiment.system.atoms.id_to_number[chem_id]
                catalyst_number = self.experiment.system.atoms.id_to_number[catalyst_id]
                
                rows.append([frame_number, chem_number, catalyst_number, duration, distance, is_hopping, is_adsorbed, solid_angle])
        
        df = pd.DataFrame(rows, columns=columns)
        return df.sort_values(by=['frame_number', 'chem_number'])

    def save_df_as_csv(self, df, path=None, name=None): 
        # Save dataframe as csv without index
        if name is None:
            name = self.experiment.system.file.name.split('.')[0] + '_events.csv'
        if path is None:
            path = Path(self.experiment.system.file.path).parent
        else:
            path = Path(path)
        df.to_csv(path / name, index=False)


class BondDistances:
    def __init__(self, path):
        self.__element = mendeleev.element
        self.__key_cache = {}
        self.path = path
        self.read_from_csv(path)

    def get_distance(self, e1, e2):
        return self.__get_val(e1, e2, self.__distances)

    def get_type(self, e1, e2):
        return self.__get_val(e1, e2, self.__types)

    def get_types(self, style='symbol'):
        return self.__get_renamed_dict(self.__types, style)

    def get_distances(self, style='symbol'):
        return self.__get_renamed_dict(self.__distances, style)

    def clear_cache(self):
        self.__key_cache = {}

    def __get_renamed_dict(self, dict_, style):
        if style == 'symbol':
            trans_func = self.__get_atomic_symbols
        elif style == 'name':
            trans_func = self.__get_atomic_names
        else:
            return dict_
        return self.__rename_key_pair(dict_, trans_func)

    def read_from_csv(self, path):
        self.__distances = {}
        self.__types = {}
        with open(path) as csvfile:
            content = csv.reader(csvfile)
            next(content, None)  # skip header
            for e1, e2, distance, type_ in content:
                dict_key = self.__get_key(e1, e2)
                self.__distances[dict_key] = float(distance)
                if type_ == 'None':
                    type_ = None
                self.__types[dict_key] = type_

    def __len__(self):
        return len(self.__distances)

    def __contains__(self, key):
        dict_key = self.__get_key(*key)
        return dict_key in self.__distances

    def __get_val(self, e1, e2, list_):
        dict_key = self.__get_key(e1, e2)
        if dict_key in list_:
            return list_[dict_key]
        else:
            return None

    def __get_key(self, e1, e2):
        atomic_number1, atomic_number2 = self.__get_atomic_numbers((e1, e2))
        return self.__ordered_pair(atomic_number1, atomic_number2)

    def __get_atomic_numbers(self, list_):
        atomic_numbers = []
        for str_ in list_:
            atomic_numbers.append(self.__get_local_key(str_))
        return tuple(atomic_numbers)

    def __get_atomic_names(self, e1, e2):
        return (self.__element(e1).name, self.__element(e2).name)

    def __get_atomic_symbols(self, e1, e2):
        return (self.__element(e1).symbol, self.__element(e2).symbol)

    def __rename_key_pair(self, dict_, trans_func):
        new_dict = {}
        for (e1, e2), val in dict_.items():
            new_dict[trans_func(e1, e2)] = val
        return new_dict

    def __ordered_pair(self, x, y):
        return tuple(sorted([x, y]))

    def __get_local_key(self, element):
        if element not in self.__key_cache:
            self.__key_cache[element] = self.__element(element).atomic_number
        return self.__key_cache[element]


class PositionCache:
    """
    Cache for atomic positions.

    Parameters
    ----------
    pos_typ : str
        Type of positions to cache. Currently only 'ase' is supported.
    data : object
        Data object to get positions from.
    init_id : int
        Initial frame id to cache.

    """
    def __init__(self, pos_typ, data=None, init_id=None):
        self.pos_typ = pos_typ
        self.positions = {}
        if data is not None:
            self.data = data
        if pos_typ == 'ase':
            self.__get_pos = self.__get_ase_pos
        if init_id is None:
            self.__add_pos(init_id)

    def get_positions(self, frame_id):
        if frame_id not in self.positions:
            self.positions[frame_id] = self.__get_pos(frame_id)
        return self.positions[frame_id]

    def __get_ase_pos(self, frame_id):
        return self.data[frame_id].get_positions()

    def __add_pos(self, frame_id):
        self.positions[frame_id] = self.__get_pos(frame_id)

    def __len__(self):
        return len(self.positions)

    def __contains__(self, key):
        return key in self.positions

    def len_data(self):
        return len(self.data)


if __name__ == "__main__":
    pass
