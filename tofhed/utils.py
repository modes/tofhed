from pathlib import Path
from ase.io import read as ase_read
from ase.visualize import view as ase_view


def view(frames, *args, **kwargs):
    return ase_view(frames, *args, **kwargs)


def read(path, *args, index=':', **kwargs):
    return ase_read(Path(path), *args,  index=index, **kwargs)


def create_frame_range(frame_range, len_frames):
    frame_number_range = []
    for i in frame_range:
        if i >= 0:
            frame_number_range.append(i)
        elif i < 0:
            frame_number_range.append(len_frames + i + 1)
    return tuple(frame_number_range)


def ase_frame_range(start, stop):
    return f'{start}:{stop}'


def file_exists(path):
    return Path(path).exists()


if __name__ == "__main__":
    pass
