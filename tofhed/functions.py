from sqlalchemy.orm import Session
from datetime import datetime
from sqlalchemy import select, delete, join, update
from sqlalchemy import and_, not_, or_, exc
from sqlalchemy import MetaData, cast, Integer
from sqlalchemy.orm import aliased
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import create_engine
from itertools import islice

import numpy as np
import ase
from ase.geometry import cellpar_to_cell
from collections import defaultdict
from tofhed.db.init import *
from tofhed.analysis import view_tag_lists_from_ids_frames, get_distance_pairs
from tofhed.utils import view
from tofhed.db import schema
from pathlib import Path


def is_jupyter():
    """
    Checks if the current environment is Jupyter Notebook or Jupyter Lab.
    """
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter Notebook or Jupyter Lab
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter



def database_status(url):
    """
    Checks the postgresql database if it exists
    """
    engine = create_engine(url)

    try:
        # Try to connect to the database
        with engine.connect():
            return "Connected successfully"
    except exc.OperationalError as e:
        print(e)
        # Check the error message to differentiate the type of OperationalError
        if "connection refused" in str(e).lower():
            return "Connection refused"
        elif "permission denied" in str(e).lower():
            return "Permission denied"
        elif '" does not exist' in str(e).lower():
            return "Database does not exist"
        else:
            return "Some other OperationalError"

    except Exception as e:
        return f"Unhandled exception: {e}"


def build_db(engine_path=None, echo=False, overwrite=False, **kwargs):
    """
    Builds and initializes a new database.

    Parameters
    ----------
    engine_path : str or pathlib.Path, optional (default=None)
        The path to the database. If None, a sqlite database is created in memory.
    echo : bool, optional (default=False)   
        If True, the SQL commands are printed.
    overwrite : bool, optional (default=False)
        If True, the database is overwritten if it already exists.

    Returns
    -------
    sqlalchemy.engine.Engine
        The engine to the database.
    """
    engine = schema.create_db(engine_path, echo=echo, overwrite=overwrite)
    initialize_new_database(engine)
    return engine


def check_path(path):
    '''Checks path if file exists.'''
    if not Path(path).exists():
        raise FileNotFoundError(f'File {path} does not exist')
    
    return True


def get_engine_path(path, dialect='sqlite+pysqlite', **kwargs):
    db_path = Path(path)
    if 'sqlite' in dialect:
        return f'{dialect}:///{db_path.resolve()}?ch eck_same_thread=False'
    elif 'postgresql' in dialect:
        if 'port' in kwargs:
            return f'{dialect}://{kwargs["user"]}:{kwargs["password"]}@{kwargs["host"]}:{kwargs["port"]}/{kwargs["database"]}'
        else:
            return f'{dialect}://{kwargs["user"]}:{kwargs["password"]}@{kwargs["host"]}/{kwargs["database"]}'
    
    raise ValueError(f'Dialect {dialect} not supported.')


def get_engine(path, echo=False, dialect='sqlite+pysqlite', **kwargs):
    """
    Creates an engine to the database.

    Parameters
    ----------
    path : str or pathlib.Path
        The path to the database.
    echo : bool, optional (default=False)
        If True, the SQL commands are printed.
    dialect : str, optional (default='sqlite+pysqlite')
        The dialect of the database. Can be 'sqlite+pysqlite' or 'postgresql+psycopg2'.
    **kwargs : dict
        The kwargs for the dialect. For 'sqlite+pysqlite' the kwargs are ignored. For 'postgresql+psycopg2' the kwargs are 'user', 'password', 'host', 'port' and 'database'.

    Returns
    -------
    sqlalchemy.engine.Engine
        The engine to the database.

    """
    engine_path = get_engine_path(path, dialect=dialect, **kwargs)

    return create_engine(engine_path, echo=echo)


def initialize_new_database(engine):
    """Initializes a fresh database with all tables and default values.

    Parameters
    ----------
    engine : sqlalchemy.engine.Engine
        The engine to the database.

    Returns
    -------
    bool
        True if the database was initialized successfully.
    
    """
    with Session(engine) as session:
        initialize_parameters(session)
        initialize_elements(session)
        initialize_properties(session)
        initialize_connection_types(session)
        initialize_base_types(session)
        initialize_surface_types(session)
        initialize_event_types(session)

    return True


def new_surface_type(session, name, commit=True):
    """
    Creates a new surface_type.
    
    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    name : str
        The name of the surface_type.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.
    
    Returns
    -------
    int
        The id of the new surface_type.
    
    """

    surface_type = SurfaceType(name=name)

    session.add(surface_type)
    if commit:
        session.commit()

    return surface_type.id


def new_settings(session, commit=True):
    """
    Creates new settings.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.
    
    Returns
    -------
    int
        The id of the new settings.
    
    """

    settings = Settings(created_at=datetime.now())
    session.add(settings)
    if commit:
        session.commit()

    return settings.id


def compare_settings(session, settings):
    """
    Compares the settings to the settings in the database if it already exists and returns the id of the settings if it does.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    settings : dict
        The settings to compare.

    Returns
    -------
    int or bool
        The id of the settings if it already exists, otherwise False.

    """

    setting_ids = session.query(Settings.id).all()

    for setting_id in setting_ids:
        loaded_settings = get_settings_updated(session, setting_id.id)

        if len(loaded_settings) is len(settings):
            test = [1 for k, v in settings.items() if k in loaded_settings and v == loaded_settings[k]]
            if len(settings) is len(test):
                return setting_id.id
    return False


def set_settings(session, settings, commit=True):
    """
    Writes the settings to the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    settings : dict
        The settings to write.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.
    
    Returns
    -------
    int
        The id of the settings.
    
    """
    
    find_setting_id = compare_settings(session, settings)
    if find_setting_id:
        return find_setting_id

    setting_id = new_settings(session)
    
    parameters = {v.name: v.id for v in session.query(Parameter).all()}

    for key, value in settings.items():    
        sett_param = SettingParameter(
            setting_id=setting_id,
            parameter_id=parameters[key],
            value=str(value),
            )
        session.add(sett_param)

    if commit:
        session.commit()
        return setting_id


def delete_settings(session, setting_id, commit=True):
    """
    Deletes the settings from the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    setting_id : int
        The id of the settings.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    """
    session.execute(
        delete(SettingParameter
               ).where(SettingParameter.setting_id == setting_id))
    session.execute(
        delete(Settings).where(Settings.id == setting_id))
    if commit:
        session.commit()


def type_conversion(value, datatype):
    """
    Converts a value to the datatype specified.
    
    Parameters
    ----------
    value : str
        The value to convert.
    datatype : str
        The datatype to convert to.
    
    Returns
    -------
    str, int, float, bool

    Notes
    -----
    The following datatypes are supported:
        - Float
        - Integer
        - Boolean
        - String
        - NoneType
    """
    
    if value is None or value == 'None':
        return None
    
    if datatype == 'Float':
        return float(value)

    elif datatype == 'Integer':
        return int(value)

    elif datatype == 'Boolean':
        if isinstance(value, str):
            if value == 'false' or value == 'False':
                return False
            if value == 'true' or value == 'True':
                return True
        return bool(int(value))

    elif datatype == 'String':
        return str(value)
    
    elif datatype == 'NoneType':
        return None

    else:
        return str(value)


def get_settings(session, setting_id):
    """
    Loads the settings from the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    setting_id : int
        The id of the settings.
    
    Returns
    -------
    dict
        The settings as a dictionary.

    """

    stmt = select(
        Parameter.name,
        SettingParameter.value,
        Parameter.datatype
    ).where(
        and_(Parameter.id == SettingParameter.parameter_id,
             SettingParameter.setting_id == setting_id))

    loaded_settings = {}
    for name, value, datatype in session.execute(stmt).all():
        loaded_settings[name] = type_conversion(value, datatype)
    return loaded_settings


def get_settings_updated(session, setting_id):
    values = session.query(SettingParameter,
                           Parameter
                           ).filter(
        SettingParameter.setting_id == setting_id
        ).join(
        Parameter,
        Parameter.id == SettingParameter.parameter_id
        ).all()
    return {v.Parameter.name: type_conversion(v.SettingParameter.value, v.Parameter.datatype) for v in values}
        

def get_all_settings(session):
    values = session.query(Settings.id).all()
    return {v.id: get_settings_updated(session, v.id) for v in values}


def new_event(session, experiment_id, frame_id,  event_type_id, commit=True):
    event = Event(experiment_id=experiment_id,
                  frame_id=frame_id,
                  event_type_id=event_type_id,
                  )
    session.add(event)

    if commit:
        session.commit()

    return event.id


def new_experiment(session, setting_id, system_id, note='', commit=True):
    """
    Creates a new experiment.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    setting_id : int
        The id of the settings. 
    system_id : int
        The id of the system.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new experiment.
    
    """

    experiment = Experiment(created_at=datetime.now(),
                             setting_id=setting_id,
                             system_id=system_id,
                             note=note,)
    session.add(experiment)

    if commit:
        session.commit()

    return experiment.id


def update_experiment(session, experiment_id, updates):
    """
    Updates the experiment with the given id and the given updates dictionary.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The id of the experiment.
    updates : dict
        The updates to apply to the experiment.
    
    """
    session.execute(
        update(Experiment).
        where(Experiment.id == experiment_id).
        values(**updates)
    )
    session.commit()


def delete_experiment(session, experiment_id, commit=True):
    """
    Deletes the experiment with the given id.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The id of the experiment.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    """
    session.execute(delete(Experiment).where(
        Experiment.id == experiment_id))
    if commit:
        session.commit()


def get_settings_from_experiment(session, experiment_id):
    """
    Loads the settings from the database for the given experiment id.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The id of the experiment.

    Returns
    -------
    dict
        The settings as a dictionary.

    """

    setting_id = session.query(Experiment).filter(
        Experiment.id == experiment_id).first().setting_id

    return get_settings_updated(session, setting_id)


def new_file(session, path, import_positions=True, commit=True):
    """
    Creates a new file in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    path : str
        The path to the file.
    import_positions : bool, optional (default=True)
        If True, the positions are imported.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new file.
    
    """

    file = File(path=str(path),
                 created_at=datetime.now(),
                 import_positions=import_positions)

    session.add(file)
    if commit:
        session.commit()

    return file.id


def get_files(session):
    """
    Loads the files from the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    dict
        All files in the database in a dictionary with the file_id as key.
        
    Notes
    -----
    It contains the following keys:
        - file_id
        - path
        - created_at
        - import_positions
    
    The structure is as follows:
        {file_id: {
            'file_id': file_id,
            'path': path,
            'created_at': created_at,
            'import_positions': import_positions,
        }}
    """
    
    files = session.query(File).all()

    return [{'file_id': file.id,
             'path': file.path,
             'created_at': file.created_at,
             'import_positions': file.import_positions,
                } for file in files]


def get_file_paths(session):
    """
    Loads the files from the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    list
        All filepaths in the database.

    """

    return [v.path for v in session.query(File).all()]


def file_exists(session, path):
    """
    Checks if the file exists in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    path : str
        The path to the file.

    Returns
    -------
    bool
        True if the file exists, False otherwise.

    """

    return str(path) in get_file_paths(session)


def new_frames(session, frame_numbers, system_id, commit=True):

    for frame_number in frame_numbers:
        new_frame(session, frame_number, system_id, commit=False)
    
    if commit:
        session.commit()
    
    return get_frame_ids_dict(session, system_id)


def new_frame(session, frame_number, system_id, commit=True):
    """
    Creates a new frame in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    frame_number : int
        The frame number.
    system_id : int
        The id of the system.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new frame.

    """

    frame = Frame(frame_number=frame_number,
                   system_id=system_id)

    session.add(frame)
    if commit:
        session.commit()

    return frame.id


def new_system(session, pbc, box, cell, file_id, frame_number_first, frame_number_last, 
               is_sequence=False, sequence_system_id=None, note='', commit=True):
    """
    Creates a new system in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    pbc : list of bool
        The periodic boundary conditions.
    box : list of float
        The box vectors. The first three elements are the lengths of the box
        vectors, the last three are the angles between the box vectors.
    file_id : int
        The id of the file.
    note : str, optional (default='')
        A note for the system.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new system.

    """

    system = System(
        note=note,
        created_at=datetime.now(),
        pbc_x=bool(pbc[0]),
        pbc_y=bool(pbc[1]),
        pbc_z=bool(pbc[2]),
        len_x=box[0],
        len_y=box[1],
        len_z=box[2],
        angle_x=box[3],
        angle_y=box[4],
        angle_z=box[5],
        cell_1_x = cell[0][0],
        cell_1_y = cell[0][1],
        cell_1_z = cell[0][2],
        cell_2_x = cell[1][0],
        cell_2_y = cell[1][1],
        cell_2_z = cell[1][2],        
        cell_3_x = cell[2][0],
        cell_3_y = cell[2][1],
        cell_3_z = cell[2][2],
        frame_number_first = frame_number_first,
        frame_number_last = frame_number_last,
        file_id=file_id,
        is_sequence=is_sequence,
    )
    if is_sequence and not sequence_system_id is None:
        system.sequence_system_id = sequence_system_id

    session.add(system)
    if commit:
        session.commit()    
        if is_sequence and sequence_system_id is None:
            system.sequence_system_id = system.id
            session.commit()
    elif is_sequence and sequence_system_id is None:
        raise ValueError('The sequence_system_id must be set if commit is False.')

    return system.id

def new_sequence(session, commit=True):
    """
    Creates a new sequence in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new sequence.

    """

    sequence = Sequence()
    session.add(sequence)
    if commit:
        session.commit()

    return sequence.id


def new_sequence_system(session, sequence_id, system_id, sequence_order, commit=True):
    """
    Creates a new sequence system in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    sequence_id : int
        The id of the sequence.
    system_id : int
        The id of the system.
    sequence_order : int
        The order of the system in the sequence.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new sequence system.

    """

    sequence_system = SequenceSystem(
        sequence_id=sequence_id,
        system_id=system_id,
        sequence_order=sequence_order,
    )
    session.add(sequence_system)
    if commit:
        session.commit()

    return sequence_system.id

def get_sequence_ids(session):
    """
    Returns the sequences in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    list of int
        The ids of the sequences.

    """

    values = session.query(
        Sequence.id
        ).all()
    return [v.id for v in values]


def get_sequence(session, sequence_id):
    """
    Returns the systems of a sequence.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    sequence_id : int
        The id of the sequence.

    Returns
    -------
    dict of int : int
        The sequence systems of the sequence. The keys are the sequence orders,
        the values are the system ids.
    """

    values = session.query(
        SequenceSystem
        ).filter_by(
            sequence_id=sequence_id
        ).order_by(
            SequenceSystem.sequence_order
        ).all()
    
    return {v.sequence_order: v.system_id for v in values}


def get_last_order_number(session, sequence_id):
    """
    Returns the last order number of a sequence.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    sequence_id : int
        The id of the sequence.

    Returns
    -------
    int
        The last order number of the sequence.
    """

    value = session.query(
        SequenceSystem
        ).filter_by(
        sequence_id=sequence_id
        ).order_by(
        SequenceSystem.sequence_order.desc()
        ).first()
    if value is None:
        raise ValueError('The sequence does not contain any systems.')
    else:
        return value.sequence_order


def new_long_experiment(session, setting_id, sequence_id, note='', commit=True):
    """
    Creates a new long experiment in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    setting_id : int
        The id of the setting.
    sequence_id : int
        The id of the sequence.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new long experiment.

    """

    long_experiment = LongExperiment(
        setting_id=setting_id,
        sequence_id=sequence_id,
        note=note,
    )
    session.add(long_experiment)
    if commit:
        session.commit()

    return long_experiment.id


def get_long_experiments(session):
    """
    Returns the long experiments in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    dict of int : dict
        The long experiments in the database. The keys are the long experiment
        ids, the values are the setting ids, the sequence ids and the notes.

    """

    values = session.query(
        LongExperiment.id
        ).all()
    
    return {v.id:
            {'setting_id': v.setting_id, 
             'sequence_id': v.sequence_id,
             'note': v.note} for v in values}


def new_sequence_experiment(session, long_experiment_id, sequence_id, experiment_id,
                            sequence_order, note='', commit=True):
    """
    Creates a new sequence experiment in the database.
    """
    sequence_experiment = SequenceExperiment(long_experiment_id=long_experiment_id,
                                             sequence_id=sequence_id,
                                             experiment_id=experiment_id,
                                             sequence_order=sequence_order,
                                             note=note)
    session.add(sequence_experiment)
    if commit:
        session.commit()
    
    return sequence_experiment.id

def get_sequence_experiments(session, long_experiment_id):
    """
    Returns the sequence experiments of a long experiment.
    """
    values = session.query(
        SequenceExperiment
        ).filter_by(
            long_experiment_id=long_experiment_id
        ).order_by(
            SequenceExperiment.sequence_order
        ).all()
    
    return {v.sequence_order:
            {'sequence_id': v.sequence_id,
             'experiment_id': v.experiment_id,
             'note': v.note} for v in values}


def new_atoms(session, atom_numbers, atomic_numbers, system_id, init_tags, commit=True):
    for atom_number, atomic_number, init_tag in zip(atom_numbers, atomic_numbers, init_tags):
        new_atom(session, atom_number, atomic_number, system_id, init_tag, commit=False)
    
    if commit:
        session.commit()
    return get_atom_ids(session, system_id)


def new_atom(session, atom_number, atomic_number, system_id, init_tag, commit=True):
    """
    Creates a new atom in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    atom_number : int
        The atom number.
    atomic_number : int
        The atomic number.
    system_id : int
        The id of the system.
    init_tag : str
        The original tag for the atom at init_frame in the file
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new atom.
    
    """

    atom = Atom(atom_number=int(atom_number),
                 atomic_number=int(atomic_number),
                 system_id=int(system_id),
                 init_tag=int(init_tag))

    session.add(atom)
    if commit:
        session.commit()

    return atom.id


def new_base_type(session, name, commit=True):
    """
    Creates a new base type in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    name : str
        The name of the base type.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new base type.

    """

    base_type = BaseType(name=name)

    session.add(base_type)
    if commit:
        session.commit()

    return base_type.id


def new_hinuma(session, experiment_id, frame_id, base_type_id, commit=True):
    """
    Creates a new hinuma entry in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The id of the experiment.
    frame_id : int
        The id of the frame.
    commit : bool, optional (default=True)
        If True, the changes are committed to the database.

    Returns
    -------
    int
        The id of the new hinuma entry.
    
    """
    
    hinuma = Hinuma(experiment_id=experiment_id, 
                    frame_id=frame_id, 
                    base_type_id=base_type_id)
    session.add(hinuma)
    if commit:
        session.commit()

    return hinuma.id


def get_atom_ids(session, system_id):
    """
    Gets the ids of the atoms in a system.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The id of the system.
    
    Returns
    -------
    list of int
        The ids of the atoms in the system.

    """

    values = session.query(
        Atom.id
    ).filter(
        Atom.system_id == system_id
    ).order_by(Atom.atom_number)

    return [atom.id for atom in values]


def get_system_id_for_atom_id(session, atom_id):
    value = session.query(
        Atom.system_id
    ).filter(
        Atom.id == atom_id
    ).one()

    return value.system_id


def get_atom_numbers_ids_dict(session, system_id):
    """
    Converts the atom ids to atom numbers.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The id of the system.

    Returns
    -------
    dict
        The atom ids as keys and the atom numbers as values.
    
    """

    values = session.query(
        Atom.id,
        Atom.atom_number
    ).filter(
        Atom.system_id == system_id
    ).order_by(Atom.atom_number)

    return {atom.id: atom.atom_number for atom in values}


def get_atom_ids_dict(session, system_id):
    """
    Converts the atom numbers to atom ids.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The id of the system.
    
    Returns
    -------
    dict
        The atom numbers as keys and the atom ids as values.

    """

    values = session.query(
        Atom.id,
        Atom.atom_number
    ).filter(
        Atom.system_id == system_id
    ).order_by(Atom.atom_number)

    return {atom.atom_number: atom.id for atom in values}


def new_positions_dicts(atom_ids, frame_id, pos, tags):
    position_dicts = []
    for atom_id, xyz, tag in zip(atom_ids, pos, tags):
        position_dicts.append(new_position_dict(atom_id, frame_id, xyz, int(tag)))
    
    return position_dicts


def new_position_dict(atom_id, frame_id, xyz, tag):
    """
    Creates a dictionary for a new position entry which can be used
    with the function new_position_from_dict.

    Parameters
    ----------
    atom_id : int
        The id of the atom.
    frame_id : int
        The id of the frame.
    xyz : list of float
        The x, y, and z coordinates of the atom.

    Returns
    -------
    dict
        The dictionary for the new position entry.

    """

    return {'atom_id': atom_id,
            'frame_id': frame_id,
            'x': xyz[0],
            'y': xyz[1],
            'z': xyz[2],
            'import_tag': tag,
           }


def get_system_ids(session, file_id):
    """
    Gets the ids of the systems in a file.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    file_id : int
        The id of the file.
    
    Returns
    -------
    list of int
        The ids of the systems in the file.

    """

    values = session.query(
        System.id
    ).filter(
        System.file_id == file_id
    ).order_by(System.id)

    return [row.id for row in values]


def get_system_id_from_frame(session, frame_id):
    """
    Gets the id of the system from a frame_id.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    frame_id : int
        The id of the frame.
    
    Returns
    -------
    int
        The id of the system.

    """

    value = session.query(
        Frame.system_id
    ).filter(
        Frame.id == frame_id
    ).one()

    return value.system_id


def get_ids_from_experiment(session, experiment_id):
    """
    Returns a dictionary with the date of creation of the experiment 
    and the ids of the setting and system.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The id of the experiment.

    Returns
    -------
    dict
        The date of creation of the experiment and the ids of the
        setting and system.
         
    Notes
    -----
    The dictionary has the following keys

    created_at : datetime.datetime
        The date of creation of the experiment.
    setting_id : int
        The id of the setting.
    system_id : int
        The id of the system.
    
    """

    v = session.query(
        Experiment
    ).filter(Experiment.id == experiment_id).one()

    return {'created_at': v.created_at,
            'setting_id': v.setting_id,
            'system_id': v.system_id,
            'note': v.note,
            }


def get_path(session, file_id):
    """
    Gets the path of a file.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    file_id : int
        The id of the file.

    Returns
    -------
    str
        The path of the file.

    """

    value = session.query(
        File.path
    ).filter(
        File.id == file_id
    ).one()

    return value.path


def get_systems(session):
    """
    Gets the systems from the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    
    Returns
    -------
    list of dict
        The systems in the database.

    Notes
    -----
    systems[system_id][key] = value

    The list contains dictionaries with the following keys

    system_id : int
        The id of the system.
    path : str
        The path of the file.
    file_id : int
        The id of the file.
    note : str
        The note of the system.
    tags : list of int
        The original tags from the import file
        ordered by atom number.
    pbc : list of bool
        The periodic boundary conditions of the system.
    cell : list of float
        The cell of the system.
    box : list of float
        The box of the system.
    chem_sym : list of str
        The chemical symbols of the atoms in the system.

    """

    systems = []

    rows = session.query(
        System,
        File
    ).join(
        File, File.id == System.file_id 
    ).all()
    
    for row in rows:
        system_id = row.System.id
        file_id = row.System.file_id
        created_at = row.System.created_at
        path = row.File.path
        note = row.System.note
        pbc = [row.System.pbc_x, row.System.pbc_y, row.System.pbc_z]
        box = [row.System.len_x, row.System.len_y, row.System.len_z,
               row.System.angle_x, row.System.angle_y, row.System.angle_z]
        cell = cellpar_to_cell(box)
        frame_number_first = row.System.frame_number_first
        frame_number_last = row.System.frame_number_last
        is_sequence = row.System.is_sequence
        sequence_id = row.System.sequence_id

        values = session.query(
            Element.chemical_symbol,
            Atom.init_tag
        ).filter(
            Atom.system_id == system_id
        ).join(
            Element, Atom.atomic_number == Element.id
        ).order_by(
            Atom.atom_number
        ).all()

        chem_sym = [value.chemical_symbol for value in values]
        tags = [value.init_tag for value in values]

        systems.append(
            {
                'system_id': system_id,
                'created_at': created_at,
                'path': path,
                'file_id': file_id,
                'note': note,
                'tags': tags,  # TODO: change to import_tags
                'pbc': pbc,
                'cell': cell,
                'box': np.array(box),
                'chem_sym': chem_sym,
                'frame_number_first': frame_number_first,
                'frame_number_last': frame_number_last,
                'is_sequence': is_sequence,
                'sequence_id': sequence_id
             }
        )

    return systems


def get_system(session, system_id):
    """
    Gets the system from the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The id of the system.
    
    Returns
    -------
    dict
        The system.

    Notes
    -----
    system[key] = value

    The dictionary has the following keys

    file_id : int
        The id of the file of the system.
    note : str
        The note of the system.
    tags : list of int
        The original tags from the import file
        ordered by atom number.
    pbc : list of bool
        The periodic boundary conditions of the system.
    cell : list of float
        The cell of the system.
    box : list of float
        The box of the system.
    chem_sym : list of str
        The chemical symbols of the atoms in the system.
    path : str
        The path of the file of the system.

    """
    
    values = session.query(
        System
    ).filter(
        System.id == system_id
    ).one()

    file_id = values.file_id
    note = values.note
    path = values.path
    pbc = [values.pbc_x, values.pbc_y, values.pbc_z]
    box = [values.len_x, values.len_y, values.len_z,
           values.angle_x, values.angle_y, values.angle_z]
    cell = cellpar_to_cell(box)
    created_at = values.created_at
    frame_number_first = values.frame_number_first
    frame_number_last = values.frame_number_last
    is_sequence = values.is_sequence
    sequence_system_id = values.sequence_system_id

    values = session.query(
            Element.chemical_symbol,
            Atom.init_tag
        ).select_from(
            join(
                Element,
                Atom
                )
        ).filter(
            Atom.system_id == system_id
        ).order_by(
            Atom.atom_number
        ).all()

    chem_sym = [value.chemical_symbol for value in values]
    tags = [value.init_tag for value in values]

    system = {
        'file_id': file_id,
        'created_at': created_at,
        'note': note,
        'tags': tags,
        'pbc': pbc,
        'cell': cell,
        'box': np.array(box),
        'chem_sym': chem_sym,
        'path': path,
        'frame_number_first': frame_number_first,
        'frame_number_last': frame_number_last,
        'is_sequence': is_sequence,
        'sequence_system_id': sequence_system_id
        }

    return system


def get_experiments(session):
    """
    Returns a list of all experiments in the database as dictionaries.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    list of dict
        The experiments in the database.
    
    Notes
    -----
    experiments[experiment_id][key] = value

    The list contains dictionaries with the following keys

    id : int
        The id of the experiment.
    created_at : datetime
        The time the experiment was created.
    setting_id : int
        The id of the setting of the experiment.
    system_id : int
        The id of the system of the experiment.   
    
    """

    values = session.query(Experiment).all()
    return [{'id': v.id,
             'created_at': v.created_at,
             'setting_id': v.setting_id,
             'system_id': v.system_id,
             'note': v.note} for v in values]


def get_experiment_ids(session):
    """
    Returns a list of all experiment ids in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    list of int
        The experiment ids in the database.

    """
    return [experiment['id'] for experiment in get_experiments(session)]


def experiment_exists(session, experiment_id):
    """
    Checks if an experiment_id exists in the database.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The id of the experiment.

    Returns
    -------
    bool
        True if the experiment exists, False otherwise.

    """

    if experiment_id in get_experiment_ids(session):
        return True
    else:
        return False


def new_atom_tag(experiment_id, atom_id, property_id, value):
    """
    Returns a new atom tag dictionary to batch insert into the database.

    Parameters
    ----------
    experiment_id : int
        The id of the experiment.
    atom_id : int
        The id of the atom.
    property_id : int
        The id of the property.
    value : float
        The value of the property.

    Returns
    -------
    dict
        The atom tag dictionary.

    """

    return {'experiment_id': experiment_id,
            'atom_id': atom_id,
            'property_id': property_id,
            'value': value}


def new_provenance_dict(experiment_id, frame_id, atom_id, base_type_id):
    """
    Returns a new provenance dictionary to batch insert into the database.

    Parameters
    ----------
    experiment_id : int
        The id of the experiment.
    frame_id : int
        The id of the frame.
    atom_id : int
        The id of the atom.
    base_type_id : int
        The id of the base type.

    Returns
    -------
    dict
        The provenance dictionary.

    """

    return {'experiment_id': experiment_id,
            'frame_id': frame_id,
            'atom_id': atom_id,
            'base_type_id': base_type_id}


def split_chemicals_catalysts(chem_sym, chemical_symbols):
    """
    Splits the a list of chemical symbols into two lists of atom numbers
    for the chemical and catalyst atoms.

    Parameters
    ----------
    chem_sym : list of str
        The chemical symbols of the chemical atoms in the system.
    chemical_symbols : list of str
        The chemical symbols of all atoms in the system ordered by atom number.

    Returns
    -------
    list of int, list of int
        The atom numbers of the chemical and catalyst atoms.

    """

    chem_atom_n, cata_atom_n = [], []
    for atom_number, chemical_symbol in enumerate(chem_sym):
        if chemical_symbol in chemical_symbols:
            chem_atom_n.append(atom_number)
        else:
            cata_atom_n.append(atom_number)

    return chem_atom_n, cata_atom_n


def init_base_type_ids(engine, experiment_id, frame_ids, atom_ids, base_type_id, write_split=100000):
    """
    Initializes the provenances in the database for a base type.

    Parameters
    ----------
    engine : sqlalchemy.engine.Engine
        The engine to the database.
    experiment_id : int
        The id of the experiment.
    frame_ids : list of int
        The ids of all frames in the system.
    atom_ids : list of int
        The ids of all atoms in the system which are of the base type.
    base_type_id : int
        The id of the base type.
    write_split : int, optional
        The number of provenances to write at once.

    """

    meta_data = MetaData(bind=engine)
    MetaData.reflect(meta_data)
    provenances = meta_data.tables['provenances']

    write_provenance_lists = []
    for frame_id in frame_ids:
        provenance_list = [new_provenance_dict(
                            experiment_id,
                            frame_id,
                            atom_id,
                            base_type_id
                            ) for atom_id in atom_ids]
        write_provenance_lists += provenance_list
        if len(write_provenance_lists) > write_split:
            engine.execute(provenances.insert(), write_provenance_lists)
            write_provenance_lists = []

    if len(write_provenance_lists) > 0:
        engine.execute(provenances.insert(), write_provenance_lists)


def get_frame_numbers_dict(session, system_id):
    """
    Gets a dictionary which maps frame ids to frame numbers.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The id of the system.

    Returns
    -------
    dict
        The dictionary which maps frame ids to frame numbers.
    
    Notes
    -----
    frame_numbers_dict[frame_id] = frame_number

    """

    values = session.query(
        Frame.id,
        Frame.frame_number
    ).filter(
        Frame.system_id == system_id
    ).order_by(
        Frame.frame_number
    )
    return {row.id: row.frame_number for row in values}


def get_frames(session, system_id):
    values = session.query(
        Frame
    ).filter(
        Frame.system_id == system_id
    ).order_by(
        Frame.frame_number
    ).all()

    return [{'frame_id': v.id,
             'frame_number': v.frame_number,
             'system_id': v.system_id,
             } for v in values]


def get_frame_ids_dict(session, system_id):
    """
    Gets a dictionary which maps frame numbers to frame ids.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The id of the system.

    Returns
    -------
    dict
        The dictionary which maps frame numbers to frame ids.

    Notes
    -----
    frame_ids_dict[frame_number] = frame_id

    """

    values = session.query(
        Frame.frame_number,
        Frame.id
    ).filter(
        Frame.system_id == system_id
    ).order_by(
        Frame.frame_number
    )
    return {row.frame_number: row.id for row in values}


def get_frame_ids(session, system_id):
    """
    A list of frame ids for a system.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The id of the system.

    Returns
    -------
    list of int
        The frame ids.
    
    """

    values = session.query(
        Frame.id
    ).filter(Frame.system_id == system_id
             ).order_by(Frame.frame_number)
    return [row.id for row in values]


def get_atomic_numbers(session, chemical_symbols):
    """
    Gets the atomic numbers for a list of chemical symbols.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    chemical_symbols : list of str
        The chemical symbols.

    Returns
    -------
    list of int
        The atomic numbers.
    
    """

    chem_sym_set = set(chemical_symbols)
    values = session.query(
        Element.id, Element.chemical_symbol
    ).filter(
        or_(Element.chemical_symbol == v for v in chem_sym_set)
    ).all()
    chem_el_dict = dict((row.chemical_symbol, row.id) for row in values)
    return [chem_el_dict[c] for c in chemical_symbols]


def get_atom_ids_by_elements(session, atomic_numbers, system_id, where_not=False):
    """
    Gets the atom ids for a list of atomic numbers ordered by atom number.
    With the where_not flag, the atoms which are not in the list are returned.
    
    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    atomic_numbers : list of int
        The atomic numbers.
    system_id : int
        The id of the system.
    where_not : bool, optional
        If True, the atoms which are not in the list are returned.

    Returns
    -------
    list of int
        The atom ids.

    """

    if not where_not:
        values = session.query(
            Atom.id
        ).filter(
            or_(Atom.atomic_number == v for v in atomic_numbers),
            Atom.system_id == system_id
        ).order_by(Atom.atom_number)
    else:
        values = session.query(
            Atom.id
        ).filter(
            not_(or_(Atom.atomic_number == v for v in atomic_numbers)),
            Atom.system_id == system_id
        ).order_by(Atom.atom_number)

    return [row.id for row in values]


def get_atom_numbers_by_elements(session, atomic_numbers, system_id, where_not=False):
    """
    Gets the atom numbers for a list of atomic numbers ordered by atom number.
    With the where_not flag, the atoms which are not in the list are returned.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    atomic_numbers : list of int
        The atomic numbers.
    system_id : int
        The id of the system.
    where_not : bool, optional
        If True, the atoms which are not in the list are returned.

    Returns
    -------
    list of int
        The atom numbers.

    """

    if not where_not:
        values = session.query(
            Atom.atom_number
        ).filter(
            or_(Atom.atomic_number == v for v in atomic_numbers),
            Atom.system_id == system_id
        ).order_by(Atom.atom_number)
    else:
        values = session.query(
            Atom.id
        ).filter(
            not_(or_(Atom.atomic_number == v for v in atomic_numbers)),
            Atom.system_id == system_id
        ).order_by(Atom.atom_number)

    return [row.atom_number for row in values]


def get_base_type_ids(session, base_type_names):
    """
    Gets the base type ids for a list of base type names.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    base_type_names : list of str
        The base type names.

    Returns
    -------
    list of int
        The base type ids.
    """
    return [get_base_type_id(session, name) for name in base_type_names]


def get_base_types_dict(session):
    """
    Gets a dictionary of base type ids and names.

    base_type_dict[base_type_id] = 'base_type_name'

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    dict
        The dictionary of base type ids and names.
    """
    return {v.id: v.name for v in session.query(BaseType.id, BaseType.name).all()}


def get_connection_types_dict(session):
    """
    Gets a dictionary of connection type ids and names.

    connection_type_dict[connection_type_id] = 'connection_type_name'

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    dict
        The dictionary of connection type ids and names.
    """
    return {v.id: v.name for v in session.query(ConnectionType.id, ConnectionType.name).all()}

def get_base_type_ids_dict(session):
    """
    Gets a dictionary of base type names and ids.

    base_type_dict['base_type_name'] = base_type_id

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    Returns
    -------
    dict
        The dictionary of base type names and ids.
    """
    return {v.name: v.id for v in session.query(BaseType.name, BaseType.id).all()}


def get_base_type_id(session, name):
    """
    Gets the base type id for a base type name.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    name : str
        The base type name.

    Returns
    -------
    int
        The base type id.
    """

    return session.query(BaseType.id).filter(BaseType.name == name).first().id


def init_base_type_by_chemical_elements(engine, atomic_numbers, experiment_id,
                                        system_id, write_split=100000):
    # TODO:
    # Optimize!
    with Session(engine) as session:
        chem_atom_ids = get_atom_ids_by_elements(session, atomic_numbers, system_id)
        cata_atom_ids = get_atom_ids_by_elements(session, atomic_numbers, system_id, where_not=True)
        frame_ids = get_frame_ids(session, system_id)

        id_lists = [chem_atom_ids, cata_atom_ids]
        base_type_names = ['chemical', 'catalyst']
        base_type_ids = get_base_type_ids(session, base_type_names)

    for atom_ids, base_type_id in zip(id_lists, base_type_ids):
        #print(f'-> Writing BaseType ID {base_type_id}...')
        init_base_type_ids(engine, experiment_id, frame_ids,
                           atom_ids, base_type_id, write_split)

    #print('Done with all provenances.')


def get_base_types(session, experiment_id, frame_id):
    """
    Gets the base types for a frame id of an experiment ordered by atom id.

    base_types_dict['atom_id'] = 'base_type_name'

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The experiment id.
    frame_id : int
        The frame id.
    
    Returns
    -------
    dict
        The dictionary of atom ids and base types.

    """

    values = session.query(
        Provenance.atom_id,
        BaseType.name
    ).select_from(
        join(Provenance, BaseType)
    ).filter(
        Provenance.experiment_id == experiment_id,
        Provenance.frame_id == frame_id
    ).order_by(Provenance.atom_id)

    return {row.atom_id: row.name for row in values}


def get_base_type_atom_ids(session, experiment_id, frame_id, name):
    """
    Gets the atom ids for a base type name of a frame_id and experiment_id ordered by atom number.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The experiment id.
    frame_id : int
        The frame id.
    name : str
        The base type name.

    Returns
    -------
    list
        The list of atom ids.

    """
    base_type_id = get_base_type_id(session, name=name)
    values = session.query(
        Provenance.atom_id,
    ).filter(
        Provenance.experiment_id == experiment_id,
        Provenance.frame_id == frame_id,
        Provenance.base_type_id == base_type_id,
        Atom.id == Provenance.atom_id
    ).order_by(Atom.atom_number)

    return [row.atom_id for row in values]


def get_base_type_atom_numbers(session, experiment_id, frame_id, name):
    """
    Gets the atom numbers for a base_type_name frame_id and experiment_id ordered by atom number.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    experiment_id : int
        The experiment id.
    frame_id : int
        The frame id.
    name : str
        The base type name.

    Returns
    -------
    list
        The list of atom numbers.

    """

    base_type_id = get_base_type_id(session, name=name)
    values = session.query(
        Atom.atom_number
    ).filter(
        Provenance.experiment_id == experiment_id,
        Provenance.frame_id == frame_id,
        Provenance.base_type_id == base_type_id,
        Atom.id == Provenance.atom_id
    ).order_by(Atom.atom_number)

    return [row.atom_number for row in values]


def get_init_base_types(session, property_id, experiment_id):
    values = session.query(
        AtomTag.atom_id, BaseType.name
                    ).filter(
        AtomTag.experiment_id == experiment_id,
        AtomTag.property_id == property_id
                    ).join(
        BaseType, AtomTag.value == BaseType.name
                    ).all()
    init_base_types = {v.atom_id: v.name for v in values}
    return init_base_types


def get_positions_for_frame(session, system_id, frame_id):
    """
    Gets the positions for a frame_id of a system ordered by atom number.
    
    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The system id.
    frame_id : int
        The frame id.

    Returns
    -------
    numpy.ndarray
        The array of positions.

    """
    values = session.query(
        Position.x,
        Position.y,
        Position.z
    ).filter(
        Position.frame_id == frame_id
    ).join(
        Atom,
        Position.atom_id == Atom.id,
    ).filter(
        Atom.system_id == system_id
    ).order_by(Atom.atom_number)
    return np.array([[row.x, row.y, row.z] for row in values])


def get_all_positions_dict_frame_id(session, system_id):
    """
    A dictionary of frame_id to a dictionary of atom_number to position.

    position_dict[frame_id][atom_number] = (x, y, z)

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The system id.
    
    Returns
    -------
    dict
        The dictionary of frame_id to a dictionary of atom_number to position.
    
    """

    values = session.query(
        Frame.id,
        Atom.atom_number,
        Position.x,
        Position.y,
        Position.z
    ).filter(
        Atom.system_id == system_id,
        Frame.system_id == system_id,
        Position.frame_id == Frame.id,
        Position.atom_id == Atom.id,
    ).order_by(Frame.frame_number, Atom.atom_number)

    pos_dict = defaultdict(dict)

    for frame_id, atom_number, x, y, z in values:
        pos_dict[frame_id][atom_number] = (x, y, z)
    return dict(pos_dict)


def get_all_positions_dict(session, system_id):
    """
    Contains all positions of a system. A dictionary of frame_number to a list of positions.

    position_dict[frame_number] = [(x, y, z), (x, y, z), ...]

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The system id.

    Returns
    ------- 
    dict
        The dictionary of frame_number to a list of positions.

    """

    values = session.query(
        Frame.frame_number,
        Atom.atom_number,
        Position.x,
        Position.y,
        Position.z
    ).filter(
        Atom.system_id == system_id,
        Frame.system_id == system_id,
        Position.frame_id == Frame.id,
        Position.atom_id == Atom.id,
    ).order_by(Frame.frame_number, Atom.atom_number)

    pos_dict = defaultdict(list)

    for frame_number, atom_number, x, y, z in values:
        pos_dict[frame_number].append([x, y, z])

    return pos_dict


def get_all_position_ndarray(session, system_id):
    """
    Contains all positions of a system. A numpy array of positions.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    system_id : int
        The system id.

    Returns
    -------
    numpy.ndarray
        The numpy array of positions.

    """

    pos_dict = get_all_positions_dict(session, system_id)
    return np.array([pos_dict[frame_id] for frame_id in sorted(pos_dict.keys())])


def get_atoms_for_system_id(session, system_id):
    values = session.query(
        Atom
    ).filter(
        Atom.system_id == system_id
    ).order_by(Atom.atom_number)

    return [{'atom_id': v.id,
             'atom_number': v.atom_number,
             'atomic_number': v.atomic_number,
             'system_id': v.system_id,
             'init_tag': v.init_tag} for v in values]


def get_atoms_for_surface_id(session, surface_id):
    """
    All atoms for a surface as two lists. One list of atom_ids and one list of atom_numbers.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    surface_id : int
        The surface id.
    
    Returns
    -------
    list
        The list of atom_ids.
    list
        The list of atom_numbers.
    """

    values = session.query(
        Atom.id,
        Atom.atom_number
    ).filter(
        Surface.id == surface_id,
        SurfaceAtom.surface_id == Surface.id,
        HinumaAtom.id == SurfaceAtom.hinuma_atom_id,
        Atom.id == HinumaAtom.atom_id
    ).order_by(Atom.atom_number)
    return [row.id for row in values], [row.atom_number for row in values]


def get_atom_id_dict_for_base_type(session, system_id, base_type_name, frame_id):
    """Returns a dictionary filtered by base_type_name in the form of dict[atom_number] = atom_id"""
    values = session.query(
        Atom.id,
        Atom.atom_number
    ).filter(
        Atom.system_id == system_id,
        BaseType.name == base_type_name,
        Provenance.frame_id == frame_id,
        Provenance.base_type_id == BaseType.id,
        Provenance.atom_id == Atom.id
    ).order_by(Atom.atom_number)
    return {row.atom_number: row.id for row in values}


def new_hinuma_atom(session, hinuma_id, atom_id, hinuma_vec, solid_angle, note='', commit=True):
    hinuma_atom = HinumaAtom(hinuma_id=hinuma_id,
                              atom_id=atom_id,
                              hinuma_vec_x=hinuma_vec[0],
                              hinuma_vec_y=hinuma_vec[1],
                              hinuma_vec_z=hinuma_vec[2],
                              solid_angle=solid_angle,
                              note=note)
    session.add(hinuma_atom)
    if commit:
        session.commit()

    return hinuma_atom.id


def new_hinuma_atom_dict(hinuma_id, atom_id, hinuma_vec, solid_angle, note=None):
    if note is None:
        note = ''
    
    if solid_angle is None:
        return {'hinuma_id': hinuma_id,
                'atom_id': atom_id,
                'hinuma_vec_x': hinuma_vec[0],
                'hinuma_vec_y': hinuma_vec[1],
                'hinuma_vec_z': hinuma_vec[2],
                'note': note}

    elif isinstance(solid_angle, str):
        note += solid_angle

    return {'hinuma_id': hinuma_id,
            'atom_id': atom_id,
            'hinuma_vec_x': hinuma_vec[0],
            'hinuma_vec_y': hinuma_vec[1],
            'hinuma_vec_z': hinuma_vec[2],
            'solid_angle': solid_angle,
            'note': note}


def explore_connections(atom_id, connections_dict, remaining_atom_ids, surface):
    surface.add(atom_id)  # add atom_id to surface set
    if atom_id not in connections_dict or connections_dict[atom_id] is None:
        return remaining_atom_ids, surface
    for new_atom_id in connections_dict[atom_id]:
        if new_atom_id not in surface and new_atom_id in remaining_atom_ids:
            surface.add(new_atom_id)
            remaining_atom_ids.remove(new_atom_id)
            remaining_atom_ids, surface = explore_connections(new_atom_id, connections_dict, remaining_atom_ids, surface)
        
    return remaining_atom_ids, surface 


def get_surfaces_from_connections(connections_dict, remaining_atom_ids=None):
    if remaining_atom_ids is None:
        remaining_atom_ids = list(connections_dict.keys())
    
    surfaces = []

    while remaining_atom_ids:
        atom_id = remaining_atom_ids.pop()
        remaining_atom_ids, surface = explore_connections(atom_id, connections_dict, remaining_atom_ids, set())
        surfaces.append(surface)

    return surfaces


def new_connections_dict(pair_ids, frame_id, experiment_id, connection_type_id):
    '''
    Returns connections ready to be inserted into the database 
    with fast_write_dicts().
    '''
    connections = []
    for pair_id in pair_ids:
        connections.append({'pair_id': pair_id,
                            'frame_id': frame_id,
                            'experiment_id': experiment_id,
                            'connection_type_id': connection_type_id})
    return connections


def new_hinuma_connections_dict(connections, hinuma_atoms_dict):
    '''
    Returns hinuma_connections ready to be inserted into the database 
    with fast_write_dicts().
    '''
    hinuma_connections = []
    for atom_id, connections_list in connections.items():
        if not connections_list or connections_list is None:
                continue
        for connecting_atom_id in connections_list:
            if atom_id == connecting_atom_id:
                continue
            hinuma_connections.append({'hinuma_atom_id': hinuma_atoms_dict[atom_id],
                                       'atom_id': atom_id,
                                       'connecting_atom_id': connecting_atom_id})
    return hinuma_connections


def get_hinuma_id(session, experiment_id):
    value = session.query(
        Hinuma.id
    ).filter(
        Hinuma.experiment_id == experiment_id
    ).one()
    return value.id


def get_hinuma_ids(session, experiment_id):
    rows = session.query(
        Hinuma.id
    ).filter(
        Hinuma.experiment_id == experiment_id
    ).all()
    return [r.id for r in rows]


def get_hinuma_atoms(session, hinuma_id):
    values = session.query(
        HinumaAtom.atom_id,
        Atom.atom_number
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id
    ).filter(Atom.id == HinumaAtom.atom_id
             ).order_by(Atom.atom_number)
    return [row.atom_id for row in values], [row.atom_number for row in values]


def get_hinuma_atoms_dict(session, hinuma_id):
    '''
    Returns a dictionary of atom_id: hinuma_atom_id
    '''
    values = session.query(
        HinumaAtom.id,
        HinumaAtom.atom_id
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id
    ).join(
        Atom, Atom.id == HinumaAtom.atom_id
    ).order_by(Atom.atom_number)
    
    return {v.atom_id: v.id for v in values}


def get_hinuma_atoms_with(session, hinuma_id, solid_angle_limit):
    values = session.query(
        HinumaAtom.atom_id,
        Atom.atom_number
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id,
        HinumaAtom.solid_angle > solid_angle_limit,
        Atom.id == HinumaAtom.atom_id
    ).order_by(Atom.atom_number)
    return [row.atom_id for row in values], [row.atom_number for row in values]


def get_atom_chem_symbols(session, system_id):
    values = session.query(
        Element.chemical_symbol
    ).filter(
        Atom.system_id == system_id,
        Element.id == Atom.atomic_number
    ).order_by(Atom.atom_number)
    return [atom.chemical_symbol for atom in values]


def get_atomic_numbers_atom_id_dict(session, system_id):
    values = session.query(
        Atom.id,
        Atom.atomic_number
    ).filter(
        Atom.system_id == system_id
    ).order_by(Atom.atom_number)
    return {v.id: v.atomic_number for v in values}


def get_atomic_numbers_from_system(session, system_id):
    values = session.query(
        Element.id
    ).filter(
        Atom.system_id == system_id,
        Element.id == Atom.atomic_number
    ).order_by(Atom.atom_number)
    return [v.id for v in values]


def get_catalyst_chemical_tag_lists(session, experiment_id, frame_number):
    tag_lists = []
    base_type_names = ['chemical', 'catalyst']
    for base_type_name in base_type_names:
        values = session.query(
            Atom.atom_number
        ).filter(
            BaseType.name == base_type_name,
            Frame.frame_number == frame_number,
            Provenance.experiment_id == experiment_id,
            Provenance.frame_id == Frame.id,
            Provenance.base_type_id == BaseType.id,
            Atom.id == Provenance.atom_id,
            Atom.system_id == Frame.system_id
        ).order_by(Atom.atom_number)

        tag_lists.append([row.atom_number for row in values])
    return tag_lists


def build_ase_frames(chem_sym, pos_lists, cell, pbc, tags):
    frames = []
    for pos in pos_lists:
        frames.append(ase.Atom(chem_sym,
                                positions=pos,
                                cell=cell,
                                pbc=pbc,
                                tags=tags))
    return frames


def view_ase_frames_engine(engine, system_id):
    with Session(engine) as session:
        frames = generate_ase_frames(session, system_id)

    view(frames)
  
def generate_ase_frames(session, system_id):
    pos_lists = get_all_position_ndarray(session, system_id)
    chem_sym = get_atom_chem_symbols(session, system_id)
    system = get_system(session, system_id)
    cell = system['cell']
    pbc = system['pbc']
    tags = system['tags']

    return build_ase_frames(chem_sym, pos_lists, cell, pbc, tags)


def get_hinuma_frame_id(session, hinuma_id):
    frame_id = session.query(
        Hinuma.frame_id
    ).filter(Hinuma.id == hinuma_id
             ).one().frame_id
    return frame_id


def view_frames(session, experiment_id, system_id, hinuma_id, solid_angle_limit):
    frames = generate_ase_frames(session, system_id)
    hinuma_frame_id = get_hinuma_frame_id(session, hinuma_id)
    chem_ids, cat_ids = get_catalyst_chemical_tag_lists(session, experiment_id, hinuma_frame_id)
    hinuma_atom_ids, hinuma_atom_numbers = get_hinuma_atoms_with(session,
                                                                 hinuma_id,
                                                                 solid_angle_limit)

    view_tag_lists_from_ids_frames(frames, [chem_ids, cat_ids, hinuma_atom_numbers])


def get_positions_atom_number(session, system_id, frame_id, atom_numbers):
    values = session.query(
        Position.x,
        Position.y,
        Position.z
    ).filter(
        Atom.system_id == system_id,
        or_(Atom.atom_number == v for v in atom_numbers),
        Position.frame_id == frame_id,
        Position.atom_id == Atom.id
    ).order_by(Atom.atom_number)
    return np.array([(v.x, v.y, v.z) for v in values])


def new_surface_group(session, experiment_id, surface_type_id, hinuma_id=None, solid_angle_limit=None, commit=True):
    surface_group_dict = {
        'experiment_id': experiment_id,
        'surface_type_id': surface_type_id,
    }
    
    if hinuma_id is not None:
        surface_group_dict['hinuma_id'] = hinuma_id

    if solid_angle_limit is not None:
        surface_group_dict['solid_angle_limit'] = solid_angle_limit

    entry = SurfaceGroup(**surface_group_dict)

    session.add(entry)
    if commit:
        session.commit()

    return entry.id


def new_surface_id(session, surface_group_id, commit=True):
    entry = Surface(
        surface_group_id=surface_group_id)

    session.add(entry)
    if commit:
        session.commit()

    return entry.id


def new_surface_atom_dict(surface_id, hinuma_atom_id):
    return {'surface_id': surface_id,
            'hinuma_atom_id': hinuma_atom_id}


def get_atom_numbers_hinuma_atom_id(session, hinuma_id):
    """
    Returns a dictionary in the format:
    dict[atom_number] = hinuma_atom_id
    """
    values = session.query(
        HinumaAtom.id,
        Atom.atom_number
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id,
        Atom.id == HinumaAtom.atom_id
    ).order_by(
        Atom.atom_number
    )
    return {v.atom_number: v.id for v in values}


def get_hinuma_vectors(session, hinuma_id, atom_numbers=False):
    if atom_numbers:
        return get_hinuma_vectors_from_atom_numbers(session, hinuma_id, atom_numbers)
    return get_hinuma_vectors_all(session, hinuma_id)


def get_hinuma_vectors_all(session, hinuma_id):
    values = session.query(
        HinumaAtom.hinuma_vec_x,
        HinumaAtom.hinuma_vec_y,
        HinumaAtom.hinuma_vec_z,
        Atom.atom_number
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id,
        Atom.id == HinumaAtom.atom_id
    ).order_by(Atom.atom_number)

    hinuma_vec_dict = {v.atom_number: np.array((v.hinuma_vec_x,
                                                v.hinuma_vec_y,
                                                v.hinuma_vec_z)) for v in values}

    return hinuma_vec_dict
def get_solid_angles_all(session, hinuma_id):
    values = session.query(
        HinumaAtom.solid_angle,
        Atom.atom_number
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id,
        Atom.id == HinumaAtom.atom_id
    ).order_by(Atom.atom_number)

    solid_angles_dict = {v.atom_number: v.solid_angle for v in values}

    return solid_angles_dict


def get_hinuma_vectors_from_atom_numbers(session, hinuma_id, atom_numbers):
    values = session.query(
        HinumaAtom.hinuma_vec_x,
        HinumaAtom.hinuma_vec_y,
        HinumaAtom.hinuma_vec_z
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id,
        Atom.id == HinumaAtom.atom_id,
        or_(Atom.atom_number == v for v in atom_numbers)
    ).order_by(Atom.atom_number)

    hinuma_vec = np.array([(v.hinuma_vec_x,
                            v.hinuma_vec_y,
                            v.hinuma_vec_z) for v in values])

    return hinuma_vec


def get_simple_surface_vec(hinuma_vec):
    average_vec = np.average(hinuma_vec, axis=0)
    norm_vec = average_vec / np.linalg.norm(average_vec)
    return np.round(norm_vec, 2)


def get_related_ids_hinuma(session, hinuma_id):
    values = session.query(
        Hinuma.experiment_id,
        Hinuma.frame_id,
        Frame.system_id
    ).filter(
        Hinuma.id == hinuma_id,
        Frame.id == Hinuma.frame_id
    ).one()
    return {'hinuma_id': hinuma_id,
            'experiment_id': values.experiment_id,
            'frame_id': values.frame_id,
            'system_id': values.system_id}


def simple_surface_plane(pos, hinuma_vec, atom_numbers):
    normal_vec = get_simple_surface_vec(hinuma_vec)

    n_point_max = atom_numbers[np.argmax(np.dot(pos, normal_vec))]
    n_point_min = atom_numbers[np.argmin(np.dot(pos, normal_vec))]

    return normal_vec, n_point_max, n_point_min


def get_surface_type_id(session, surface_type_name):
    value = session.query(
        SurfaceType.id
    ).filter(
        SurfaceType.name == surface_type_name).one()

    return value.id


def get_surface_type_ids(session):
    values = session.query(
        SurfaceType.name,
        SurfaceType.id
    ).all()

    return {v.name: v.id for v in values}


def get_surface_types_dict(session):
    values = session.query(
        SurfaceType.name,
        SurfaceType.id
    ).all()

    return {v.id: v.name for v in values}


def new_surface_parameter_list(surface_id, parameters, parameter_ids_dict):
    surface_parameters = []
    for parameter, value in parameters.items():
        surface_parameters.append(
            {'surface_id': surface_id,
             'parameter_id': parameter_ids_dict[parameter],
             'value': value})
    return surface_parameters


def get_pos_ids_dict_for_frame(session, system_id, frame_id):
    values = session.query(
        Position.id,
        Atom.atom_number
    ).filter(
        Atom.system_id == system_id,
        Position.frame_id == frame_id,
        Position.atom_id == Atom.id
    ).order_by(Atom.atom_number)
    return {v.atom_number: v.id for v in values}


def get_pos_ids_dict_for_atom_numbers(session, atom_numbers, system_id, frame_id):
    values = session.query(
        Position.id,
        Atom.atom_number
    ).filter(
        Atom.system_id == system_id,
        or_(Atom.atom_number == v for v in atom_numbers),
        Position.frame_id == frame_id,
        Position.atom_id == Atom.id
    ).order_by(Atom.atom_number)
    return {v.atom_number: v.id for v in values}


def get_position_from_id(session, position_id):
    value = session.query(
        Position.x,
        Position.y,
        Position.z
    ).filter(
        or_(Position.id == position_id)
    ).one()

    return np.array((value.x, value.y, value.z))


def get_positions_from_ids(session, position_ids):
    values = session.query(
        Position.x,
        Position.y,
        Position.z
    ).filter(
        or_(Position.id == v for v in position_ids),
        Atom.id == Position.atom_id
    ).order_by(
        Atom.atom_number
    )
    return np.array([(v.x, v.y, v.z) for v in values])


def get_positions_from_atom_ids(session, atom_ids, frame_id):
    values = session.query(
        Position.x,
        Position.y,
        Position.z
    ).filter(
        or_(Atom.id == v for v in atom_ids),
        Position.frame_id == frame_id,
        Position.atom_id == Atom.id
    ).order_by(
        Atom.atom_number
    )
    return np.array([(v.x, v.y, v.z) for v in values])


def get_parameter_ids_dict(session):
    values = session.query(
        Parameter.id,
        Parameter.name).all()
    return {v.name: v.id for v in values}


def get_surface_ids(session, surface_group_ids):
    if isinstance(surface_group_ids, int):
        surface_group_ids = [surface_group_ids]
    values = session.query(
        Surface.id
    ).filter(
        or_(Surface.surface_group_id == v for v in surface_group_ids)
    ).order_by(
        Surface.id)
    return [v.id for v in values]


# NEEDED?
def get_surface_from_id(session, surface_id):
    values = session.query(
        Surface.id,
        SurfaceGroup.solid_angle_limit,
        SurfaceGroup.surface_type_id,
        SurfaceType.name
    ).join(
        SurfaceGroup, SurfaceGroup.id == Surface.surface_group_id
    ).filter(
        Surface.id == surface_id,
        SurfaceType.id == SurfaceGroup.surface_type_id
    ).one()
    return {'surface_id': values.id,
            'solid_angle_limit': values.solid_angle_limit,
            'surface_type_id': values.surface_type_id,
            'surface_type_name': values.name}


def get_surface_group(session, surface_group_id):
    v = session.query(
        SurfaceGroup.id,
        SurfaceGroup.experiment_id,
        SurfaceGroup.hinuma_id,
        SurfaceGroup.solid_angle_limit,
        SurfaceGroup.surface_type_id,
        SurfaceType.name
    ).filter(
        SurfaceGroup.id == surface_group_id,
    ).join(SurfaceType, SurfaceType.id == SurfaceGroup.surface_type_id
    ).one()
    return {'surface_group_id': v.id,
            'experiment_id': v.experiment_id,
            'hinuma_id': v.hinuma_id,
            'solid_angle_limit': v.solid_angle_limit,
            'surface_type_id': v.surface_type_id,
            'surface_type_name': v.name}


def get_surface_groups(session, experiment_id):
    values = session.query(
        SurfaceGroup.id,
        SurfaceGroup.experiment_id,
        SurfaceGroup.hinuma_id,
        SurfaceGroup.solid_angle_limit,
        SurfaceGroup.surface_type_id,
        SurfaceType.name,
    ).filter(
        SurfaceGroup.experiment_id == experiment_id,
    ).join(
        SurfaceType, SurfaceType.id == SurfaceGroup.surface_type_id
    ).all()
    
    return [{'surface_group_id': v.id,
             'experiment_id': v.experiment_id,
             'hinuma_id': v.hinuma_id,
             'solid_angle_limit': v.solid_angle_limit,
             'surface_type_id': v.surface_type_id,
             'surface_type_name': v.name} for v in values]


def get_surface_group_ids(session, experiment_ids):
    values = session.query(
        SurfaceGroup.id
    ).filter(
        or_(SurfaceGroup.experiment_ids == v for v in experiment_ids)
    ).all()

    return [v.id for v in values]


def get_surface_parameters(session, surface_id):
    #Duplicate of get_surface_parameters_for_surface_id
    values = session.query(
        Parameter.name,
        Parameter.datatype,
        SurfaceParameter.value
    ).filter(
        SurfaceParameter.surface_id == surface_id,
        Parameter.id == SurfaceParameter.parameter_id)

    return {v.name: type_conversion(v.value, v.datatype) for v in values}


def get_surface_ids_for_surface_group_id(session, surface_group_id):
    values = session.query(
        Surface.id,
    ).filter(
        Surface.surface_group_id == surface_group_id
    ).order_by(
        Surface.id
    ).all()
    return [v.id for v in values]


def get_surface_parameters_for_surface_id(session, surface_id):
    values = session.query(
            SurfaceParameter,
            Parameter
        ).filter(
            SurfaceParameter.surface_id == surface_id
        ).join(
            Parameter, SurfaceParameter.parameter_id == Parameter.id
        ).order_by(
            SurfaceParameter.id
        ).all()
    return {p.Parameter.name: type_conversion(p.SurfaceParameter.value, p.Parameter.datatype) for p in values}


def get_surface_atoms_for_surface_id(session, surface_id):
    values = session.query(
        SurfaceAtom,
        Atom
    ).filter(
        SurfaceAtom.surface_id == surface_id
    ).join(
        Atom, SurfaceAtom.atom_id == Atom.id
    ).order_by(
        Atom.atom_number
    ).all()
    return {a.Atom.id for a in values}


def get_2d_plane_parameters(session, surface_id):
    surface_parameters = get_surface_parameters(session, surface_id)
    normal_vec = np.array((surface_parameters['normal_vec_x'],
                           surface_parameters['normal_vec_y'],
                           surface_parameters['normal_vec_z']))
    pos_vec_max = get_position_from_id(session, surface_parameters['position_id_max'])
    pos_vec_min = get_position_from_id(session, surface_parameters['position_id_min'])

    return normal_vec, pos_vec_max, pos_vec_min


def get_plane_point_distance(normal_vec, plane_point, point):
    return np.dot(normal_vec, point - plane_point)


def calc_pair_distances_frames(pos_dict, approach_dict, surface_atoms, settings, system, **kwargs):
    pair_distances_frames = {}
    for frame_id, atoms_on_surface in approach_dict.items():
        temp_dict = {}
        pos = np.array([v for v in pos_dict[frame_id].values()])
        for surface_id, approach_atom_numbers in atoms_on_surface.items():
            cat_atom_numbers = list(surface_atoms[surface_id])
            temp_dict[surface_id] = get_distance_pairs(pos, cat_ids=cat_atom_numbers,
                                                       chem_ids=list(approach_atom_numbers),
                                                       bond_cutoff=settings['bond_cutoff'],
                                                       box=system['box'])
        pair_distances_frames[frame_id] = temp_dict
    return pair_distances_frames


def write_pair_distances(engine, pair_distances_frames, atom_id_convert, **kwargs):
    pair_ids = {}
    write_distances = []

    with Session(engine) as session:
        for iterlist in pair_generator(pair_distances_frames):
            frame_id, surface_id, chem_atom_number, cat_atom_number, distance = iterlist
            atom_id_1 = atom_id_convert[cat_atom_number]
            atom_id_2 = atom_id_convert[chem_atom_number]
            pair = tuple(sorted([atom_id_1, atom_id_2]))
            if pair not in pair_ids:
                pass  # TODO: write new pair check function!
                pair_ids[pair] = new_pair(session, atom_id_1, atom_id_2)
            write_distances.append({
                'pair_id': pair_ids[pair],
                'frame_id': frame_id,
                'distance': distance})
        session.bulk_insert_mappings(Distance, write_distances)
        session.commit()

        return write_distances, pair_ids


def pair_generator(pair_distances_frames):
    for frame_id, pair_distances in pair_distances_frames.items():
        for surface_id, pairs in pair_distances.items():
            for chem_atom_number, cat_dist in pairs.items():
                for cat_atom_number, distance in cat_dist:
                    yield frame_id, surface_id, chem_atom_number, cat_atom_number, distance


def get_provenance_ids(session, experiment_id, frame_id, atom_ids):
    values = session.query(
        Provenance.id
    ).filter(
        or_(Atom.id == a for a in atom_ids)
    ).filter(
        Provenance.experiment_id == experiment_id,
        Provenance.frame_id == frame_id,
        Provenance.atom_id == Atom.id
    ).order_by(
        Atom.atom_number
    )
    return [v.id for v in values]


def get_provenances(session, experiment_id):
    values = session.query(
        Provenance
    ).filter(
        Provenance.experiment_id == experiment_id
    ).all()

    return [{'provenance_id': v.id,
             'experiment_id': v.experiment_id,
             'frame_id': v.frame_id,
             'atom_id': v.atom_id,
             'base_type_id': v.base_type_id,
             } for v in values]


def get_provenances_iter(session, experiment_id):
    values = session.query(
        Provenance
    ).filter(
        Provenance.experiment_id == experiment_id
    ).all()

    for v in values:
        yield {'provenance_id': v.id,
               'experiment_id': v.experiment_id,
               'frame_id': v.frame_id,
               'atom_id': v.atom_id,
               'base_type_id': v.base_type_id,
               }


def get_property_ids_dict(session):
    values = session.query(
        Property.id,
        Property.name).all()
    return dict([(v.name, v.id) for v in values])


def get_properties_dict(session):
    values = session.query(
        Property.id,
        Property.name,
        Property.display_name,
        Property.description,
        Property.datatype,
        ).all()
    return dict([(v.id, {
        'id': v.id,
        'name': v.name,
        'display_name': v.display_name,
        'description': v.description,
        'datatype': v.datatype,
        }) for v in values])


def new_pair(session, atom_id_1, atom_id_2, commit=True):
    entry = Pair(
        atom_id_1=atom_id_1,
        atom_id_2=atom_id_2)

    session.add(entry)
    if commit:
        session.commit()

    return entry.id


def get_approach_dict(session, surface_id, property_name='in_approach_to'):
    values = session.query(
        Atom.atom_number,
        Provenance.frame_id,
        ProvenanceProperty.value
    ).filter(
        Property.name == property_name,
        ProvenanceProperty.property_id == Property.id,
        ProvenanceProperty.provenance_id == Provenance.id,
        Frame.id == Provenance.frame_id,
        Atom.id == Provenance.atom_id,
        cast(ProvenanceProperty.value, Integer) == surface_id,
    ).order_by(Frame.id, Atom.atom_number)

    approach_dict = defaultdict(list)

    for v in values:
        approach_dict[int(v.frame_id)].append(int(v.atom_number))

    return dict(approach_dict)


def get_atom_surface_distance(session, atom_id, surface_id):
    values = session.query(
        SurfaceDistance.distance
    ).filter(
        SurfaceDistance.surface_id == surface_id,
        SurfaceDistance.atom_id == atom_id,
        Frame.id == SurfaceDistance.frame_id
    ).order_by(Frame.frame_number)
    return [v.distance for v in values]


def get_pair_ids_from_system(session, system_id):
    values = session.query(
        Pair.id,
        Pair.atom_id_1,
        Pair.atom_id_2,
    ).filter(
        Atom.system_id == system_id,
        or_(Pair.atom_id_1 == Atom.id,
            Pair.atom_id_2 == Atom.id,)
    ).order_by(
        Pair.id
    )
    pair_ids = {}
    for row in values:
        pair_ids[tuple(sorted((int(row.atom_id_1), int(row.atom_id_2))))] = int(row.id)

    return pair_ids


def convert_surface_classifier(solid_angles, r_dashes, connections, distances_dict, atom_ids, all_ids=None):

    solid_angles_dict = dict(zip(atom_ids, solid_angles))
    r_dashes_dict = dict(zip(atom_ids, r_dashes))
    
    if all_ids is None:
        all_ids = atom_ids

    connections_dict = convert_connections(connections, atom_ids, all_ids)
    pair_distances = {}
    for i, dists in distances_dict.items():
        for j, dist in dists.items():
            pair_distances[tuple(sorted([all_ids[i], all_ids[j]]))] = dist
    return solid_angles_dict, r_dashes_dict, connections_dict, pair_distances


def get_heterogeneous_connections(session, experiment_id, connection_type_id, chemical_ids, catalyst_ids):
    values = session.query(
        Connection,
        Pair,
            ).filter(
        Connection.experiment_id == experiment_id,
        Connection.connection_type_id == connection_type_id,
            ).join(
        Pair, Connection.pair_id == Pair.id
            ).all()

    connected_type = defaultdict(lambda: defaultdict(set))
    for value in values:
        frame_id = value.Connection.frame_id
        atom_id_1 = value.Pair.atom_id_1
        atom_id_2 = value.Pair.atom_id_2
        if atom_id_1 in chemical_ids and atom_id_2 in catalyst_ids:
            connected_type[frame_id][atom_id_1].add(atom_id_2)
        elif atom_id_2 in chemical_ids and atom_id_1 in catalyst_ids:
            connected_type[frame_id][atom_id_2].add(atom_id_1)
        else:
            raise ValueError('Atom ids not found in experiment')
    return connected_type


def convert_connections(connections, atom_ids, all_ids=None):
    '''
    Converts the connections list of lists to a dictionary of lists
    Schema: {atom_id: [atom_id, ...], ...}
    '''
    if all_ids is None:
        print('all_ids is None at convert_connections')
        all_ids = atom_ids
    connections_dict = defaultdict(list)
    for atom_id, links in zip(atom_ids, connections):
        if links is None or not links:
            connections_dict[atom_id] = None
        else:
            connections_dict[atom_id] = set([all_ids[i] for i in links])
    return dict(connections_dict)


def get_hinumas_of_experiment(session, experiment_id):
    values = session.query(
        Hinuma
    ).filter(
        Hinuma.experiment_id == experiment_id
    ).all()
    return {v.id: {'hinuma_id': v.id,
                   'experiment_id': v.experiment_id,
                   'frame_id': v.frame_id,
                   'base_type_id': v.base_type_id,
                   } for v in values}


def get_hinuma_connections(session, hinuma_id):
    '''
    Load Hinuma connections for a given Hinuma ID
    Schema: dict[atom_id] = set[connecting_atom_id]
    '''
    values = session.query(
        HinumaConnection,
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id
    ).join(
        HinumaAtom, HinumaAtom.id == HinumaConnection.hinuma_atom_id
    ).all()

    connections = defaultdict(set)
    for v in values:
        a_1, a_2 = v.atom_id, v.connecting_atom_id
        connections[a_1].add(a_2)
        connections[a_2].add(a_1)
    return dict(sorted(connections.items(), key=lambda x: x[0]))


def get_hinuma_values(session, hinuma_id):    
    values = session.query(
        HinumaAtom
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id
    ).join(
        Atom, Atom.id == HinumaAtom.atom_id
    ).order_by(
        Atom.atom_number
    ).all()

    solid_angles = {}
    r_dashes = {}

    for v in values:
        solid_angles[v.atom_id] = v.solid_angle
        r_dashes[v.atom_id] = np.array((v.hinuma_vec_x, v.hinuma_vec_y, v.hinuma_vec_z)) 

    return solid_angles, r_dashes


def get_hinuma_solid_angles(session, hinuma_id):    
    values = session.query(
        HinumaAtom.solid_angle,
        HinumaAtom.atom_id,
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id
    ).join(
        Atom, Atom.id == HinumaAtom.atom_id
    ).order_by(
        Atom.atom_number
    ).all()

    solid_angles = {}

    for v in values:
        solid_angles[v.atom_id] = v.solid_angle

    return solid_angles


def get_hinuma_r_dashes(session, hinuma_id):    
    values = session.query(
        HinumaAtom.hinuma_vec_x,
        HinumaAtom.hinuma_vec_y,
        HinumaAtom.hinuma_vec_z,
        HinumaAtom.atom_id,
    ).filter(
        HinumaAtom.hinuma_id == hinuma_id
    ).join(
        Atom, Atom.id == HinumaAtom.atom_id
    ).order_by(
        Atom.atom_number
    ).all()

    r_dashes = {}

    for v in values:
        r_dashes[v.atom_id] = np.array((v.hinuma_vec_x, v.hinuma_vec_y, v.hinuma_vec_z)) 

    return r_dashes


def get_hinuma_dicts(session, experiment_id):

    return get_hinumas_of_experiment(session, experiment_id)


def get_event_types(session):
    values = session.query(
        EventType.name,
        EventType.id
    ).all()
    return {value.name: value.id for value in values}


def get_connected_pairs_covalent(session, system_id, fudge_factor):
    a1 = aliased(Atom)
    a2 = aliased(Atom)
    e1 = aliased(Element)
    e2 = aliased(Element)
    values = session.query(
        Pair.id,
        Distance.frame_id,
        a1.atom_number,
        e1.chemical_symbol,
        e1.covalent_radius_pyykko,
        a2.atom_number,
        e2.chemical_symbol,
        e2.covalent_radius_pyykko,
        Distance.distance,
        fudge_factor * (e1.covalent_radius_pyykko + e2.covalent_radius_pyykko)
    ).join(
        Distance, Distance.pair_id == Pair.id
    ).join(
        a1, a1.id == Pair.atom_id_1
    ).join(
        e1, e1.id == a1.atomic_number
    ).join(
        a2, a2.id == Pair.atom_id_2
    ).join(
        e2, e2.id == a2.atomic_number
    ).filter(
        Distance.distance <= fudge_factor * (e1.covalent_radius_pyykko + e2.covalent_radius_pyykko),
        a1.system_id == system_id,
        a2.system_id == system_id
    ).order_by(
        Distance.frame_id, a1.atom_number, a2.atom_number
    ).all()
    d = defaultdict(dict)

    for row in values:
        if row[1] not in d[row[5]]:
            d[row[5]][row[1]] = []
        d[row[5]][row[1]].append((row[2], row[8]))
    return dict(d)


def fast_write_dicts(engine, table_name, values):
    meta_data = MetaData(bind=engine)
    MetaData.reflect(meta_data)
    engine.execute(meta_data.tables[table_name].insert(), values)
    return True


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    it = iter(lst)
    while True:
        chunk = list(islice(it, n))
        if not chunk:
            return
        yield chunk


if __name__ == "__main__":
    pass
