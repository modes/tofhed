# Copyright 2023 MODES, Thomas Bakaj
# (see accompanying license files for details).

"""
'tofhed' is a tool for handling heterogeneous atom trajectories. Its functionalities include:
- Importing atom trajectories (from xyz, extxyz, cif, etc.)
- Building a SQL database for data collection and subsequent analysis (only SQLite and Postgres have been tested)
- Analysing frames using the Hinuma algorithm
- Identifying surface atoms and their connectivities
- Finding connected atoms based on their Pyykko-Distance
- Detecting events such as pinging, adsorption, and desorption

**************************************************************************************
WARNING: This package is still in development and is not yet ready for production use.
**************************************************************************************

"""

import sys


if sys.version_info[0] == 2:
    raise ImportError('This Package requires Python3. This is Python2.')

__author__ = 'MODES, Thomas Bakaj'
__version__ = '0.1.2'

from . import analysis, hinuma, utils, db, future
from .functions import *
from .db import create_db

if __name__ == '__main__':
    pass