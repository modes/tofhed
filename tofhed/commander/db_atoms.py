from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *


class DBAtoms(SuperDict):
    def __init__(self, system, *args, **kwargs):
        self.system = system
        self.db = system.db
        super().__init__(*args, **kwargs)
        self.update(self._get_atoms())
        self.number_to_id = {a.atom_number: a.atom_id for a in self}
        self.id_to_number = {a.atom_id: a.atom_number for a in self}
        self._atomic_numbers = None

    def _get_atoms(self):
        with self.db.Session() as session:
            return {a['atom_id']: DBAtom(a, self.system) for a in get_atoms_for_system_id(session, self.system.system_id)}
    
    def atoms_sorted_by_number(self):
        for atom_number in sorted(self.number_to_id.keys()):
            yield self[self.number_to_id[atom_number]]

    def ids_sorted_by_number(self):
        for atom_number in sorted(self.number_to_id.keys()):
            yield self.number_to_id[atom_number]
    
    @property
    def ids(self):
        return list(self.keys())

    @property
    def atomic_numbers(self):
        if self._atomic_numbers is None:
            self._atomic_numbers = {a.atom_id: a.atomic_number for a in self}
        return self._atomic_numbers

    def ids_to_numbers(self, ids):
        return [self.id_to_number[atom_id] for atom_id in ids]
    
    def numbers_to_ids(self, numbers):
        return [self.number_to_id[atom_number] for atom_number in numbers]
    
    @property
    def numbers(self):
        return list(self.number_to_id.keys())
    

class DBAtom:
    def __init__(self, atom_dict, system):
        self.system = system
        self.__dict__.update(atom_dict)

    @property
    def id(self):
        return self.atom_id

    @property
    def number(self):
        return self.atom_number

    def base_type(self, experiment_id):
        return self.system.db.experiments[experiment_id].base_types_dict[self.atom_id]

    def __repr__(self):
        return f'atom_id={self.atom_id}, atom_number={self.atom_number}'
