class SuperDict(dict):
    """
    SuperDict is an enhanced dictionary that allows for initialization with keyword arguments
    and provides an iterator that yields its values.

    Usage:
        s = SuperDict({'a': 1, 'b': 2}, c=3, d=4)
        print(s)  # {'a': 1, 'b': 2, 'c': 3, 'd': 4}
        for value in s:
            print(value)  # 1, 2, 3, 4

    Note:
        The iterator for SuperDict yields values, not keys.
    """

    def __init__(self, input_dict=None, *args, **kwargs):
        """
        Initialize a SuperDict.

        Parameters:
            input_dict (dict): A dictionary to initialize the SuperDict with.
            *args: Arguments passed to the parent dict class.
            **kwargs: Key-value pairs to be added to the SuperDict.
        """
        super().__init__(*args)
        if input_dict:
            self.update(input_dict)
        for k, v in kwargs.items():
            self.__setattr__(k, v)
    
    def __iter__(self):
        """
        Return an iterator that yields the values of the SuperDict.

        Returns:
            iterator: An iterator over the values of the SuperDict.
        """
        return (value for value in self.values())



class RetryProperty(property):
    """
    A custom property decorator that provides retry logic for database lookups.
    
    If a KeyError is encountered during the initial attempt to retrieve a value,
    the database is refreshed, and a second attempt is made. If the second attempt
    also fails, a custom KeyError is raised.
    
    Usage:
        class YourClass:
            @RetryProperty
            def some_attribute(self):
                return self.db.data[self.some_id]

    Note:
        This property decorator assumes the presence of a `db` object with a 
        `refresh()` method on the instance it's used in.
    """

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        try:
            return super().__get__(obj, objtype)
        except KeyError:
            print(obj.db)  # Debugging output
            obj.db.refresh()
            try:
                return super().__get__(obj, objtype)
            except KeyError:
                raise KeyError(f'{self.fget.__name__} with id={getattr(obj, self.fget.__name__ + "_id")} not found in database')
