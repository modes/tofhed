from tofhed.commander.super_dict import SuperDict, RetryProperty
from tofhed.functions import *
from tofhed.commander.db_events import DBEvents
from tofhed.commander.db_provenances import DBProvenances
from tofhed.commander.db_hinumas import DBHinumas
from tofhed.commander.db_surfaces import DBSurfaceGroups
from tofhed.hinuma import surface_classifier, surface_classifier_selected
from tofhed.analysis import debug_view_split, debug_view_surfaces, matplotlib_debug_viz, matplotlib_hinuma_viz
import os
from multiprocessing import Pool, cpu_count


class DBExperiments(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_experiments())

    @property
    def ids(self):
        return list(self.keys())
    
    def _get_experiments(self):
        with self.db.Session() as session:
            return {e['id']: DBExperiment(e['id'], e, self.db) for e in get_experiments(session)}
    
    def new(self, setting_id, system_id, note='', set_base_types=True, commit=True):
        return self.new_experiment(setting_id, system_id, note=note, set_base_types=set_base_types, commit=commit)

    def new_experiment(self, setting_id, system_id, note='', set_base_types=True, commit=True):
        with self.db.Session() as session:
            experiment_id = new_experiment(session, setting_id, system_id, note=note, commit=commit)
        
        self.update(self._get_experiments())
        if set_base_types:
            self[experiment_id].set_base_types()
        
        return self[experiment_id]


def calculate_hinuma_standalone(input):
    '''Standalone function for multiprocessing. See DBExperiment._calculate_hinuma_parallel()'''
    
    hinuma_kwargs, atom_ids = input
    
    solid_angles, r_dashes, connection_lists, distances_dict = surface_classifier(**hinuma_kwargs)

    return convert_surface_classifier(solid_angles, r_dashes, connection_lists, distances_dict, atom_ids)


class DBExperiment:
    def __init__(self, experiment_id, experiment_dict, db):
        self.experiment_id = experiment_id
        self.db = db
        self.__dict__.update(experiment_dict)
        self._provenances = None
        self._catalyst_ids = None
        self._chemical_ids = None
        self._base_types_dict = None
        self._hinumas = None
        self._surface_groups = None
        self.events = DBEvents(self)
        self.is_sequence = self.system.is_sequence
        self.view = ViewExperiment(self)

    @property
    def id(self):
        return self.experiment_id

    @property
    def provenances(self):
        if self._provenances is None:
            self._provenances = DBProvenances(self)
        return self._provenances

    @RetryProperty
    def settings(self):
        return self.db.settings[self.setting_id]
        
    @RetryProperty
    def system(self):
        return self.db.systems[self.system_id]
    
    @RetryProperty
    def atoms(self):
        return self.system.atoms
    
    @RetryProperty
    def frames(self):
        return self.system.frames
    
    @property
    def hinumas(self):
        if self._hinumas is None:
            self._hinumas = DBHinumas(self)
        return self._hinumas
    
    @property
    def surface_groups(self):
        if self._surface_groups is None:
            self._surface_groups = DBSurfaceGroups(self)
        return self._surface_groups

    @property
    def init_frame_id(self):
        return self.frames.number_to_id[self.settings.init_frame_number]
    
    @property
    def catalyst_ids(self):
        if self._catalyst_ids is None or self._catalyst_ids == []:
            self._catalyst_ids = self._base_type_atom_ids('catalyst')
        return self._catalyst_ids
    
    @property
    def chemical_ids(self):
        if self._chemical_ids is None or self._chemical_ids == []:
            self._chemical_ids = self._base_type_atom_ids('chemical')
        return self._chemical_ids
    
    @property
    def base_types_dict(self):
        if self._base_types_dict is None:
            self._base_types_dict = self._load_base_types_dict()

        return self._base_types_dict

    @property
    def filename(self):
        return self.system.file.name
    
    @property
    def distances(self):
        return self.system.distances

    def set_base_types(self, base_types=None, by=None):
        if base_types is not None:
            return self._set_base_types(base_types)

        if by is None:
            by = self.settings.base_type_algorithm
        return self.set_base_types_by(by=by)
    

    def _set_base_types(self, base_types):
        '''Set base_types_dict from base_types list.
        Schema: {atom_id: base_type_name}
        '''
        base_types_dict = {}
        for atom_number, base_type in enumerate(base_types):
            atom_id = self.atoms.number_to_id[atom_number]
            base_types_dict[atom_id] = base_type

        return self._init_base_type_ids(base_types_dict)
    
    def set_base_types_by(self, input=None, by='chemical_elements', overwrite=False):
        if self._base_types_dict is not None and not overwrite:
            raise ValueError('base_types_dict is already set. Use overwrite=True to overwrite it.')

        if by == 'chemical_elements':
            if input is None:
                input = self.settings.chemical_elements
            return self._set_base_types(self._base_types_by_chemical_elements(input))
        
        elif by == 'hinuma':
            if input is None:
                input = self.settings.max_molecule_size
            return self._set_base_types(self._base_types_by_hinuma(input))

        elif by == 'chemical_tag':
            if input is None:
                input = self.settings.chemical_tag
            return self._set_base_types(self._base_types_by_tags(input))
        else:
            raise ValueError(f'by={by} is not a valid option')

    def _init_base_type_ids(self, base_types_dict):
        property_id = self.db.properties.name_to_id['init_base_type']
        write_atom_tags = []
        for atom_id, base_type_id in base_types_dict.items():
            write_atom_tags.append(new_atom_tag(self.experiment_id, atom_id, 
                                                property_id, base_type_id))

        with self.db.Session() as session:
            session.bulk_insert_mappings(AtomTag, write_atom_tags)
            session.commit()

        self._base_types_dict = base_types_dict
        return self._base_types_dict
    
    def _base_type_atom_ids(self, base_type):
        if isinstance(base_type, str):
            base_type_name = base_type
        elif isinstance(base_type, (int, float)):
            base_type_name = self.db.base_types.id_to_name[base_type]
        else:
            raise TypeError('Base type must be a string or integer')
        
        return [a_id for a_id, b_id in self.base_types_dict.items() if b_id == base_type_name]

    def _base_types_by_chemical_elements(self, chemical_elements):
        base_types = []
        if isinstance(chemical_elements[0], str):
            chemical_elements = [self.db.element(el).atomic_number for el in chemical_elements]

        for atomic_number in self.atoms.atomic_numbers.values():
            if atomic_number in chemical_elements:
                base_types.append('chemical')
            else:
                base_types.append('catalyst')

        if not 'chemical' in base_types:
            print('WARNING: Chemical elements not found in experiment')
            
        return base_types
    
    def _base_types_by_hinuma(self, max_molecule_size):
        hinuma = self.hinumas._set_new_hinuma(base_type='all')
        all_fragments = get_surfaces_from_connections(hinuma.connections)
        catalyst_ids, chemical_ids = set(), set()
        for fragment in all_fragments:
            if len(fragment) > max_molecule_size:
                catalyst_ids.update(fragment)
            else:
                chemical_ids.update(fragment)

        base_types = []
        for atom_id in self.atoms.number_to_id.values():
            if atom_id in catalyst_ids:
                base_types.append('catalyst')            
            elif atom_id in chemical_ids:
                base_types.append('chemical')
            else:
                raise ValueError('Atom not found in any fragment')
        return base_types
    
    def _base_types_by_tags(self, input):
        if isinstance(input, (list, tuple)):
            chem_tags = input
        elif isinstance(input, int):
            chem_tags = [input]
        else:
            print(input, type(input))
            raise TypeError('input must be a list, tuple, or integer')

        base_types = []
        for atom in self.atoms.atoms_sorted_by_number():
            if atom.init_tag in chem_tags:
                base_types.append('chemical')
            else:
                base_types.append('catalyst')

        return base_types

    def _load_base_types_dict(self):
        # Check if base_types are in database
        base_types_dict = self._get_init_base_types()
        if len(base_types_dict) == 0:
            raise ValueError('Base types not found in database. Use set_base_types() to set them.')
        
        return base_types_dict

    def _get_init_base_types(self):
        property_id = self.db.properties.name_to_id['init_base_type']
        with self.db.Session() as session:
            return get_init_base_types(session, property_id, self.id)

    def _calculate_hinuma(self, frame_id=None, base_type='catalyst', dynamic_cutoff=None):
        if frame_id is None:
            frame_id = self.init_frame_id

        if base_type == 'catalyst':
            atom_ids = self.catalyst_ids
        elif base_type == 'chemical':
            atom_ids = self.chemical_ids
        elif base_type == 'all':
            atom_ids = self.atoms.ids
        
        xyz_filter = self.settings.xyz_filter

        atom_numbers = [self.atoms.id_to_number[i] for i in atom_ids]
        pos = np.array([self.system.frames[frame_id].positions[i] for i in atom_numbers])

        if dynamic_cutoff is None and 'dynamic_cutoff' in self.settings.all():
            dynamic_cutoff = self.settings.dynamic_cutoff
            pyykko_fudge = self.settings.pyykko_fudge
        else:
            dynamic_cutoff = False
            pyykko_fudge = 1.0
        
        if dynamic_cutoff:
            atomic_numbers = [self.atoms[i].atomic_number for i in atom_ids]
        else:
            atomic_numbers = False

        if self.db.engine.name == 'sqlite':
            # TODO: Find a way for multiprocessing to work with sqlite
            n_threads = 1
        elif self.db.engine.name == 'postgresql':
            # TODO: Find a way for multiprocessing to work with sqlite and postgresql AND/OR determine if nested its multiprocessing
            n_threads = 1
        else:
            n_threads = os.cpu_count()
        
        solid_angles, r_dashes, connection_lists, distances_dict = surface_classifier(\
            pos, self.system.cell, self.system.pbc,
            self.system.box, 
            cutoff=self.settings.hinuma_cutoff,
            zero_vec_tolerance=self.settings.zero_vec_tolerance,
            mic=self.settings.mic, 
            xyz_filter=xyz_filter,
            dynamic_cutoff=dynamic_cutoff,
            pyykko_fudge=pyykko_fudge,
            atomic_numbers=atomic_numbers,
            return_distances=True,
            n_threads=n_threads)
                
        return convert_surface_classifier(solid_angles, r_dashes, connection_lists, distances_dict, atom_ids)
    

    def _calculate_hinuma_parallel(self, frame_ids=None, n_cores=4, base_type='catalyst', dynamic_cutoff=None):
        if frame_ids is None:
            frame_ids = [self.init_frame_id]

        n_cores = min(n_cores, cpu_count())
        print(f'Cores used: {n_cores}')
        data_for_frames = []
        for frame_id in frame_ids:
            if base_type == 'catalyst':
                atom_ids = self.catalyst_ids
            elif base_type == 'chemical':
                atom_ids = self.chemical_ids
            elif base_type == 'all':
                atom_ids = self.atoms.ids
            
            xyz_filter = self.settings.xyz_filter

            atom_numbers = [self.atoms.id_to_number[i] for i in atom_ids]
            pos = np.array([self.system.frames[frame_id].positions[i] for i in atom_numbers])

            if dynamic_cutoff is None and 'dynamic_cutoff' in self.settings.all():
                dynamic_cutoff = self.settings.dynamic_cutoff
                pyykko_fudge = self.settings.pyykko_fudge
            else:
                dynamic_cutoff = False
                pyykko_fudge = 1.0
            
            if dynamic_cutoff:
                atomic_numbers = [self.atoms[i].atomic_number for i in atom_ids]
            else:
                atomic_numbers = False

            hinuma_kwargs = {
                'pos': pos,
                'cell': self.system.cell,
                'pbc': self.system.pbc,
                'box': self.system.box,
                'zero_vec_tolerance': self.settings.zero_vec_tolerance,
                'mic': self.settings.mic,
                'xyz_filter': xyz_filter,
                'dynamic_cutoff': dynamic_cutoff,
                'pyykko_fudge': pyykko_fudge,
                'atomic_numbers': atomic_numbers,
                'cutoff': self.settings.hinuma_cutoff,
                'return_distances': True,
                'n_threads': 1
            }

            data_for_frames.append([hinuma_kwargs, atom_ids])

        if n_cores > 1:
            with Pool(n_cores) as p:
                results = p.map(calculate_hinuma_standalone, data_for_frames)
        else:
            results = [calculate_hinuma_standalone(data_for_frame) for data_for_frame in data_for_frames]

        return dict(zip(frame_ids, results))


    def _calculate_hinuma_selected(self, filter_ids, configuration_ids, frame_id, dynamic_cutoff=None):

        xyz_filter = self.settings.xyz_filter
        reference_numbers = [self.atoms.id_to_number[i] for i in filter_ids]
        configuration_numbers = [self.atoms.id_to_number[i] for i in configuration_ids]
        
        all_numbers = list(sorted(set(reference_numbers + configuration_numbers)))
        all_ids = [self.atoms.number_to_id[i] for i in all_numbers]

        pos = np.array([self.system.frames[frame_id].positions[i] for i in all_numbers])

        if dynamic_cutoff is None and 'dynamic_cutoff' in self.settings.all():
            dynamic_cutoff = self.settings.dynamic_cutoff
            pyykko_fudge = self.settings.pyykko_fudge
        else:
            dynamic_cutoff = False
            pyykko_fudge = 1.0
      
        if dynamic_cutoff:
            atomic_numbers = [self.atoms[i].atomic_number for i in all_ids]
        else:
            atomic_numbers = False

        if self.db.engine.name == 'sqlite':
            n_threads = 1
        elif self.db.engine.name == 'postgresql':
            # TODO: Find a way for multiprocessing to work with sqlite and postgresql AND/OR determine if nested its multiprocessing
            n_threads = 1
        else:
            n_threads = os.cpu_count()

        hinuma_results = surface_classifier_selected(\
            pos, reference_numbers,
            self.system.box, 
            cutoff=self.settings.hinuma_cutoff,
            zero_vec_tolerance=self.settings.zero_vec_tolerance,
            mic=self.settings.mic, 
            xyz_filter=xyz_filter,
            dynamic_cutoff=dynamic_cutoff,
            pyykko_fudge=pyykko_fudge,
            atomic_numbers=atomic_numbers,
            return_distances=True,
            n_threads=n_threads)
        
        solid_angles, r_dashes, connection_lists, distances_dict = hinuma_results
        
        return convert_surface_classifier(solid_angles, r_dashes, connection_lists, 
                                          distances_dict, reference_numbers, all_ids=all_ids)
    
    def _test_vis_hinuma(self, frame_id=None, base_type='catalyst', dynamic_cutoff=None):
        if frame_id is None:
            frame_id = self.init_frame_id

        if base_type == 'catalyst':
            atom_ids = self.catalyst_ids
        elif base_type == 'chemical':
            atom_ids = self.chemical_ids
        elif base_type == 'all':
            atom_ids = self.atoms.ids
        
        xyz_filter = self.settings.xyz_filter

        pos = np.array([self.system.pos_dict_id[self.init_frame_id][i] for i in atom_ids])

        if dynamic_cutoff is None and 'dynamic_cutoff' in self.settings.all():
            dynamic_cutoff = self.settings.dynamic_cutoff
            pyykko_fudge = self.settings.pyykko_fudge
        else:
            dynamic_cutoff = False
            pyykko_fudge = 1.0
        
        if dynamic_cutoff:
            atomic_numbers = [self.atoms[i].atomic_number for i in atom_ids]
        else:
            atomic_numbers = False

        if self.db.engine.name == 'sqlite':
            n_threads = 1
        # TODO: Find a way for multiprocessing to work with sqlite and postgresql AND/OR determine if nested its multiprocessing
        elif self.db.engine.name == 'postgresql':
            n_threads = 1
        else:
            n_threads = os.cpu_count()

        solid_angles, r_dashes, connection_lists, distances_dict, all_atom_pos, all_atom_ids = surface_classifier(\
            pos, self.system.cell, self.system.pbc,
            self.system.box, 
            cutoff=self.settings.hinuma_cutoff,
            zero_vec_tolerance=self.settings.zero_vec_tolerance,
            mic=self.settings.mic, 
            xyz_filter=xyz_filter,
            dynamic_cutoff=dynamic_cutoff,
            pyykko_fudge=pyykko_fudge,
            atomic_numbers=atomic_numbers,
            return_distances=True,
            n_threads=n_threads,
            debug=True)
        
        return all_atom_pos, all_atom_ids

    def set_new_surfaces(self, surface_type='hinuma', hinuma_id=None, frame_id=None, dynamic_cutoff=None):
        surface_group_id = self.surface_groups._new_surface_group(surface_type=surface_type, 
                                                                  hinuma_id=hinuma_id, 
                                                                  frame_id=frame_id,
                                                                  dynamic_cutoff=dynamic_cutoff)
        return self.surface_groups[surface_group_id]._set_new_surfaces()

    def __repr__(self):
        return f'experiment_id={self.experiment_id}, system_id ={self.system_id}, setting_id={self.setting_id}'
    

class ViewExperiment:
    def __init__(self, experiment) -> None:
        self.experiment = experiment

    def split(self):
        debug_view_split(self.experiment)

    def surfaces(self, surface_group_id=None):
        debug_view_surfaces(self.experiment, surface_group_id=surface_group_id)

    def matplot(self, frame_id=None, rotation='90x, 0y, 0z', cut=False):
        if frame_id is None:
            frame_number = self.experiment.frames.id_to_number[self.experiment.init_frame_id]
        matplotlib_debug_viz(self.experiment, frame=frame_number, rotation=rotation, cut=cut)

    def matplotlib_hinuma(self, experiment, hinuma_id=None, frame=0, rotation='90x, 0y, 0z', color_range=(0, 4*np.pi), cut=False, 
                          centering=False, figsize=(10, 8), dpi=150, plot_histogram=False, n_bins=16, solid_angles=None):
        matplotlib_hinuma_viz(experiment, hinuma_id, frame, rotation, color_range, cut, centering, figsize, dpi, plot_histogram, n_bins, solid_angles)
