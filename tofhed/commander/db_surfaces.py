from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *


class DBSurfaceGroups(SuperDict):
    def __init__(self, experiment, *args, **kwargs):
        self.experiment = experiment
        self.db = self.experiment.db
        self.surface_types = self.db.surface_types
        super().__init__(*args, **kwargs)
        self.update(self._get_surface_groups())
        self.hinuma_id = None

    def _get_surface_groups(self):
        with self.db.Session() as session:
            surface_groups = get_surface_groups(session, self.experiment.experiment_id)

        _surface_groups = {}
        for surface_group in surface_groups:
            _surface_groups[surface_group['surface_group_id']] = DBSurfaceGroup(surface_group, self.experiment)
 
        return _surface_groups

    def _new_surface_group(self, surface_type='hinuma', hinuma_id=None, solid_angle_limit=None, frame_id=None, dynamic_cutoff=None):
        surface_type_id = self.surface_types.name_to_id[surface_type]

        if surface_type == 'hinuma' and hinuma_id is None:
            hinuma_id = self.experiment.hinumas._set_new_hinuma(frame_id=frame_id, dynamic_cutoff=dynamic_cutoff).hinuma_id
        
        self.hinuma_id = hinuma_id
        
        if not hinuma_id is None and solid_angle_limit is None:
            solid_angle_limit = self.experiment.settings.solid_angle_limit
        

        with self.db.Session() as session:
            surface_group_id = new_surface_group(session, self.experiment.experiment_id, 
                                                 surface_type_id, hinuma_id,
                                                 solid_angle_limit, commit=True)

        _surface_group = {'surface_group_id': surface_group_id,
                          'experiment_id': self.experiment.experiment_id,
                          'hinuma_id': hinuma_id,
                          'solid_angle_limit': solid_angle_limit,
                          'surface_type_id': surface_type_id,
                          'surface_type_name': surface_type}

        self[surface_group_id] = DBSurfaceGroup(_surface_group, self.experiment)

        return surface_group_id
    
    @property
    def surface_group_ids(self):
        return list(sorted(self.keys()))

    @property
    def latest(self):
        return self[self.surface_group_ids[-1]]

    def __repr__(self):
        return f'{self.values()}'


class DBSurfaceGroup(SuperDict):
    def __init__(self, surface_group_dict, experiment, *args, **kwargs):
        self.experiment = experiment
        self.db = self.experiment.db
        super().__init__(*args, **kwargs)
        self.__dict__.update(surface_group_dict)
        self.update(self._get_surfaces())
    
    @property
    def id(self):
        return self.surface_group_id

    @property
    def surface_atom_ids(self):
        for surface in self.values():
            yield from surface.atom_ids
    
    @property
    def atom_numbers(self):
        for surface in self:
            for atom_id in surface.surface_atoms:
                yield self.experiment.system.atoms[atom_id].atom_number

    def _calculate_surfaces(self, surface_type_name=None):
        if surface_type_name is None:
            surface_type_name = self.surface_type_name

        if surface_type_name == 'plane_2d' or surface_type_name == 'hinuma':
            if self.hinuma_id is None:
                raise Exception('hinuma_id is not set and needed!')
            
        if surface_type_name == 'plane_2d':
            return self._calculate_plane_2d_surfaces()
        elif surface_type_name == 'simple_2d':
            return self._calculate_simple_2d_surfaces()
        elif surface_type_name == 'hinuma':
            return self._calculate_hinuma_surfaces()
        else:
            raise Exception(f'surface_type_name {self.surface_type_name} not implemented!')

    def _calculate_plane_2d_surfaces(self):
        raise Exception('not implemented yet!')

    def _calculate_simple_2d_surfaces(self):
        raise Exception('not implemented yet!')

    def _calculate_hinuma_surfaces(self, debug_prints=False):
        hinuma = self.experiment.hinumas[self.hinuma_id]
        solid_angle_limit = self.experiment.settings.solid_angle_limit
        surface_atom_ids = []
        for atom_id in self.experiment.catalyst_ids:
            if atom_id not in hinuma.solid_angles:
                if debug_prints:
                    print(f'Warning: Atom ID {atom_id} not found in hinuma.solid_angles')
                continue

            if isinstance(hinuma.solid_angles[atom_id], str):
                if debug_prints:
                    print(f'Debug: {hinuma.solid_angles[atom_id]} for Atom ID {atom_id}')
                continue
            if hinuma.solid_angles[atom_id] is None:
                if debug_prints:
                    print(f'Debug: {hinuma.solid_angles[atom_id]} for Atom ID {atom_id}')
                continue
            if hinuma.solid_angles[atom_id] > solid_angle_limit:
                surface_atom_ids.append(atom_id)
        
        surfaces = get_surfaces_from_connections(hinuma.connections, surface_atom_ids)
        return surfaces

    
    def _set_new_surfaces(self, surface_type_name=None):
        surfaces = self._calculate_surfaces(surface_type_name=surface_type_name)
        return self._set_surfaces(surfaces)
    
    def _set_surfaces(self, surfaces):
        for surface in surfaces:
            self._set_surface(surface)
        return self
    
    def _set_surface(self, surface):
        with self.db.Session() as session:
            surface_id = new_surface_id(session, self.surface_group_id, commit=True)

        surface_atoms_dict = []
        for atom_id in surface:
            surface_atoms_dict.append(
                {'atom_id': atom_id,
                 'surface_id': surface_id})
        with self.db.Session() as session:
            session.bulk_insert_mappings(SurfaceAtom, surface_atoms_dict)
            session.commit()

        surface_dict = {'surface_id': surface_id,
                        'surface_group_id': self.surface_group_id,
                        'surface_type_id': self.surface_type_id,
                        'surface_type_name': self.surface_type_name,
                        'surface_atoms': surface}
                            
        self[surface_id] = DBSurface(surface_dict, self)
        return self[surface_id]
    
    def _get_surfaces_for_surface_group_id(self):
        with self.db.Session() as session:
            surface_ids = get_surface_ids_for_surface_group_id(session, self.surface_group_id)
            surfaces = {}
            for surface_id in surface_ids:
                surfaces[surface_id] = {'surface_id': surface_id,
                                        'surface_group_id': self.surface_group_id,
                                        'surface_type_id': self.surface_type_id,
                                        'surface_type_name': self.surface_type_name,
                                        'surface_atoms': get_surface_atoms_for_surface_id(session, surface_id),
                                        'parameters': get_surface_parameters_for_surface_id(session, surface_id),
                                        }
            return surfaces

    def _get_surfaces(self):
        surfaces = {}
        for surface_id, surface_dict in self._get_surfaces_for_surface_group_id().items():
            surfaces[surface_id] = DBSurface(surface_dict, self)
        return surfaces

    def pyykko_event_detection(self, write_to_db=True):
        return self.experiment.events.pyykko_event_detection(surface_group_id=self.surface_group_id,
                                                             write_to_db=write_to_db)

    def __repr__(self):
        return f'SurfaceGroup ID {self.surface_group_id} - Surfaces: {len(self)}'


class DBSurface:
    def __init__(self, surface_dict, surface_group):
        self.surface_group = surface_group
        self.__dict__.update(surface_dict)

    @property
    def id(self):
        return self.surface_id

    @property
    def atom_ids(self):
        return list(self.surface_atoms) 

    def __repr__(self):
        return f'Surface ID {self.surface_id}: {len(self.surface_atoms)} atoms'