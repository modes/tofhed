from tofhed.functions import *
import time
from MDAnalysis.lib import distances as mda_distances
from itertools import product


class DBDistances:
    def __init__(self, system):
        self.system = system
        self.db = self.system.db
        # Schema: {frame_id: {[atom_id, atom_id]: distance}}
        self._distances = {}
    
    @property
    def memory(self):
        return self._distances
    
    @property
    def all(self):
        return self._distances
    
    def add_distances(self, distances):
        '''Add distances to the database and memory.
        Schema of distances: {frame_id: {[atom_id, atom_id]: distance}}
        
        '''
        # Save the distances to the database
        write_distances = self._try_write_distances_to_db(distances)

        # Update the distances in memory
        self._update_distances(distances)

        return dict(distances)

    def _get_distances(self, frame_ids, atom_ids_1=None, atom_ids_2=None):
        atom_ids_1, atom_ids_2, distances, pairs = self._init_get_distances(atom_ids_1, atom_ids_2)

        # Check if the distances are already loaded
        distances_to_get, distances = self._try_get_distances_from_memory(frame_ids, pairs, distances)
        
        # Check if the distances are already in the database
        distances_to_calculate, distances = self._try_get_distances_from_db(distances_to_get, distances)

        # Calculate the distances
        write_distances = {}
        for frame_id, pairs_set in distances_to_calculate.items():
            # TODO: Don't calculate the distances that are already in the database
            dist = self._calculate_distances(frame_id, atom_ids_1, atom_ids_2)
            calculated_distances = {pair: dist[pair] for pair in pairs_set}

            write_distances[frame_id] = calculated_distances
            distances[frame_id].update(calculated_distances)

        # Save the distances to the database
        self._write_distances_to_db(write_distances)

        # Update the distances in memory
        self._update_distances(distances)

        return dict(distances)
    
    def get_distance_if_available(self, frame_id, atom_id_1, atom_id_2):
        pair = self.pair(atom_id_1, atom_id_2)
        try:
            return (True, self._distances[frame_id][pair])
        except KeyError:
            return (False, None)

    def _get_capped_distances(self, frame_ids, atom_ids_1=None, atom_ids_2=None, max_cutoff=4.0):
        start_time = time.time()
        print(start_time, 'get capped distances start')
        atom_ids_1, atom_ids_2, distances, pairs = self._init_get_distances(atom_ids_1, atom_ids_2)
        print(time.time()-start_time, 'made pairs')
        # Check if the distances are already loaded
        distances_to_get, distances = self._try_get_distances_from_memory(frame_ids, pairs, distances)
        print(time.time()-start_time, 'got from memory')
        # Check if the distances are already in the database
        distances_to_calculate, distances = self._try_get_distances_from_db(distances_to_get, distances)
        print(time.time()-start_time, 'read from db')
        # Calculate the distances
        write_distances = {}
        for frame_id, pairs_set in distances_to_calculate.items():
            dist = self._calculate_capped_distances(frame_id, atom_ids_1, atom_ids_2, max_cutoff=max_cutoff)
            calculated_distances = {pair: dist[pair] for pair in pairs_set if pair in dist}
            write_distances[frame_id] = calculated_distances
            distances[frame_id].update(calculated_distances)
        print(time.time()-start_time, 'calculated distances')
        # Save the distances to the database
        self._write_distances_to_db(write_distances)
        print(time.time()-start_time, 'wrote to db')
        # Update the distances in memory
        self._update_distances(distances)
        print(time.time()-start_time, 'updated to memory')
        return self._filter_distances(dict(distances), max_cutoff=max_cutoff)

    def _get_capped_distances_from_memory(self, frame_ids, atom_ids_1=None, atom_ids_2=None, max_cutoff=4.0):
        atom_ids_1, atom_ids_2, distances, pairs = self._init_get_distances(atom_ids_1, atom_ids_2)
        _, distances = self._try_get_distances_from_memory(frame_ids, pairs, distances)
        
        return self._filter_distances(dict(distances), max_cutoff=max_cutoff)

    def _init_get_distances(self, atom_ids_1=None, atom_ids_2=None):
        if atom_ids_1 is None:
            atom_ids_1 = self.system.atoms.ids
            # system_id = self.db.atom_id_to_system_id(atom_ids_1[0])

        if atom_ids_2 is None:
            atom_ids_2 = atom_ids_1

        distances = defaultdict(dict)
        pairs = self._pairs(atom_ids_1, atom_ids_2)

        return atom_ids_1, atom_ids_2, distances, pairs
    
    def _filter_distances(self, distances, max_cutoff=4.0):
        capped_distances = {}
        for frame_id, frame_distances in distances.items():
            capped_distances[frame_id] = {pair: distance for pair, distance in frame_distances.items() if distance < max_cutoff}
        return capped_distances

    def pair(self, atom_id_1, atom_id_2):
        return tuple(sorted([atom_id_1, atom_id_2]))
    
    def _pair_ids_from_db(self):
        with self.db.Session() as session:
            return get_pair_ids_from_system(session, self.system.system_id)

    def _update_distances(self, distances, overwrite=False):
        for frame_id, frame_distances in distances.items():
            if frame_id not in self._distances:
                self._distances[frame_id] = {}
            if overwrite:
                self._distances[frame_id].update(frame_distances)
            else:
                for pair, distance in frame_distances.items():
                    if pair not in self._distances[frame_id]:
                        self._distances[frame_id][pair] = distance

    def _write_distances_to_db(self, distances):
        '''
        Write the distances to the database.

        Schema: {frame_id: {[atom_id, atom_id]: distance}}
        '''
        pair_ids = self._pair_ids_from_db()
        write_pairs = []
        for frame_id, frame_distances in distances.items():
            for pair in frame_distances.keys():
                if pair not in pair_ids:
                    write_pairs.append({'atom_id_1': pair[0],
                                        'atom_id_2': pair[1]})
        with self.db.Session() as session:
            if write_pairs:             
                session.bulk_insert_mappings(Pair, write_pairs)
                session.commit()
                pair_ids = get_pair_ids_from_system(session, self.system.system_id)
        
            write_distances = []
            for frame_id, frame_distances in distances.items():
                for pair, distance in frame_distances.items():
                    write_distances.append({'frame_id': frame_id,
                                            'pair_id': pair_ids[pair],
                                            'distance': distance})
            
            if write_distances:
                session.bulk_insert_mappings(Distance, write_distances)
                session.commit()
        
        return True

    def _pairs(self, atom_ids_1, atom_ids_2=None):
        if atom_ids_2 is None:
            atom_ids_2 = atom_ids_1
        
        return {self.pair(i, j) for i in atom_ids_1 for j in atom_ids_2 if i != j}

    #TODO: Move out of here
    def _init_calculation(self, frame_id, atom_ids_1, atom_ids_2=None):
        system = self.db.systems[self.db.frame_id_to_system_id[frame_id]]
        pos = system.positions[system.frames.id_to_number[frame_id]]
        box = system.box

        pos_1 = pos[system.atoms.ids_to_numbers(atom_ids_1)]
        
        if atom_ids_2 is None:
            atom_ids_2 = atom_ids_1
            pos_2 = pos_1
        else: 
            pos_2 = pos[system.atoms.ids_to_numbers(atom_ids_2)]
        
        return pos_1, pos_2, atom_ids_2, box
    
    def _calculate_distances(self, frame_id, atom_ids_1, atom_ids_2=None):
        pos_1, pos_2, atom_ids_2, box = self._init_calculation(frame_id, atom_ids_1, atom_ids_2)
        
        # TODO: Use the backend from the settings
        dist = mda_distances.distance_array(pos_1, pos_2, box=box, backend='OpenMP')
        
        distances_dict = {}
        for atom_id_1, atom_id_2 in product(atom_ids_1, atom_ids_2):
            if atom_id_1 == atom_id_2:
                continue
            pair = self.pair(atom_id_1, atom_id_2)
            if pair not in distances_dict:
                i, j = atom_ids_1.index(atom_id_1), atom_ids_2.index(atom_id_2)
                distances_dict[pair] = dist[i, j]

        return distances_dict
    
    def _calculate_capped_distances(self, frame_id, atom_ids_1, atom_ids_2=None, max_cutoff=4.0):
        pos_1, pos_2, atom_ids_2, box = self._init_calculation(frame_id, atom_ids_1, atom_ids_2)
 
        pairs, dist = mda_distances.capped_distance(pos_1, pos_2, max_cutoff=max_cutoff, box=box, return_distances=True)
        
        distances_dict = {}
        for (i, j), distance in zip(pairs, dist):
            distances_dict[self.pair(atom_ids_1[i], atom_ids_2[j])] = distance

        return distances_dict

    def _try_get_distances_from_memory(self, frame_ids, pairs, distances):
        distances_to_get = defaultdict(set)
        for frame_id in frame_ids:
            if frame_id in self._distances:
                for pair in pairs:
                    if pair not in self._distances[frame_id]:
                        distances_to_get[frame_id].add(pair)
                    else:
                        distances[frame_id][pair] = self._distances[frame_id][pair]
            else:
                distances_to_get[frame_id].update(set(pairs))
        
        return distances_to_get, distances
    
    def _try_get_distances_from_db(self, distances_to_get, distances):
        distances_to_calculate = defaultdict(set)
        distances_from_db = self._get_distances_from_db_picker(distances_to_get)

        for frame_id, pairs_set in distances_to_get.items():
            if frame_id in distances_from_db:
                for pair in pairs_set:
                    if pair in distances_from_db[frame_id]:
                        distances[frame_id][pair] = distances_from_db[frame_id][pair]
                    else:
                        distances_to_calculate[frame_id].add(pair)
            else:
                distances_to_calculate[frame_id].update(pairs_set)
        
        return distances_to_calculate, distances
    
    def _try_write_distances_to_db(self, distances):
        distances_from_db = self._get_distances_from_db_picker(distances)

        write_distances = defaultdict(dict)
        for frame_id, frame_distances in distances.items():
            for pair, distance in frame_distances.items():
                if frame_id not in distances_from_db or pair not in distances_from_db[frame_id]:
                    write_distances[frame_id][pair] = distance
        
        if write_distances:
            self._write_distances_to_db(write_distances)
        
        return write_distances

    def _get_distances_from_db_picker(self, distances):
        frame_ids = list(distances.keys())
        if len(frame_ids) > 500:
            distances_from_db = self._get_all_distances_from_db()
        else:
            distances_from_db = self._get_distances_from_db(frame_ids) 
        return distances_from_db
    
    def _get_distances_from_db(self, frame_ids, pairs=None, chunk_size=1000):
        distances = defaultdict(dict)
        with self.db.Session() as session:
            for _frame_ids in chunks(frame_ids, chunk_size):    
                values = session.query(
                    Distance.distance,
                    Distance.frame_id,
                    Pair,
                ).filter(
                    or_(Distance.frame_id == frame_id for frame_id in _frame_ids)
                ).join(
                    Pair, Pair.id == Distance.pair_id
                ).all()
            
                for v in values:
                    distances[v.frame_id][v.Pair.pair] = v.distance
        if pairs is not None:
            return {frame_id: {pair: distances[frame_id][pair] for pair in pairs} for frame_id in frame_ids}
        
        return distances
    
    def _get_all_distances_from_db(self, pairs=None):
        distances = defaultdict(dict)
        with self.db.Session() as session:
            values = session.query(
                Distance.distance,
                Distance.frame_id,
                Pair,
            ).join(
                Pair, Pair.id == Distance.pair_id
            ).join(
                Frame, Frame.id == Distance.frame_id
            ).filter(
                Frame.system_id == self.system.system_id
            ).all()
        
            for v in values:
                distances[v.frame_id][v.Pair.pair] = v.distance
        if pairs is not None:
            frame_ids = self.system.frames.ids
            return {frame_id: {pair: distances[frame_id][pair] for pair in pairs} for frame_id in frame_ids}
        
        return distances
    
    def _all_pairs(self, is_sorted=False):
        if is_sorted:
            return sorted({(i, j) for i in self.system.atoms.ids for j in self.system.atoms.ids if i < j})
        return {(i, j) for i in self.system.atoms.ids for j in self.system.atoms.ids if i < j}

    def __repr__(self) -> str:
        return f"Distances(system={self.system}), len={len(self._distances)}"