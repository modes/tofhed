from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *
from tofhed.commander.db_frames import DBFrames
from tofhed.commander.db_atoms import DBAtoms
from tofhed.commander.db_distances import DBDistances
from itertools import combinations
from tofhed.utils import read, ase_frame_range, file_exists
from mendeleev import element


class DBSystems(SuperDict):
    def __init__(self, db, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = db
        self.update(self._get_systems())

    def _get_systems(self):
        with self.db.Session() as session:
            return {s['system_id']: DBSystem(s, self.db) for s in get_systems(session)}


class DBSystem:
    def __init__(self, system_dict, db):
        self.db = db
        self.__dict__.update(system_dict)
        self._experiments = None
        self._frames = None
        self._positions = None
        self._atoms = None
        self._cell = None
        self._box = None
        self._distances = None
        self._pair_ids = None
        self._init_tags = None

    @property
    def id(self):
        return self.system_id

    @property
    def experiments(self):
        if not self._experiments:
            if self._experiments is None:
                self._experiments = {}
            for e_id, e in self.db.experiments.items():
                if e.system_id == self.system_id:
                    self._experiments[e_id] = e
        
        return self._experiments
    
    @property
    def frames(self):
        if self._frames is None:
            self._frames = DBFrames(self)
        
        return self._frames

    @property
    def positions(self):
        '''Returns a numpy array of the positions.
        Schema: positions[frame_number, atom_number, xyz]
        '''
        if self._positions is None:
            if self.file.import_positions:
                self._positions = self._get_positions()
            
            else:
                file_path = self.file.path
                if not file_exists(file_path):
                    raise FileNotFoundError(f'File {file_path} does not exist or has moved to another path.')
                frame_number_range = ase_frame_range(self.frame_number_first, self.frame_number_last)
                print(f'frame_number_range: {frame_number_range}')
                ase_frames = read(file_path, index=frame_number_range)
                print(f'len(ase_frames): {len(ase_frames)}')
                # TODO: Check if schema is the same -> positions[frame_number, atom_number, xyz]
                print('TODO: commander.py -> DBSystems.positions check schema.')
                self._positions = np.array([frame.get_positions() for frame in ase_frames])

        return self._positions

    @property
    def file(self):
        return self.db.files[self.file_id]
    
    @property
    def filename(self):
        return self.file.name
    
    @property
    def atoms(self):
        if self._atoms is None:
            self._atoms = DBAtoms(self)
        
        return self._atoms
    
    @property
    def init_tags(self):
        if self._init_tags is None:
            self._init_tags = {a.id: a.init_tag for a in self.atoms}
        return self._init_tags
    
    @property
    def pairs(self):
        return combinations(sorted(self.atoms.ids), 2)
    
    @property
    def distances(self):
        if self._distances is None:
            self._distances = DBDistances(self)
        
        return self._distances
    
    @property
    def pair_ids(self):
        if self._pair_ids is None:
            self._pair_ids = self._get_pair_ids()
        
        return self._pair_ids
    
    @property
    def pair_id_to_atom_ids(self):
        return {v: k for k, v in self.items()}
    
    @property
    def elements(self):
        return [element(e).name for e in set(self.atoms.atomic_numbers.values())]
    
    def new_experiment(self, settings_dict, note=''):
        setting_id = self.db.settings.new_settings(settings_dict, note=note)
        
        with self.db.Session() as session:
            return self.db.experiments.new_experiment(setting_id, self.system_id, note=note)

    def _get_positions(self):
        with self.db.Session() as session:
            return get_all_position_ndarray(session, self.system_id)
        
    def _get_pair_ids(self):
        with self.db.Session() as session:
            return get_pair_ids_from_system(session, self.system_id)
        
    def all(self):
        return dict(self.__dict__)

    def read_frame_ase(self, frame_number):
        return self.file.read_frame_ase(frame_number)
    
    def __repr__(self):
        return f'system_id={self.system_id}, file_id={self.file_id}, filename={self.filename}'