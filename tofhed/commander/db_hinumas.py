from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *


class DBHinumas(SuperDict):
    def __init__(self, experiment, *args, **kwargs):
        self.experiment = experiment
        self.db = self.experiment.db
        super().__init__(*args, **kwargs)
        self.update(self._get_hinumas())
    
    def _get_hinumas(self):
        hinumas = {}
        with self.db.Session() as session:
            hinuma_dicts = get_hinuma_dicts(session, self.experiment.id)
            for hinuma_id, hinuma_dict in hinuma_dicts.items():
                hinumas[hinuma_id] = DBHinuma(hinuma_id, hinuma_dict ,self.experiment)
        return hinumas

    def _new_hinuma(self, frame_id=None, base_type='catalyst'):
        if frame_id is None:
            frame_id = self.experiment.init_frame_id
        
        base_type_id = self.db.base_types.name_to_id[base_type]

        with self.db.Session() as session:
            hinuma_id = new_hinuma(session, self.experiment.experiment_id, 
                                   frame_id, base_type_id=base_type_id, commit=True)
        
        return hinuma_id

    def _set_new_hinuma(self, frame_id=None, base_type='catalyst', dynamic_cutoff=None):
        if frame_id is None:
            frame_id = self.experiment.init_frame_id

        solid_angles, r_dashes, connections, pair_distances = self.experiment._calculate_hinuma(\
            frame_id=frame_id, 
            base_type=base_type,
            dynamic_cutoff=dynamic_cutoff)

        hinuma_id = self._new_hinuma(frame_id, base_type=base_type)
        
        hinuma_atoms_dict = self._write_hinuma_atoms(hinuma_id, solid_angles, r_dashes)
        self._write_hinuma_connections(connections, hinuma_atoms_dict)
        hinuma_dict = {'frame_id': frame_id,
                       'solid_angles': solid_angles,
                       'r_dashes': r_dashes,
                       'connections': connections,
                       }
        self[hinuma_id] = DBHinuma(hinuma_id, hinuma_dict, self.experiment)

        self.experiment.system.distances.add_distances({frame_id: pair_distances})

        return self[hinuma_id]

    def _write_hinuma_atoms(self, hinuma_id, solid_angles, r_dashes, write_all=False):
        hinuma_atoms = []

        for atom_id, solid_angle in solid_angles.items():
            if isinstance(solid_angle, float):
                if np.isnan(solid_angle):
                    print('DEBUG NAN')
                    print(atom_id, solid_angle)

            if not isinstance(solid_angle, str):
                hinuma_atoms.append(new_hinuma_atom_dict(hinuma_id, atom_id, 
                                                         hinuma_vec=r_dashes[atom_id],
                                                         solid_angle=solid_angle))
            elif write_all:
                hinuma_atoms.append(new_hinuma_atom_dict(hinuma_id, atom_id, 
                                                         hinuma_vec=r_dashes[atom_id],
                                                         solid_angle=None))
        
        with self.db.Session() as session:
            session.bulk_insert_mappings(HinumaAtom, hinuma_atoms)
            session.commit()
        
        return get_hinuma_atoms_dict(session, hinuma_id)

    def _write_hinuma_connections(self, connections, hinuma_atoms_dict):
        hinuma_connection_dict = new_hinuma_connections_dict(connections, hinuma_atoms_dict)
        with self.db.Session() as session:
            session.bulk_insert_mappings(HinumaConnection, hinuma_connection_dict)
            session.commit()
        
        return hinuma_connection_dict
    
    def _set_new_hinumas_parallel(self, frame_ids=None, n_cores=4, base_type='catalyst', dynamic_cutoff=None):
        if frame_ids is None:
            frame_ids = [self.experiment.init_frame_id]

        results = self.experiment._calculate_hinuma_parallel(
            frame_ids=frame_ids, 
            n_cores=n_cores,
            base_type=base_type,
            dynamic_cutoff=dynamic_cutoff
        )

        hinumas = {}
        for frame_id, (solid_angles, r_dashes, connections, pair_distances) in results.items():
            hinuma_id = self._new_hinuma(frame_id, base_type=base_type)

            hinuma_atoms_dict = self._write_hinuma_atoms(hinuma_id, solid_angles, r_dashes)
            
            self._write_hinuma_connections(connections, hinuma_atoms_dict)
            hinuma_dict = {
                'frame_id': frame_id,
                'solid_angles': solid_angles,
                'r_dashes': r_dashes,
                'connections': connections,
            }
            hinumas[hinuma_id] = DBHinuma(hinuma_id, hinuma_dict, self.experiment)

            self.experiment.system.distances.add_distances({frame_id: pair_distances})

        self.update(hinumas)

        return hinumas
    
    def latest(self):
        return self[max(self.keys())]
    
    def latest_id(self):
        return max(self.keys())

    def __repr__(self):
        return f'hinumas:{[h for h in self.values()]}'


class DBHinuma:
    def __init__(self, hinuma_id, hinuma_dict, experiment):
        self.hinuma_id = hinuma_id
        self.experiment = experiment
        self.db = self.experiment.db
        self.__dict__.update(hinuma_dict)
        self._solid_angles = None
        self._r_dashes = None
        self._connections = None

    def refresh(self):
        self._get_values()

    @property
    def id(self):
        return self.hinuma_id
    
    @property
    def solid_angles(self):
        if self._solid_angles is None:
            self._get_solid_angles()

        return self._solid_angles

    @property
    def r_dashes(self):
        if self._r_dashes is None:
            self._get_r_dashes()

        return self._r_dashes
    
    @property
    def connections(self):
        if self._connections is None:
            self._get_connections()

        return self._connections

    def _get_values(self):
        with self.db.Session() as session:
            self._solid_angles, self._r_dashes = get_hinuma_values(session, self.hinuma_id)
            self._connections = get_hinuma_connections(session, self.hinuma_id)

    def _get_solid_angles(self):
        with self.db.Session() as session:
            self._solid_angles = get_hinuma_solid_angles(session, self.hinuma_id)

    def _get_r_dashes(self):
        with self.db.Session() as session:
            self._r_dashes = get_hinuma_r_dashes(session, self.hinuma_id)

    def _get_connections(self):
        with self.db.Session() as session:
            self._connections = get_hinuma_connections(session, self.hinuma_id)

    def __repr__(self):
        return f'hinuma_id={self.hinuma_id}'