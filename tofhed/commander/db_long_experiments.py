from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *


class DBLongExperiments(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_long_experiments())
    
    def new(self, setting_id, sequence_id, note=''):
        with self.db.Session() as session:
            long_experiment_id = new_long_experiment(session, setting_id, sequence_id, note)
        self[long_experiment_id] = DBLongExperiment(long_experiment_id, setting_id, sequence_id, self.db, note)
        return self[long_experiment_id]
        
    def _get_long_experiments(self):
        with self.db.Session() as session:
            long_experiments = get_long_experiments(session)
        return {i: DBLongExperiment(**long_experiments[i], db=self.db) for i in long_experiments}

    def __repr__(self):
        return f'LongExperiments({super().__repr__()})'

class DBLongExperiment:
    def __init__(self, long_experiment_id, setting_id, sequence_id, db, note=''):
        self.long_experiment_id = long_experiment_id
        self.setting_id = setting_id
        self.sequence_id = sequence_id
        self.note = note
        self.db = db
        self.ordered_experiments = None

    @property
    def settings(self):
        return self.db.settings[self.setting_id]
    
    @property
    def sequence(self):
        return self.db.sequences[self.sequence_id]
    
    @property
    def experiments(self):
        if self.ordered_experiments is None:
            self.ordered_experiments = self._get_experiments()
        return self.ordered_experiments


    def run_sequence(self, note=''):
        self.events = {}
        pyykko_connected = {}
        for i, system_id in enumerate(self.sequence.system_ids):
            experiment = self.run_experiment(system_id, note=note)
            # init base types on first run
            if i == 0:
                self.init_experiment = experiment.experiment_id
                self.base_types_dict = experiment.set_base_types()
                if len(experiment.hinumas) == 1:
                    self.hinuma_id = list(experiment.hinumas.keys())[0]
                else:
                    raise ValueError('More than one hinuma_id found')
                
                _surfaces = experiment.set_new_surfaces(hinuma_id=self.hinuma_id, frame_id=None)
                if _surfaces:
                    self.surface_group_id = _surfaces[0].surface_group_id
                    print('self.surface_group_id', self.surface_group_id)
                else:
                    raise ValueError('No surfaces found')
                self.surfaces = experiment.surface_groups[self.surface_group_id]
            pyykko_connected[system_id] = experiment.events._get_pyykko_connected_catalysis_sequence(self.surfaces, self.sequence)
            
        return pyykko_connected

    def run_experiment(self, system_id, note=''):
        experiment = self.db.experiments.new_experiment(self.setting_id, 
                                                        system_id=system_id,
                                                        note=note)
        self._new_sequence_experiment(experiment.experiment_id, note=note)
        sequence_order = self.sequence.get_system_id_order(system_id)
        self._update(sequence_order, experiment)
        return experiment

    def _new_sequence_experiment(self, experiment_id, sequence_order=None, note=''):
        experiment = self.db.experiments[experiment_id]
        if sequence_order is None:
            sequence_order = self.sequence.get_system_id_order(experiment.system_id)
        
        with self.db.Session() as session:
            new_sequence_experiment(session, self.long_experiment_id, self.sequence_id, 
                                        experiment_id, sequence_order, note=note)

    def _get_experiments(self):
        with self.db.Session() as session:
            sequence_experiments = get_sequence_experiments(session, self.long_experiment_id)
        
        for sequence_experiment in sequence_experiments:
            self._update(sequence_experiment.sequence_order, self.db.experiments[sequence_experiment.experiment_id])

    def _update(self, sequence_order, experiment):
        if self.ordered_experiments is None:
            self.ordered_experiments = {}
        self.ordered_experiments[sequence_order] = experiment

