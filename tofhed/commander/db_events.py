from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *
from tofhed.hinuma import PyykkoDistances
from tofhed.analysis import EventAnalysis

class DBEvents(SuperDict):
    '''
    Event Class in the shape of a dictionary.
    Schema: {frame_number: {chemical_id: (event_type, {catalyst_ids})}}
    '''
    def __init__(self, experiment, *args, **kwargs):
        self.experiment = experiment
        self.system = experiment.system
        self.db = experiment.db
        super().__init__(*args, **kwargs)

        self.settings = experiment.settings
        elements = self.system.atoms.atomic_numbers.values()
        self.pyykko = PyykkoDistances(elements)

        self._fudge_factor = None
        self._duration_cutoff = None
        self._pyykko_connected = None
        self._connected = None
        self._all_events = None
        self.analysis = EventAnalysis(experiment)

    @property
    def duration_cutoff(self):
        if self._duration_cutoff is None:
            self._duration_cutoff = self.settings.duration_cutoff
        return self._duration_cutoff
    
    @property
    def fudge_factor(self):
        if self._fudge_factor is None:
            self._fudge_factor = self.settings.fudge_factor
        return self._fudge_factor

    def pyykko_event_detection(self, surface_group_id,  write_to_db=True):
        surfaces = self.experiment.surface_groups[surface_group_id]
        _events = self._pyykko_event_detection(surfaces, write_to_db=write_to_db)
        self._update(_events)
        return self
    
    def _update(self, _events):
        for frame_number, events in _events.items():
            if frame_number not in self:
                self[frame_number] = events
            else:
                self[frame_number].update(events)

    def _filter_pyykko(self, distances):
        connected = defaultdict(dict)
        for frame_id, distance_frames in distances.items():
            for pair, distance in distance_frames.items():
                atomic_numbers = [self.system.atoms.atomic_numbers[atom_id] for atom_id in pair]
                if distance < self.pyykko(*atomic_numbers)*self.fudge_factor:
                    connected[frame_id][pair] = distance
        return connected

    def _get_pyykko_conntected(self, surfaces):
        '''CHECK IF STILL NEEDED!'''
        # TODO: Check if still needed
        frame_ids = self.system.frames.keys()
        max_cutoff = self.settings.approach_cutoff
        surface_atoms = [atom_id for surface in surfaces for atom_id in surface.atom_ids]
        chemical_atoms = self.experiment.chemical_ids
        distances = self.system.distances._get_capped_distances(frame_ids, surface_atoms, chemical_atoms, max_cutoff=max_cutoff)
        return self._filter_pyykko(distances)
    
    def _get_pyykko_connected_catalysis(self, surfaces):
        frame_ids = self.system.frames.keys()
        max_cutoff = self.settings.approach_cutoff
        surface_atoms = [atom_id for surface in surfaces for atom_id in surface.atom_ids]
        chemical_atoms = self.experiment.chemical_ids
        distances = self.system.distances._get_capped_distances(frame_ids, surface_atoms, chemical_atoms, max_cutoff=max_cutoff)
        
        pyykko_connected = {}
        for frame_id, connenctions in distances.items():
            connected = defaultdict(set)
            for pair, distance in connenctions.items():
                atomic_numbers = [self.system.atoms.atomic_numbers[atom_id] for atom_id in pair]
                if distance < self.pyykko(*atomic_numbers)*self.fudge_factor:
                    if pair[0] in chemical_atoms:
                        connected[pair[0]].add(pair[1])
                    elif pair[1] in chemical_atoms:
                        connected[pair[1]].add(pair[0])
                    else:
                        raise ValueError('Neither of the atoms in the pair is a chemical atom')
            pyykko_connected[frame_id] = dict(connected)
        return pyykko_connected
    
    def _get_pyykko_connected_catalysis_sequence(self, surfaces, sequence):
        max_cutoff = self.settings.approach_cutoff
        surface_atoms = [atom_id for surface in surfaces for atom_id in surface.atom_ids]
        chemical_atoms = self.experiment.chemical_ids
        distances = {}

        for seq, system in sequence.systems.items():
            frame_ids = system.frames.keys()
            distances.update(self.system.distances._get_capped_distances(frame_ids, surface_atoms, 
                                                                         chemical_atoms, max_cutoff=max_cutoff))
        
        pyykko_connected = {}
        for frame_id, connenctions in distances.items():
            connected = defaultdict(set)
            for pair, distance in connenctions.items():
                atomic_numbers = [self.system.atoms.atomic_numbers[atom_id] for atom_id in pair]
                if distance < self.pyykko(*atomic_numbers)*self.fudge_factor:
                    if pair[0] in chemical_atoms:
                        connected[pair[0]].add(pair[1])
                    elif pair[1] in chemical_atoms:
                        connected[pair[1]].add(pair[0])
                    else:
                        raise ValueError('Neither of the atoms in the pair is a chemical atom')
            pyykko_connected[frame_id] = dict(connected)
        return pyykko_connected

    def _convert_pyykko_connected(self, pyykko_connected):
        connected = {}
        for frame_id, chem_con in pyykko_connected.items():
            frame_number = self.system.frames[frame_id].frame_number
            for chem_id, con in chem_con.items():
                if chem_id not in connected:
                    connected[chem_id] = dict()
                if frame_number not in connected[chem_id]:
                    connected[chem_id][frame_number] = set()
                connected[chem_id][frame_number].update(con)
        return connected

    def _event_detection(self, connected, duration_cutoff=None):
        '''Detect events from connected atoms
        The connected atoms are a dictionary of the form:
        {chemical_id: {frame_number: {surface_atom_id}}}

        The events are a dictionary of the form:
        {frame_number: {chemical_id: (event_type, surface_atoms)}}
        
        '''
        if duration_cutoff is None:
            if self.duration_cutoff is None:
                self.duration_cutoff = 5
                print('WARNING: No duration cutoff given, using default value of 5 frames')
            else:
                duration_cutoff = self.duration_cutoff

        _events = defaultdict(dict)
        for chem_id, frame_con in connected.items():
            connection_duration = 0
            prev_frame_number = None
            for frame_number, surface_atoms in sorted(frame_con.items()):
                if prev_frame_number is None:
                    last_frame_number = max(frame_con.keys())
                    prev_frame_number = frame_number
                    connection_duration = 1
                    continue

                if frame_number - prev_frame_number == 1 and frame_number != last_frame_number:
                    connection_duration += 1
                    if surface_atoms != frame_con[prev_frame_number]:
                        # hopping
                        event_type = 'hopping'
                        _events[frame_number][chem_id] = (event_type, surface_atoms)
                elif frame_number - prev_frame_number > 1 or frame_number == last_frame_number:
                    # connection broken
                    _in = prev_frame_number - connection_duration + 1
                    _out = prev_frame_number + 1
                    if connection_duration > duration_cutoff:
                        # connection was longer than cutoff duration -> adsoption event
                        event_type_in = 'adsorption'
                        event_type_out = 'desorption'
                    else:
                        # connection was shorter than cutoff duration -> ping event
                        event_type_in = 'ping-in'
                        event_type_out = 'ping-out'
                    _events[_in][chem_id] = (event_type_in, frame_con[_in])
                    _events[_out][chem_id] = (event_type_out, surface_atoms)
                    connection_duration = 1
                prev_frame_number = frame_number
        return dict(_events)
    
    def _event_detection_extended(self, connected, duration_cutoff = None):
        '''Detect events from connected atoms
        The connected atoms are a dictionary of the form:
        {chemical_id: {frame_number: {surface_atom_id}}}
        
        '''
        hoppings = defaultdict(set)
        adsorbed = defaultdict(dict)
        ping = defaultdict(dict)
        connection_durations = defaultdict(dict)
        
        if duration_cutoff is None:
            if self.duration_cutoff is None:
                self.duration_cutoff = 5
                print('WARNING: No duration cutoff given, using default value of 5 frames')
            else:
                duration_cutoff = self.duration_cutoff

        _events = defaultdict(dict)

        for chem_id, frame_con in connected.items():
            connection_duration = 0
            prev_frame_number = None
            for frame_number, surface_atoms in sorted(frame_con.items()):
                if prev_frame_number is None:
                    last_frame_number = max(frame_con.keys())
                    prev_frame_number = frame_number
                    connection_duration = 1
                    continue

                if frame_number - prev_frame_number == 1 and frame_number != last_frame_number:
                    connection_duration += 1
                    if surface_atoms != frame_con[prev_frame_number]:
                        # hopping
                        event_type = 'hopping'
                        _events[frame_number][chem_id] = (event_type, surface_atoms)
                        hoppings[frame_number].add(chem_id)
                elif frame_number - prev_frame_number > 1 or frame_number == last_frame_number:
                    # connection broken
                    _in = prev_frame_number - connection_duration + 1
                    _out = prev_frame_number + 1
                    if connection_duration > duration_cutoff:
                        # connection was longer than cutoff duration -> adsoption event
                        event_type_in = 'adsorption'
                        event_type_out = 'desorption'
                    else:
                        # connection was shorter than cutoff duration -> ping event
                        event_type_in = 'ping-in'
                        event_type_out = 'ping-out'
                    _events[_in][chem_id] = (event_type_in, frame_con[_in])
                    _events[_out][chem_id] = (event_type_out, surface_atoms)
                    
                    # add to adsorbed or ping 
                    for d, frame in enumerate(range(_in, _out)):
                        if event_type_in == 'adsorption':
                            adsorbed[frame][chem_id] = surface_atoms
                        elif event_type_in == 'ping-in':
                            ping[frame][chem_id] = surface_atoms
                        connection_durations[frame][chem_id] = d + 1

                    connection_duration = 1
                prev_frame_number = frame_number
        return dict(_events), dict(hoppings), dict(adsorbed), dict(ping), dict(connection_durations)


    def write_events(self, _events):
        write_event_atoms = []
        experiment_id = self.experiment.id
        for frame_number, chem_events in sorted(_events.items()):
            frame_id = self.experiment.frames.number_to_id[frame_number]
            for chem_id, (event_type, catalyst_atoms) in chem_events.items():
                event_type_id = self.db.event_types[event_type]
                with self.experiment.db.Session() as session:
                    event_id = new_event(session, experiment_id, frame_id, event_type_id)

                write_event_atoms.extend(
                    [{
                        'event_id': event_id,
                        'atom_id': cat_atom_id,
                    } for cat_atom_id in catalyst_atoms]
                )
                write_event_atoms.append(
                    {
                        'event_id': event_id,
                        'atom_id': chem_id, 
                    }
                )

        with self.db.Session() as session:
            session.bulk_insert_mappings(EventAtom, write_event_atoms)
            session.commit()

        return write_event_atoms
    
    def write_pyykko_connected(self, pyykko_connected):
        write_connection_dicts = []
        connection_type_id = self.db.connection_types.name_to_id['pyykko']
        experiment_id = self.experiment.id

        for frame_id in pyykko_connected:
            pairs = set()
            for chem_id in pyykko_connected[frame_id]:
                for catalyst_id in pyykko_connected[frame_id][chem_id]:
                    pairs.add(tuple(sorted([chem_id, catalyst_id])))
            pair_ids = [self.system.pair_ids[pair] for pair in pairs]
            temp_ = new_connections_dict(pair_ids, frame_id, experiment_id, connection_type_id)
            write_connection_dicts.extend(temp_)
        
        with self.db.Session() as session:
            session.bulk_insert_mappings(Connection, write_connection_dicts)
            session.commit()

        return write_connection_dicts

    def _pyykko_event_detection(self, surfaces, write_to_db = True):
        pyykko_connected = self._get_pyykko_connected_catalysis(surfaces)
        connected = self._convert_pyykko_connected(pyykko_connected)
        _events = self._event_detection(connected)

        self._pyykko_connected = pyykko_connected
        self._connected = connected
        self._all_events = _events

        if write_to_db:
            write_connection_dicts = self.write_pyykko_connected(pyykko_connected)
            write_event_atoms = self.write_events(_events)

        return dict(sorted(_events.items()))
    
    def _get_pyykko_connected_from_db(self):
        experiment_id = self.experiment.id
        connection_type_id = self.db.connection_types.name_to_id['pyykko']
        chemical_ids = self.experiment.chemical_ids
        catalyst_ids = self.experiment.catalyst_ids

        with self.db.Session() as session:
            pyykko_connected = get_heterogeneous_connections(session, experiment_id, connection_type_id, 
                                                             chemical_ids, catalyst_ids)
        if len(pyykko_connected) == 0:
            raise ValueError('No Pyykko connections found in the database')
        return pyykko_connected

    def _all_connection_durations(self, connected):
        connection_durations = dict()

        for chem_id in connected:
            connecting_frame_numbers = set()
            for frame_number in connected[chem_id]:
                connecting_frame_numbers.add(frame_number)
            connection_durations[chem_id] = self._get_connection_duration(connecting_frame_numbers)
        return connection_durations

    def _get_connection_duration(self, connecting_frame_numbers):
        connection_duration = 0
        previous_frame_number = None
        connection_duration_dict = dict()

        for frame_number in sorted(connecting_frame_numbers):
            if previous_frame_number is None:
                previous_frame_number = frame_number
                connection_duration = 1
                connection_duration_dict[frame_number] = connection_duration
                continue

            if frame_number == previous_frame_number + 1:
                connection_duration += 1

            else:
                connection_duration = 1

            previous_frame_number = frame_number
            connection_duration_dict[frame_number] = connection_duration
        return connection_duration_dict

    @property
    def pyykko_connected(self):
        '''pyykko_connected[frame_id][chem_id][catalyst_id]'''
        if self._pyykko_connected is None:
            self._pyykko_connected = self._get_pyykko_connected_from_db()

        return self._pyykko_connected
    
    @property
    def connected(self):
        '''connected[chem_id][frame_number]{catalyst_ids}'''
        if self._connected is None:
            self._connected = self._convert_pyykko_connected(self.pyykko_connected)
        return self._connected
    
    @property
    def all_events(self):
        ''' _events[frame_number][chem_id](event_type, {catalyst_ids})'''
        if self._all_events is None:
            if self.connected is None:
                # TODO: Read events from database
                print('TODO: Read events directly from database')
                self._all_events = self._event_detection(self.connected)

            else:
                self._all_events = self._event_detection(self.connected)
        return self._all_events
