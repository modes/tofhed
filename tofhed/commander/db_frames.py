from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *


class DBFrames(SuperDict):
    def __init__(self, system, *args, **kwargs):
        self.system = system
        self.db = system.db
        super().__init__(*args, **kwargs)
        self.update(self._get_frames())
        self._number_to_id = None
        self._id_to_number = None
    
    @property 
    def number_to_id(self):
        if self._number_to_id is None:
            self._number_to_id = {frame.frame_number: frame_id for frame_id, frame in self.items()}
        return self._number_to_id

    @property 
    def id_to_number(self):
        if self._id_to_number is None:
            self._id_to_number = {frame_id: frame.frame_number for frame_id, frame in self.items()}
        return self._id_to_number

    @property
    def ids(self):
        return [self.number_to_id[n] for n in self.numbers]

    @property
    def numbers(self):
        return list(sorted(self.number_to_id.keys()))

    def from_frame_number(self, frame_number):
        return self[self.number_to_id[frame_number]]

    def _get_frames(self):
        with self.db.Session() as session:
            return {f['frame_id']: DBFrame(f, self.system) for f in get_frames(session, self.system.system_id)}


class DBFrame:
    def __init__(self, frame_dict, system):
        self.__dict__.update(frame_dict)
        self.system = system
        self.db = system.db
        self._positions = None

    @property
    def id(self):
        return self.frame_id
    
    @property
    def number(self):
        return self.frame_number

    @property
    def pos(self):
        return self.positions

    @property
    def positions(self):
        if self._positions is None:
            self._positions = self._get_positions()

        return self._positions

    def _get_positions(self):
        # TODO: Test if in...
        if not self.system._positions is None and self.frame_number in self.system._positions:
            return self.system._positions[self.frame_number]

        elif self.system.file.import_positions:
            return self._get_positions_from_db()

        else:
            return self.system.read_frame_ase(self.frame_number).get_positions()

    def _get_positions_from_db(self):
        with self.db.Session() as session:
            return get_positions_for_frame(session, self.system.system_id, self.frame_id)

    def __repr__(self):
        return f'frame_id={self.frame_id}, frame_number={self.frame_number}'
