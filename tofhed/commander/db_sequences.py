from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *


class DBSequences(SuperDict):
    def __init__(self, db, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = db
        self._update()

    def reload(self):
        self._update()

    def _update(self):
        self.update(self._get_sequences_from_db())
    
    def _get_sequences_from_db(self):
        with self.db.Session() as session:
            sequence_ids = get_sequence_ids(session)
            if not sequence_ids:
                return {}
            return {sequence_id: DBSequence(sequence_id, 
                                            get_sequence(session, sequence_id), 
                                            self.db) for sequence_id in sequence_ids}

    def _get_sequence_from_db(self, sequence_id):
        with self.db.Session() as session:
            return get_sequence(session, sequence_id)


class DBSequence:
    def __init__(self, sequence_id, sequence_order, db):
        self.sequence_id = sequence_id
        self.order = self._sort_dict_order(sequence_order)
        self.db = db
        self._system_id_order = None

    @property
    def systems(self):
        return {seq: self.db.systems[system_id] for seq, system_id in self.order.items()}
    
    @property
    def system_ids(self):
        return list(self.order.values())
    
    def frames(self):
        for system_id in self.system_ids:
            for frame in self.db.systems[system_id].frames:
                yield frame

    def get_system_id_order(self, system_id):
        if self._system_id_order is None:
            self._system_id_order = {system_id: seq for seq, system_id in self.order.items()}

        return self._system_id_order[system_id]

    def _sort_dict_order(self, sequence_order):
        return {seq: system_id for seq, system_id in sorted(sequence_order.items(), key=lambda x: x[0])}

    def __repr__(self):
        return f'DBSequence(sequence_id={self.sequence_id})'