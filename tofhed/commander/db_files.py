from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *
from tofhed.utils import read, create_frame_range
from tofhed.commander.db_systems import DBSystem
from pathlib import Path


class DBFiles(SuperDict):
    def __init__(self, db, *args, **kwargs):
        super(DBFiles, self).__init__(*args, **kwargs)
        self.db = db
        self._update()

    @property
    def paths(self):
        return [f.path for f in self.values()]

    def _update(self):
        self.update(self._get_files())

    def import_ase_sequence(self, file_paths, init_frame_number=0, frame_range=(0,-1), note='', 
                            sequence_id=None, write_split=100000, import_positions=False):
        if sequence_id is None:
            order_start = 0
            with self.db.Session() as session:
                sequence_id = new_sequence(session)
        else:
            with self.db.Session() as session:
                order_start = get_last_order_number(session, sequence_id)

        for i, path in enumerate(file_paths):
            sequence_order = order_start + i
            file = self.import_ase(path, init_frame_number, frame_range=frame_range,
                                   is_sequence=True, sequence_id=sequence_id, sequence_order=sequence_order,
                                   note=note, write_split=write_split, import_positions=import_positions)
            system_id = file.latest_system.id
            with self.db.Session() as session:
                new_sequence_system(session, sequence_id, system_id, sequence_order)
            
            print(f'Progress: {sequence_order+1}/{len(file_paths)}')
        self.db.sequences.reload()
        print('Done')
        return sequence_id
    
    def import_ase(self, path, init_frame_number=0, frame_range=(0,-1), 
                   is_sequence=False, sequence_id=None, sequence_order=None,
                   note='', write_split=100000, import_positions=True):
        check_path(path)
        
        frames = read(path)
        file_id = self._import_from_frames(frames, path, init_frame_number,
                                           frame_range=frame_range, is_sequence=is_sequence, 
                                           sequence_id=sequence_id, sequence_order=sequence_order,
                                           note=note, write_split=write_split, 
                                           import_positions=import_positions)
        return self.db.files[file_id]

    def _import_from_frames(self, frames, path, init_frame_number, frame_range=(0,-1), 
                            is_sequence=False, sequence_id=None, sequence_order=None,
                            note='', write_split=100000, import_positions=True):
        # TODO: Auto Check if system changes and split into multiple systems

        _system = DBSystem({}, self.db)

        # Check if frames is a list
        if not isinstance(frames, list):
            frames = [frames]

        len_frames = len(frames)

        # Check if init_frame_number is relative to the end
        if init_frame_number < 0:
            init_frame_number = len_frames + init_frame_number

        # Check if path is in the database
        if path in self.paths:
            raise ValueError(f'File {path} already in database. Import aborted.')

        # System
        _system.path = path
        _system.note = note

        frame_number_first, frame_number_last = create_frame_range(frame_range, len_frames)

        _system.frame_number_first = frame_number_first
        _system.frame_number_last = frame_number_last

        _system.is_sequence = is_sequence
        _system.sequence_id = sequence_id
        _system.sequence_order = sequence_order
        
        _system.pbc = frames[init_frame_number].get_pbc()
        _system.cell = frames[init_frame_number].cell
        _system.box = frames[init_frame_number].cell.cellpar()
        
        frame_numbers = list(range(len(frames)))
        atom_numbers = list(range(len(frames[init_frame_number])))

        # Initial tags
        _system._init_tags = frames[init_frame_number].get_tags()
        _system.import_tags = {frame_number: atoms.get_tags() for frame_number, atoms in enumerate(frames)}

        # Elements
        _system.chem_sym = frames[init_frame_number].get_chemical_symbols()
        _system.atomic_numbers = frames[init_frame_number].get_atomic_numbers()

        with _system.db.Session() as session:
            _system.file_id = new_file(session, _system.path, import_positions)
            _system.system_id = new_system(session, _system.pbc, _system.box, _system.cell, 
                                           _system.file_id, _system.frame_number_first, 
                                           _system.frame_number_last, _system.is_sequence,
                                           _system.sequence_id, _system.note)
            atom_ids = new_atoms(session, atom_numbers, _system.atomic_numbers, _system.system_id, _system._init_tags)
            frame_ids = new_frames(session, frame_numbers, _system.system_id)

        _system.init_frame_id = frame_ids[init_frame_number]

        if not import_positions:
            self.db.refresh()
            return _system.file_id

        # Positions
        write_positions = []
        # TODO: pos_dict und tag_dict replacement
        _system.pos_dict = {}
        _system.tag_dict = {}
        _system.frame_id_convert = {}

        for frame_number, frame_id in frame_ids.items():
            pos = frames[frame_number].get_positions()
            tags = _system.import_tags[frame_number]

            _system.pos_dict[frame_id] = {k: tuple(v) for k, v in enumerate(pos)}
            _system.tag_dict[frame_id] = {k: v for k, v in enumerate(tags)}
            _system.frame_id_convert[frame_id] = frame_number
            write_positions += new_positions_dicts(atom_ids, frame_id, pos, tags)

            if len(write_positions) > write_split:
                with _system.db.Session() as session:
                    session.bulk_insert_mappings(Position, write_positions)
                    session.commit()
                write_positions = []

        if len(write_positions) > 0:
            with _system.db.Session() as session:
                session.bulk_insert_mappings(Position, write_positions)
                session.commit()

        self.db.refresh()

        return _system.file_id

    def _get_files(self):
        with self.db.Session() as session:
            return {f['file_id']: DBFile(f['file_id'], f, self.db) for f in get_files(session)}


class DBFile:
    """
    Notes
    -----
        It contains the following keys:
        - file_id
        - path
        - created_at
        - import_positions
    """

    def __init__(self, file_id, file_dict, db):
        self.file_id = file_id
        self.__dict__.update(file_dict)
        self.db = db

    @property
    def id(self):
        return self.file_id

    @property
    def systems(self):
        return {system.system_id: system for system in self.db.systems if system.file_id == self.file_id} 
    
    @property
    def name(self):
        return Path(self.path).name
    
    @property
    def latest_system(self):
        return max(self.systems.values(), key=lambda x: x.created_at)

    def all(self):
        return dict(self.__dict__)


    def read_frame_ase(self, frame_number):
        return read(self.path, index=frame_number)

    def __repr__(self):
        return f'file_id={self.file_id}'
