from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *


class DBProvenances(SuperDict):
    """
    Contains all provenances for a given experiment. 

    The provenances are stored in a 2D dictionary, 
    where the first key is the frame_id and the second key is the atom_id.

    Alternatively, the provenances can be accessed directly by their provenance_id with the all attribute.
    """
    def __init__(self, experiment, *args, **kwargs):
        self.experiment = experiment
        self.system = experiment.system
        self.db = experiment.db
        super().__init__(*args, **kwargs)
        self.all = self._get_provenances()
        self.update(self._sort_2d())

    def _sort_2d(self):
        dict_2d = defaultdict(dict)
        for p in self.all.values():
            dict_2d[p.frame_id][p.atom_id] = p
        return dict(dict_2d)

    def _get_provenances(self):
        with self.db.Session() as session:
            return {p['provenance_id']: DBProvenance(p, self.experiment) for p in get_provenances_iter(session, self.experiment.experiment_id)} 
    

class DBProvenance:
    def __init__(self, provenance_dict, experiment):
        self.experiment = experiment
        self.db = experiment.db
        self.__dict__.update(provenance_dict)
    
    @property
    def frame(self):
        return self.experiment.frames[self.frame_id]
    
    @property
    def atom(self):
        return self.experiment.atoms[self.atom_id]
    
    def all(self):
        return dict(self.__dict__)
    
    def __repr__(self):
        return f'provenance_id={self.provenance_id}'