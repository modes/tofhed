from pathlib import Path
from tofhed.functions import *
from sqlalchemy.orm import sessionmaker
from tofhed.commander.super_dict import SuperDict
from tofhed.commander.db_settings import DBSettingsDict
from tofhed.commander.db_settings import DBSettings
from tofhed.commander.db_files import DBFiles
from tofhed.commander.db_systems import DBSystems
from tofhed.commander.db_experiments import DBExperiments
from tofhed.hinuma import PyykkoDistances
from tofhed.commander.db_sequences import DBSequences
from tofhed.commander.db_long_experiments import DBLongExperiments
import nest_asyncio


class DataBase:
    """
    A class to handle the database.

    Parameters
    ----------
    path : str or pathlib.Path
        Path to the database.
    **kwargs
        Keyword arguments to pass to the database engine.
    
    Notes
    -----
    Possible kwargs are:
        - echo : bool
            If True, the SQL statements are printed to the console.
        - dialect : str
            The database dialect. Default is 'sqlite+pysqlite'.
    """
    def __init__(self, path=':memory:', **kwargs):
        self.path = path

        if is_jupyter():
            print('Jupyter detected. Applying nest_asyncio.')
            nest_asyncio.apply()

        if path == ':memory:':
            self.engine = build_db()

        elif path == ':postgresql:':
            engine_path = get_engine_path(path, **kwargs)
            status = database_status(engine_path)
            
            print(status)

            if status == 'Database does not exist':
                self.engine = build_db(engine_path, **kwargs)
            elif status == 'Connected successfully':
                self.engine = get_engine(engine_path, **kwargs)
            else:
                print("Connection could not be established")
                return False

        elif not Path(path).exists():
            engine_path = get_engine_path(path, **kwargs)
            self.engine = build_db(engine_path, **kwargs)
        else:
            self.engine = get_engine(path, **kwargs)

        self.Session = sessionmaker(bind=self.engine)
        self.refresh()

    def refresh(self):
        self.settings = DBSettingsDict(self)
        self.files = DBFiles(self)
        self.systems = DBSystems(self)
        self.experiments = DBExperiments(self)
        self.base_types = DBBaseTypes(self)
        self.event_types = DBEventTypes(self)
        self.properties = DBProperties(self)
        self.surface_types = DBSurfaceTypes(self)
        self.pyykko_distance = PyykkoDistances()
        self.element = mendeleev.element
        self.sequences = DBSequences(self)
        self.long_experiments = DBLongExperiments(self)
        self._frame_id_to_system_id = None
        self.connection_types = DBConnectionTypes(self)

    @property
    def frame_id_to_system_id(self):
        if self._frame_id_to_system_id is None:
            self._frame_id_to_system_id = {frame.id: system.id for system in self.systems for frame in system.frames}
        return self._frame_id_to_system_id

    def atom_id_to_system_id(self, atom_id):
        with self.Session() as session:
            system_id = get_system_id_for_atom_id(session, atom_id)
        return system_id

    def import_file(self, *args, **kwargs):
        file = self.files.import_ase(*args, **kwargs)
        system = file.latest_system
        return file, system

    def import_experiment_from_file(self, path_xyz, settings, note='', frame_range=(0,-1), import_positions=True, **kwargs):
        if isinstance(settings, (str, Path)):
            settings = self.settings.new_settings_from_toml(settings)
        elif isinstance(settings, dict):
            settings = self.settings.new_settings(settings)
        elif isinstance(settings, DBSettings):
            pass
        else:
            raise TypeError(f'Incorrect Type. `settings` has to be `str`, `Path`, `dict`, or `DBSettingsDict` but is {type(settings)}')
        
        file, system = self.import_file(path_xyz, init_frame_number=settings.init_frame_number,
                                        note=note, frame_range=frame_range, import_positions=import_positions, **kwargs)

        experiment = self.experiments.new(settings.setting_id, 
                                          system_id=system.system_id,
                                          note=note)
        
        return experiment

    def __repr__(self):
        return f'DataBase(path={self.path})'
    
    def __len__(self):
        return len(self.systems)
    

class DBBaseTypes(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_base_types())

    @property
    def id_to_name(self):
        return self 

    @property
    def name_to_id(self):
        return {v: k for k, v in self.items()}

    def _get_base_types(self):
        with self.db.Session() as session:
            return get_base_types_dict(session)
        

class DBEventTypes(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_event_types())

    @property
    def id_to_name(self):
        return self 
    
    @property
    def name_to_id(self):
        return {v: k for k, v in self.items()}

    def _get_event_types(self):
        with self.db.Session() as session:
            return get_event_types(session)
        

class DBProperties(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_properties())

    def _get_properties(self):
        with self.db.Session() as session:
            _properties = {}
            for p_id, p in get_properties_dict(session).items():
                _properties[p_id] = DBProperty(self.db, p)
            return _properties
    
    @property
    def id_to_name(self):
        return {k: v.name for k, v in self.items()}
    
    @property
    def name_to_id(self):
        return {v.name: k for k, v in self.items()}
    
    def __repr__(self):
        return str(self.id_to_name)
    

class DBProperty:
    def __init__(self, db, property_dict):
        self.db = db
        self.__dict__.update(property_dict)

    def __repr__(self):
        return f'{self.display_name}: {self.id} - {self.name}'


class DBSurfaceTypes(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_surface_types())

    @property
    def id_to_name(self):
        return self 
    
    @property
    def name_to_id(self):
        return {v: k for k, v in self.items()}

    def _get_surface_types(self):
        with self.db.Session() as session:
            return get_surface_types_dict(session)


class DBConnectionTypes(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_connection_types())

    @property
    def id_to_name(self):
        return self 
    
    @property
    def name_to_id(self):
        return {v: k for k, v in self.items()}

    def _get_connection_types(self):
        with self.db.Session() as session:
            return get_connection_types_dict(session)
