from tofhed.commander.super_dict import SuperDict
from tofhed.functions import *
import toml


class DBSettingsDict(SuperDict):
    def __init__(self, db, *args, **kwargs):
        self.db = db
        super().__init__(*args, **kwargs)
        self.update(self._get_settings())

    def new(self, settings_dict):
        return self.new_settings(settings_dict)
    
    def new_settings(self, settings_dict):
        with self.db.Session() as session:
            setting_id = set_settings(session, settings_dict)
            self[setting_id] = DBSettings(setting_id, settings_dict, self.db)
            return self[setting_id]
    
    def new_from_toml(self, path_toml):
        return self.new_settings_from_toml(path_toml)

    def new_settings_from_toml(self, path_toml):
        check_path(path_toml)
        settings_dict = self._load_toml_settings(path_toml)
        return self.new_settings(settings_dict)
        
    def _load_toml_settings(self, path_toml):
        with open(path_toml, 'r') as f:
            settings_dict = toml.load(f)
        return settings_dict

    def _get_settings(self):
        with self.db.Session() as session:
            return {s_id: DBSettings(s_id, s, self.db) for s_id, s in get_all_settings(session).items()}


class DBSettings:
    def __init__(self, setting_id, settings_dict, db):
        self.setting_id = setting_id
        self.db = db
        self._update(settings_dict)

    @property
    def id(self):
        return self.setting_id

    def _update(self, settings_dict):
        if 'chemical_elements' in settings_dict:
            settings_dict['chemical_elements'] = settings_dict['chemical_elements'].split()
        if not 'xyz_filter' in settings_dict:
            settings_dict['xyz_filter'] = None

        self.__dict__.update(settings_dict)

    def new_experiment(self, system_id, note=''):
        return self.db.experiments.new_experiment(self.setting_id, system_id, note)
    
    def all(self):
        return dict(self.__dict__)
    
    def __repr__(self):
        return f'setting_id={self.setting_id}'
    