import numpy as np
import scipy as sp
import os
from numba import njit, prange
from MDAnalysis.lib import distances as mda_distances
from collections.abc import Iterable
from collections import OrderedDict
import mendeleev
from concurrent.futures import ThreadPoolExecutor


@njit
def area_spherical_triangle(v1, v2, v3):
    """Returns the area of a spherical triangle. Input is a list of vectors.
    
    """
    frac_u = 1 + np.vdot(v1, v2) + np.vdot(v1, v3) + np.vdot(v2, v3)
    frac_d = np.sqrt(2*(1 + np.vdot(v1, v2))
                     * (1 + np.vdot(v1, v3))
                     * (1 + np.vdot(v2, v3)))
    area = 2*np.arccos(frac_u / frac_d)

    return area


@njit(parallel=True)
def calc_surface_area(hull_connections, hull_vectors):
    """Returns the surface area of all connected atoms

    """
    surface_area = 0
    for i in prange(len(hull_connections)):
        v1, v2, v3 = [hull_vectors[v_id] for v_id in hull_connections[i]]

        frac_u = 1 + np.vdot(v1, v2) + np.vdot(v1, v3) + np.vdot(v2, v3)
        frac_d = np.sqrt(2*(1 + np.vdot(v1, v2))
                         * (1 + np.vdot(v1, v3))
                         * (1 + np.vdot(v2, v3)))
        div = frac_u / frac_d
        if div > 1:
            div = 1

        area = 2*np.arccos(div)
        surface_area += area
 
    return surface_area


def get_pyykko_distance(element_i, element_j):
    """Returns the pyykko distance between two elements. In Angstrom.
    
    """
    distance = mendeleev.element(element_i).covalent_radius_pyykko + \
        mendeleev.element(element_j).covalent_radius_pyykko
    return distance*0.01


class PyykkoDistances:
    def __init__(self, elements=None):
        self.distances = {}
        if elements is not None:
            self.precompute(elements)

    def precompute(self, elements):
        elements = set(elements)
        for element_i in elements:
            for element_j in elements:
                pair = self._pair(element_i, element_j)
                if pair not in self.distances:
                    self.distances[pair] = get_pyykko_distance(*pair)
    
    def _pair(self, element_i, element_j):
        return tuple(sorted([element_i, element_j]))

    def __call__(self, element_i, element_j):
        pair = self._pair(element_i, element_j)
        if pair not in self.distances:
            self.distances[pair] = get_pyykko_distance(*pair)
        
        return self.distances[pair]
    
    def __repr__(self) -> str:
        return f"PyykkoDistances({self.distances})"


def surface_classifier(pos, cell, pbc, box, cutoff=3.5, zero_vec_tolerance=0.0001,
                       mic=True, xyz_filter=None, dynamic_cutoff=False, 
                       atomic_numbers=None, pyykko_fudge=1.7, n_threads=None, 
                       return_distances=False, **kwargs):
    """
    Classifies atoms based on their surface areas using the Hinuma et al. algorithm.

    This function identifies the surface atoms based on the method described in:
    Hinuma, Yoyo & Kamachi, Takashi & Hamamoto, Nobutsugu. (2020). 
    Algorithm for Automatic Detection of Surface Atoms. 
    Transactions of the Materials Research Society of Japan. 45. 115-120. 
    DOI 10.14723/tmrsj.45.115.

    Parameters
    ----------
    pos : list of tuple
        List of xyz-positions of each atom as [(x1, y1, z1), (x2, y2, z2), ...].
    cell : 3x3 matrix or list of length 3 or 6
        Describes unit cell vectors. For orthorhombic cells, can be given as three numbers.
        For non-orthorhombic cells, six numbers represent [len(a), len(b), len(c), 
        angle(b,c), angle(a,c), angle(a,b)].
    pbc : bool or tuple of bool
        Specifies periodic boundary conditions. Examples: True, (1, 1, 0).
    box : ndarray
        Specifies the simulation box.
    cutoff : float, optional (default=3.5)
        Specifies the distance cutoff in angstrom.
    zero_vec_tolerance : float, optional (default=0.0001)
        Tolerance for considering normalized distance vectors as identical.
    mic : bool, optional (default=True)
        Applies minimum image convention if set to True.
    xyz_filter : callable, optional
        A function to filter a list of vectors.
    dynamic_cutoff : bool, optional (default=False)
        Dynamically adjusts the cutoff based on pyykko distance if set to True.
    atomic_numbers : list of int, optional
        Specifies atomic numbers of atoms in 'pos'. Required if dynamic_cutoff is True.
    pyykko_fudge : float, optional (default=1.7)
        Factor to adjust pyykko distances.
    n_threads : int, optional
        Number of threads to use. By default, uses all available CPU cores.
    return_distances : bool, optional (default=False)
        Returns distance information if set to True.

    Returns
    -------
    surface_area_list : list
        Surface areas per atom according to the Hinuma paper. The index corresponds to 'pos'.
    vec_r_dash_list : list
        List of r' vectors in order of 'pos'.
    connection_list : list of ndarray
        Indices of points forming simplices in triangulation.
    distance_cache : list of tuple (optional)
        Contains 'distance_vecs' (distance vectors) and 'distances' (euclidean norms).

    Raises
    ------
    ValueError
        If atomic_numbers is None while dynamic_cutoff is True.

    Notes
    -----
    - Ensure all dependent functions are imported or present in the module.
    - If other keywords are supported in the future via **kwargs, update the docstring accordingly.

    """

    org_atom_ids = generate_org_atom_ids(pos)
    
    if box is not None:
        box = box.astype(np.single)
    
    if mic:
        all_atom_pos, all_atom_ids = augment_positions(pos, box, cutoff)
    else:
        all_atom_pos, all_atom_ids = pos, org_atom_ids

    distance_dict = get_mda_distance_dict_filter(all_atom_pos, cutoff, box=None,
                                                 filter_ids=org_atom_ids)

    surface_area_list = []
    vec_r_dash_list = []
    connection_list = []
    
    pyykko_distance = None
    if dynamic_cutoff:
        if atomic_numbers is None:
            raise ValueError('Atomic numbers are required for dynamic cutoff')
        pyykko_distance = PyykkoDistances()
        pyykko_distance.precompute(set(atomic_numbers))

    args = []
    for atom_i_id, capped_atoms_list in distance_dict.items():
        args.append((atom_i_id, all_atom_pos, all_atom_ids, 
                    capped_atoms_list, cutoff, zero_vec_tolerance, 
                    xyz_filter, dynamic_cutoff, atomic_numbers,
                    pyykko_distance, pyykko_fudge))

    if n_threads is None or n_threads > 1:
        if n_threads is None:
            n_threads = os.cpu_count()        
        with ThreadPoolExecutor(max_workers=n_threads) as executor:
            results = list(executor.map(lambda x: process_atom(*x), args))
            executor.shutdown(wait=True) # wait for all threads to finish
    else:
        results = [process_atom(*x) for x in args]

    for vec_r_dash, connection_ids, surface_area in results:
        connection_list.append(connection_ids)
        vec_r_dash_list.append(vec_r_dash)
        surface_area_list.append(surface_area)


    if return_distances:
        org_distance_dict = {}
        for i, dists in distance_dict.items():
            org_distance_dict[i] = {all_atom_ids[j]: dist for j, dist in dists}
        
        if 'debug' in kwargs and kwargs['debug']:
            return surface_area_list, vec_r_dash_list, connection_list, org_distance_dict, all_atom_pos, all_atom_ids
        
        return surface_area_list, vec_r_dash_list, connection_list, org_distance_dict

    return surface_area_list, vec_r_dash_list, connection_list


def surface_classifier_selected(pos, filter_ids, box, cutoff=3.5, zero_vec_tolerance=0.0001,
                           mic=True, xyz_filter=None, dynamic_cutoff=False, 
                           atomic_numbers=None, pyykko_fudge=1.7, 
                           n_threads=None, return_distances=False, **kwargs):

    org_atom_ids = generate_org_atom_ids(pos)

    if box is not None:
        box = box.astype(np.single)
    
    if mic:
        all_atom_pos, all_atom_ids = augment_positions(pos, box, cutoff)
    else:
        all_atom_pos, all_atom_ids = pos, org_atom_ids

    distance_dict = get_mda_distance_dict_filter_selected(all_atom_pos, cutoff, box=None,
                                                 filter_ids=filter_ids) 

    surface_area_list = []
    vec_r_dash_list = []
    connection_list = []
    
    pyykko_distance = None
    if dynamic_cutoff:
        if atomic_numbers is None:
            raise ValueError('Atomic numbers are required for dynamic cutoff')
        pyykko_distance = PyykkoDistances()
        pyykko_distance.precompute(set(atomic_numbers))

    args = []
    for atom_i_id, capped_atoms_list in distance_dict.items():
        args.append((atom_i_id, all_atom_pos, all_atom_ids, 
                    capped_atoms_list, cutoff, zero_vec_tolerance, 
                    xyz_filter, dynamic_cutoff, atomic_numbers,
                    pyykko_distance, pyykko_fudge))

    if n_threads is None or n_threads > 1:
        if n_threads is None:
            n_threads = os.cpu_count()        
        with ThreadPoolExecutor(max_workers=n_threads) as executor:
            results = list(executor.map(lambda x: process_atom(*x), args))
            executor.shutdown(wait=True) # wait for all threads to finish
    else:
        results = [process_atom(*x) for x in args]

    for vec_r_dash, connection_ids, surface_area in results:
        connection_list.append(connection_ids)
        vec_r_dash_list.append(vec_r_dash)
        surface_area_list.append(surface_area)


    if return_distances:
        org_distance_dict = {}
        for i, dists in distance_dict.items():
            org_distance_dict[i] = {all_atom_ids[j]: dist for j, dist in dists}
        
        if 'debug' in kwargs and kwargs['debug']:
            return surface_area_list, vec_r_dash_list, connection_list, org_distance_dict, all_atom_pos, all_atom_ids
        
        return surface_area_list, vec_r_dash_list, connection_list, org_distance_dict



    return surface_area_list, vec_r_dash_list, connection_list



def get_mda_distance_dict_filter_selected(pos, cutoff, box, filter_ids):
    pos_ref = np.array([pos[i] for i in filter_ids])

    dist_pair_ids, pair_distances = mda_distances.capped_distance(pos_ref, pos,
                                                                max_cutoff=cutoff,
                                                                box=box,
                                                                return_distances=True)

    orig_pair_ids = [[filter_ids[i], j] for i,j in dist_pair_ids]

    dist_dict = OrderedDict()

    # if filter_id is in one of the ids within orig_pair_ids, 
    # then add the other id and distance
    for filter_id in sorted(filter_ids):
        dist_dict[filter_id] = [(ids[1], dist) if ids[0] == filter_id else (ids[0], dist)
                                for ids, dist in zip(orig_pair_ids, pair_distances)
                                if filter_id in ids and ids[0] != ids[1]]

    return dist_dict


def generate_org_atom_ids(pos):
    return list(range(len(pos)))


def augment_positions(pos, box, cutoff):
    ext_pos, ext_atom_ids = mda_distances.augment_coordinates(pos.astype(np.single),
                                                              box,
                                                              np.single(cutoff))
    all_atom_pos = np.concatenate([pos, ext_pos])
    all_atom_ids = np.concatenate([generate_org_atom_ids(pos), ext_atom_ids])
    return all_atom_pos, all_atom_ids


def process_atom(atom_i_id, all_atom_pos, all_atom_ids, capped_atoms_list,
                 cutoff, zero_vec_tolerance, xyz_filter,
                 dynamic_cutoff=False, atomic_numbers=None,
                 pyykko_distance=None, pyykko_fudge=1.7):
    
    xyz_filter_func = get_filter(xyz_filter)

    dist_vecs_i, dists_i, dists_ids_j = [], [], []
    vec_i = all_atom_pos[atom_i_id]
    for atom_j_id, dist in capped_atoms_list:
        orig_atom_j_id = all_atom_ids[atom_j_id]
        
        # Pyykko's filter
        if dynamic_cutoff:
            element_i = atomic_numbers[atom_i_id]
            element_j = atomic_numbers[orig_atom_j_id]
            if dist > pyykko_distance(element_i, element_j) * pyykko_fudge:
                continue
                
        vec_j = all_atom_pos[atom_j_id]
        dist_vec = vec_j - vec_i
        dist_vecs_i.append(dist_vec)
        dists_i.append(dist)
        dists_ids_j.append(orig_atom_j_id)
    
    vec_r_j_list, vec_r_j_ids_j = calc_vec_r_j_list_ids(dist_vecs_i, dists_i,
                                                        dists_ids_j, cutoff=cutoff)
    
    if not vec_r_j_list:
        # Single atom exception
        return [0., 0., 0.], None, 4*np.pi

    # Calculate r_j and r'
    vec_r_nsum = -np.sum(vec_r_j_list, axis=0)
    vec_r_nsum_norm = np.linalg.norm(vec_r_nsum)

    # Check if the sum of the normalized distance vectors is zero
    if vec_r_nsum_norm <= zero_vec_tolerance:
         vec_r_nsum_norm = zero_vec_tolerance
    
    vec_r_dash = vec_r_nsum / vec_r_nsum_norm
    
    if xyz_filter_func is not None:
        if xyz_filter_func(vec_r_dash):
            return [0., 0., 0.], None, 'xyz_filter'
        
    len_r_j = len(vec_r_j_list)
    hull_vector_ids = vec_r_j_ids_j.copy()
    hull_vector_ids.append(atom_i_id)
    connection_ids = flatten_to_set([vec_r_j_ids_j.copy(), atom_i_id])

    surface_area = 0.0
    
    if len_r_j == 1:  # Bonded to a single atom
        surface_area = 4 * np.pi
    elif len_r_j == 2:  # Bonded to two atoms
        surface_area = 4 * np.pi
    elif len_r_j >= 3:
        hull_vectors = np.vstack((vec_r_j_list, [vec_r_dash]))
        hull_connections = get_hull(hull_vectors)

        if len(hull_connections) > 0:
            try:
                surface_area = np.sum(calc_surface_area(hull_connections, hull_vectors))
            except SystemError:
                for i, hull_connection in enumerate(hull_connections):
                    if len(hull_connection) != 3:
                        raise ValueError(f"Expected three indices in hull_connections at position {i}, but got: {hull_connection}")
                    for j in hull_connection:
                        if j is None:
                            raise ValueError(f"Expected three indices in hull_connections at position {i}, but got: {hull_connection}")
                            
                raise ValueError(f"Unknown error in calc_surface_area with hull_connections: {hull_connections} and hull_vectors: {hull_vectors}")
                                
        else:
            surface_area = 'Error: len(hull_connections) == 0'

    return vec_r_dash, connection_ids, surface_area


def get_mda_distance_dict_filter(pos, cutoff, box, filter_ids):
    dist_pair_ids, pair_distances = mda_distances.self_capped_distance(pos,
                                                                       max_cutoff=cutoff,
                                                                       box=box,
                                                                       return_distances=True)

    dist_dict = OrderedDict()

    for filter_id in sorted(filter_ids):
        dist_dict[filter_id] = [(ids[1], dist) if ids[0] == filter_id else (ids[0], dist)
                                for ids, dist in zip(dist_pair_ids, pair_distances)
                                if filter_id in ids]

    return dist_dict


def calc_vec_r_j_list_ids(dist_vecs, dists, dists_ids_j, cutoff=None):
    vec_r_j_list = []
    vec_r_j_ids_j = []
    for distance_vec, distance, id_j in zip(dist_vecs, dists, dists_ids_j):
        # Filter for atoms within cutoff distance
        if distance == 0.0 or (cutoff is not None and distance > cutoff):
            continue

        # Normalize distance vector
        vec_r_j = distance_vec / distance

        if not vec_r_j_list:
            vec_r_j_list.append(vec_r_j)
            vec_r_j_ids_j.append(id_j)
        else:
            # Check for duplicates of normalized distance vector
            for compare_vec in vec_r_j_list:
                is_same = np.array_equal(compare_vec, vec_r_j)
                if is_same:
                    break

            if not is_same:
                vec_r_j_list.append(vec_r_j)
                vec_r_j_ids_j.append(id_j)

    return (vec_r_j_list, vec_r_j_ids_j)


def get_filter(filter_):
    if filter_ is None:
        return None
    elif isinstance(filter_, str):
        return select_filter(filter_)
    elif callable(filter_):
        return filter_
    else:
        raise ValueError


def select_filter(name):
    if name == 'None':
        return None
    elif name == 'z_filter':
        return z_filter
    else:
        raise NameError("This filter doesn't exist")


def z_filter(vec, inverse=False):

    if vec[2] > 0:
        return inverse is True
    else:
        return inverse is False


def get_hull(hull_vectors):
    """
    Get the convex hull of a set of vectors.
    """
    hull = sp.spatial.ConvexHull(hull_vectors)
    r_id = len(hull_vectors)-1  # Since r' is the last appended one
    hull_connections = [s for s in hull.simplices if r_id in s]

    return np.array(hull_connections)


def flatten_to_set(input):
    """Flatten a list of lists to a set of unique elements."""
    if isinstance(input, Iterable):
        return set().union(*map(flatten_to_set, input))
    else:
        return {input}
    

if __name__ == "__main__":
    pass
