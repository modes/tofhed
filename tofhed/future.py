import numpy as np
from collections import defaultdict
import pandas as pd
from pathlib import Path
from tofhed.utils import read
from tofhed import analysis


def get_connections(path, init_frame_id, setup_dict):
    frames = read(path)
    init_atoms = frames[init_frame_id]
    frame_id_range = range(0, len(frames))

    pos_cache = analysis.PositionCache('ase', data=frames, init_id=init_frame_id)

    pos, tags, pbc, cell, box, chem_sym = analysis.ase_load_init(frames, init_frame_id)

    chem_ids, cat_ids = analysis.split_by_chemicals(init_atoms, setup_dict['chemical_elements'])

    results = {
        **setup_dict,
        'pos_cache': pos_cache,
        'tags': tags,
        'pbc': pbc,
        'cell': cell,
        'box': box,
        'chem_sym': chem_sym,
        'chem_ids': chem_ids,
        'cat_ids': cat_ids,
                 }

    top_surface_atom_ids, bottom_surface_atom_ids = analysis.get_top_bottom_surface_atom_ids(pos, **results)

    top_approach_zone, bottom_approach_zone = analysis.get_approach_zone(pos, top_surface_atom_ids, 
                                                                       bottom_surface_atom_ids)
    approach_zones = (top_approach_zone, bottom_approach_zone)

    results = {
        **results, 
        'top_surface_atom_ids': top_surface_atom_ids, 
        'bottom_surface_atom_ids': bottom_surface_atom_ids,
        'approach_zones': approach_zones,
        'top_approach_zone': top_approach_zone,
        'bottom_approach_zone': bottom_approach_zone,
                 }

    atom_bonded_frames, pair_distances_cache, bond_pairs_cache = analysis.check_for_bonds(frame_id_range, chem_ids.copy(), **results)

    results = {
        **results, 
        'atom_bonded_frames': atom_bonded_frames,
        'pair_distances_cache': pair_distances_cache, 
        'bond_pairs_cache': bond_pairs_cache,
                 }

    bond_status = analysis.split_bond_durations(atom_bonded_frames, min_bond_duration=results['min_bond_duration'])

    bonded_atom_ids = analysis.get_bonded_atom_ids(bond_status, bond_pairs_cache)
    
    results = {
        **results, 
        'bond_status': bond_status,
        'bonded_atom_ids': bonded_atom_ids, 
                 }
    return results


def connectivity(bond_pairs_cache, adsorbed_duration=5):
    connected_for = defaultdict(dict)
    connected_to = defaultdict(dict)
    hopping = defaultdict(dict)
    adsorbed = defaultdict(dict)

    for frame_id, bond_pairs_frame in bond_pairs_cache.items():
        for chem_id, (cat_id, distance) in bond_pairs_frame.items():
            connected_to[frame_id][chem_id] = cat_id
            if frame_id == 0:
                    connected_for[frame_id][chem_id] = 1
                    continue

            if chem_id in connected_for[frame_id - 1]:
                connected_for[frame_id][chem_id] = connected_for[frame_id - 1][chem_id] + 1
                connected_to[frame_id][chem_id] = cat_id
                if cat_id != connected_to[frame_id - 1][chem_id]:
                    hopping[frame_id][chem_id] = (connected_to[frame_id - 1][chem_id], cat_id)

            else:
                connected_for[frame_id][chem_id] = 1
                connected_to[frame_id][chem_id] = cat_id

            if chem_id in adsorbed[frame_id - 1]:
                adsorbed[frame_id][chem_id] = True
            else:
                if connected_for[frame_id][chem_id] >= adsorbed_duration:
                    for i in range(adsorbed_duration):
                        adsorbed[frame_id-i][chem_id] = True
    
    return dict(connected_for), dict(connected_to), dict(hopping), dict(adsorbed)


def connection_df(connected_for, connected_to, hopping, adsorbed, ping):
    columns = ['frame_id', 'chem_id', 'cat_id', 'duration', 'hopping', 'adsorbed', 'ping']
    rows = []
    for frame_id, frame_connected_to in connected_to.items():
        for chem_id, cat_id in frame_connected_to.items():
            hopping_bool, adsorbed_bool, ping_bool = False, False, False
            duration = connected_for[frame_id][chem_id]
            if frame_id in hopping and chem_id in hopping[frame_id]:
                hopping_bool = True
            if frame_id in adsorbed and chem_id in adsorbed[frame_id]:
                adsorbed_bool = True
            if frame_id in ping and chem_id in ping[frame_id]:
                ping_bool = True
            
            rows.append([frame_id,
                         chem_id, 
                         cat_id, 
                         duration, 
                         hopping_bool, 
                         adsorbed_bool,
                         ping_bool])

    return pd.DataFrame(rows, columns=columns)


if __name__ == "__main__":
    pass
