import sqlalchemy as _sa
from sqlalchemy import Column, ForeignKey 
from sqlalchemy import Integer, Float, String
from sqlalchemy import Boolean, DateTime
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.ext.hybrid import hybrid_property
from numpy import float64, int64
from pathlib import Path


Base = declarative_base()


class Experiment(Base):
    __tablename__ = "experiments"
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime())
    setting_id = Column(Integer, ForeignKey("settings.id"))
    system_id = Column(Integer, ForeignKey("systems.id"))
    note = Column(String)
    
    #settings = relationship("Settings", back_populates="experiments")
    #system = relationship("System", back_populates="experiments")
    #provenances = relationship("Provenance", back_populates="experiment")
    #hinuma = relationship("Hinuma", back_populates="experiment")

    def __repr__(self):
        return f"Experiment(id={self.id!r}, created_at={self.created_at!r}, setting_id={self.setting_id!r})"

    
class Settings(Base):
    __tablename__ = "settings"
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime())
    note = Column(String)
    #parameters = relationship("SettingParameter", back_populates="settings")
    #experiments = relationship("Experiment", back_populates="settings")

    def __repr__(self):
        return f"Setting(id={self.id!r}, created_at={self.created_at!r})"


class SettingParameter(Base):
    __tablename__ = "setting_parameters"
    id = Column(Integer, primary_key=True)
    setting_id = Column(Integer, ForeignKey("settings.id"), nullable=False)
    parameter_id = Column(Integer, ForeignKey("parameters.id"), nullable=False)
    #settings = relationship("Settings", back_populates="parameters")
    #parameters = relationship("Parameter")
    value = Column(String)

    def __repr__(self):
        return f"Setting Parameter(id={self.id!r}, setting_id={self.setting_id!r}, parameter_id={self.parameter_id!r}"


class Parameter(Base):
    __tablename__ = "parameters"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String)
    datatype = Column(String, nullable=False)

    def __repr__(self):
        return f"Parameter(id={self.id!r}, name={self.name!r}, datatype={self.datatype!r})"
    

class System(Base):
    __tablename__ = "systems"
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime())
    note = Column(String)
    pbc_x = Column(Boolean)
    pbc_y = Column(Boolean)
    pbc_z = Column(Boolean)
    len_x = Column(Float)
    len_y = Column(Float)
    len_z = Column(Float)
    angle_x = Column(Float)
    angle_y = Column(Float)
    angle_z = Column(Float)
    cell_1_x = Column(Float)
    cell_1_y = Column(Float)
    cell_1_z = Column(Float)
    cell_2_x = Column(Float)
    cell_2_y = Column(Float)
    cell_2_z = Column(Float)
    cell_3_x = Column(Float)
    cell_3_y = Column(Float)
    cell_3_z = Column(Float)
    frame_number_first = Column(Integer)
    frame_number_last = Column(Integer)
    is_sequence = Column(Boolean)
    sequence_id = Column(Integer, ForeignKey("sequences.id"))
    file_id = Column(Integer, ForeignKey("files.id"), nullable=False)
    #files = relationship("File", back_populates="systems")
    #atoms = relationship("Atom", back_populates="system")
    #frames = relationship("Frame", back_populates="system")
    #experiments = relationship("Experiment", back_populates="system")

    def __repr__(self):
        return f"System(id={self.id!r})"


class File(Base):
    __tablename__ = "files"
    id = Column(Integer, primary_key=True)
    path = Column(String, unique=True, nullable=False)
    created_at = Column(DateTime())
    import_positions = Column(Boolean)
    #systems = relationship("System", back_populates="files")

    def __repr__(self):
        return f"File(id={self.id!r}, path={self.path!r})"
    
    
class Frame(Base):
    __tablename__ = "frames"
    id = Column(Integer, primary_key=True)
    frame_number = Column(Integer)
    system_id = Column(Integer, ForeignKey("systems.id"), nullable=False)

    #system = relationship("System", back_populates="frames")
    #provenances = relationship("Provenance", back_populates="frame")
    #positions = relationship("Position", back_populates="frame")
    #atoms = relationship("Atom", secondary="positions", back_populates="frames", viewonly=True)
    #hinuma = relationship("Hinuma", back_populates="frame")

    def __repr__(self):
        return f"Frame(id={self.id!r})"


class Sequence(Base):
    __tablename__ = "sequences"
    id = Column(Integer, primary_key=True)
    
    def __repr__(self):
        return f"Sequence(id={self.id!r})"


class SequenceSystem(Base):
    __tablename__ = "sequence_systems"
    id = Column(Integer, primary_key=True)
    sequence_id = Column(Integer, ForeignKey("sequences.id"), nullable=False)
    system_id = Column(Integer, ForeignKey("systems.id"), nullable=False)
    sequence_order = Column(Integer, nullable=False)

    def __repr__(self):
        return f"SequenceSystem(id={self.id!r}, sequence_id={self.sequence_id!r}, system_id={self.system_id!r})"


class LongExperiment(Base):
    __tablename__ = "long_experiments"
    id = Column(Integer, primary_key=True)
    setting_id = Column(Integer, ForeignKey("settings.id"), nullable=False)
    sequence_id = Column(Integer, ForeignKey("sequences.id"), nullable=False)
    note = Column(String)

    def __repr__(self):
        return f"LongExperiment(id={self.id!r}, setting_id={self.setting_id!r}, sequence_id={self.sequence_id!r})"


class SequenceExperiment(Base):
    __tablename__ = "sequence_experiments"
    id = Column(Integer, primary_key=True)
    long_experiment_id = Column(Integer, ForeignKey("long_experiments.id"), nullable=False)
    sequence_id = Column(Integer, ForeignKey("sequences.id"), nullable=False)
    experiment_id = Column(Integer, ForeignKey("experiments.id"), nullable=False)
    sequence_order = Column(Integer, nullable=False)
    note = Column(String)

    def __repr__(self):
        return f"SequenceExperiment(id={self.id!r}, sequence_id={self.sequence_id!r}, experiment_id={self.experiment_id!r})"


class Atom(Base):
    __tablename__ = "atoms"
    id = Column(Integer, primary_key=True)
    atom_number = Column(Integer)
    atomic_number = Column(Integer, ForeignKey("elements.id"), nullable=False)
    system_id = Column(Integer, ForeignKey("systems.id"), nullable=False)

    #system = relationship("System", back_populates="atoms")
    #element = relationship("Element", foreign_keys=[atomic_number])
    #positions = relationship("Position", back_populates="atom")
    
    #provenances = relationship("Provenance", back_populates="atom")
    #frames = relationship("Frame", secondary="positions", back_populates="atoms", viewonly=True)

    init_tag = Column(Integer)


    def __repr__(self):
        return f"Atom(id={self.id!r}, atomic_number={self.atomic_number!r})"


class Element(Base):
    __tablename__ = "elements"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    chemical_symbol = Column(String)
    covalent_radius_pyykko = Column(Float)
    covalent_radius_pyykko_double = Column(Float)
    covalent_radius_pyykko_triple = Column(Float)
    atomic_radius = Column(Float)
    vdw_radius = Column(Float)
    cpk_color = Column(String)
    jmol_color = Column(String)
    #atoms = relationship("Atom", back_populates="element")
    
    def __repr__(self):
        return f"Element(id={self.id!r}, name={self.name!r}, chemical_symbol={self.chemical_symbol!r})"


class Event(Base):
    __tablename__ = "events"
    id = Column(Integer, primary_key=True)
    experiment_id = Column(Integer, ForeignKey("experiments.id"), nullable=False)
    frame_id = Column(Integer, ForeignKey("frames.id"), nullable=False)
    event_type_id = Column(Integer, ForeignKey("event_types.id"), nullable=False)

    def __repr__(self):
        return f"Event(id={self.id!r}, experiment_id={self.experiment_id!r}, frame_id={self.frame_id!r}, event_type_id={self.event_type_id!r})"


class EventType(Base):
    __tablename__ = "event_types"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    description = Column(String)

    def __repr__(self):
        return f"EventType(id={self.id!r}, name={self.name!r}), description={self.description!r}"


class EventAtom(Base):
    __tablename__ = "event_atoms"
    id = Column(Integer, primary_key=True)
    event_id = Column(Integer, ForeignKey("events.id"), nullable=False)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)

    def __repr__(self):
        return f"EventAtom(id={self.id!r}, event_id={self.event_id!r}, atom_id={self.atom_id!r})"


class Provenance(Base):
    __tablename__ = "provenances"
    id = Column(Integer, primary_key=True)
    experiment_id = Column(Integer, ForeignKey("experiments.id"), nullable=False)
    frame_id = Column(Integer, ForeignKey("frames.id"), nullable=False)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    #experiment = relationship("Experiment", back_populates="provenances")
    #frame = relationship("Frame", back_populates="provenances")
    #atom = relationship("Atom", back_populates="provenances")

    def __repr__(self):
        return f"Provenance(id={self.id!r})"


class Position(Base):
    __tablename__ = "positions"
    id = Column(Integer, primary_key=True)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    frame_id = Column(Integer, ForeignKey("frames.id"), nullable=False)
    import_tag = Column(Integer)
    x = Column(Float)
    y = Column(Float)
    z = Column(Float)
    
    #atom = relationship("Atom", back_populates="positions")
    #frame = relationship("Frame", back_populates="positions")

    def __repr__(self):
        return f"Position(id={self.id!r}, atom_id={self.atom_id!r}, frame_id={self.frame_id!r}"


class BaseType(Base):
    __tablename__ = "base_types"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)

    def __repr__(self):
        return f"BaseType(id={self.id!r}, name={self.name!r})"


class Hinuma(Base):
    __tablename__ = "hinuma"
    id = Column(Integer, primary_key=True)
    experiment_id = Column(Integer, ForeignKey("experiments.id"), nullable=False)
    frame_id = Column(Integer, ForeignKey("frames.id"), nullable=False)
    base_type_id = Column(Integer, ForeignKey("base_types.id"), nullable=False)

    #experiment = relationship("Experiment", back_populates="hinuma")
    #frame = relationship("Frame", back_populates="hinuma")
    #hinuma_atoms = relationship("HinumaAtom", back_populates="hinuma")
    #atoms = relationship("Atom", secondary="hinuma_atoms", viewonly=True)

    def __repr__(self):
        return f"Hinuma(id={self.id!r}, frame_id={self.frame_id!r})"


class HinumaAtom(Base):
    __tablename__ = "hinuma_atoms"
    id = Column(Integer, primary_key=True)
    hinuma_id = Column(Integer, ForeignKey("hinuma.id"), nullable=False)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    hinuma_vec_x = Column(Float)
    hinuma_vec_y = Column(Float)
    hinuma_vec_z = Column(Float)
    solid_angle = Column(Float)
#    _solid_angle = Column('solid_angle', Float)
    note = Column(String)

#    @hybrid_property
#    def solid_angle(self):
#        return self._solid_angle
#
#    @solid_angle.setter
#    def solid_angle(self, value):
#        if value is None or isinstance(value, str):
#            print(f'Not a float: {value}, {type(value)}')
#            self._solid_angle = None
#        else:
#            self._solid_angle = value


    #hinuma = relationship("Hinuma", foreign_keys=[hinuma_id], back_populates="hinuma_atoms")
    #atom = relationship("Atom", foreign_keys=[atom_id])

    def __repr__(self):
        return f"HinumaAtom(id={self.id!r}, atom_id={self.atom_id!r}, hinuma_id={self.hinuma_id!r})"
    

class HinumaConnection(Base):
    __tablename__ = "hinuma_connections"
    id = Column(Integer, primary_key=True)
    hinuma_atom_id = Column(Integer, ForeignKey("hinuma_atoms.id"), nullable=False)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    connecting_atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)

    #hinuma_atom = relationship("HinumaAtom", foreign_keys=[hinuma_atom_id])
    #atom = relationship("Atom", foreign_keys=[atom_id])
    #connecting_atom = relationship("Atom", foreign_keys=[connecting_atom_id])

    def __repr__(self):
        return f"HinumaConnection(id={self.id!r}, atom_id={self.atom_id!r}, connecting_atom_id={self.connecting_atom_id!r})"


class SurfaceGroup(Base):
    __tablename__ = "surface_groups"
    id = Column(Integer, primary_key=True)
    surface_type_id = Column(Integer, ForeignKey("surface_types.id"), nullable=False)
    experiment_id = Column(Integer, ForeignKey("experiments.id"), nullable=False)
    hinuma_id = Column(Integer, ForeignKey("hinuma.id"))
    solid_angle_limit = Column(Float)

    # hinuma = relationship("Hinuma", foreign_keys=[hinuma_id])
    #experiment = relationship("Experiment", foreign_keys=[experiment_id])
    #surface_type = relationship("SurfaceType", back_populates="surface_groups")
    #surfaces = relationship("Surface", back_populates="surface_group")

    def __repr__(self):
        return f"SurfaceGroup(id={self.id!r}, solid_angle_limit={self.solid_angle_limit!r})"


class Surface(Base):
    __tablename__ = "surfaces"
    id = Column(Integer, primary_key=True)
    surface_group_id = Column(Integer, ForeignKey("surface_groups.id"), nullable=False)

    #surface_group = relationship("SurfaceGroup", back_populates="surfaces")
    #surface_atoms = relationship("SurfaceAtom", back_populates="surface")
    #surface_parameters = relationship("SurfaceParameter", back_populates="surface")

    def __repr__(self):
        return f"Surface(id={self.id!r}, SurfaceGroup={self.surface_group_id!r})"


class SurfaceAtom(Base):
    __tablename__ = "surface_atoms"
    id = Column(Integer, primary_key=True)
    surface_id = Column(Integer, ForeignKey("surfaces.id"), nullable=False)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)

    #surface = relationship("Surface", back_populates="surface_atoms")

    def __repr__(self):
        return f"SurfaceAtom(id={self.id!r}, surface_id={self.surface_id!r})"


class SurfaceType(Base):
    __tablename__ = "surface_types"
    id = Column(Integer, primary_key=True)
    name = Column(String)

    #surface_groups = relationship("SurfaceGroup", back_populates="surface_type")

    def __repr__(self):
        return f"SurfaceType(id={self.id!r}, name={self.name!r})"


class SurfaceParameter(Base):
    __tablename__ = "surface_parameters"
    id = Column(Integer, primary_key=True)
    surface_id = Column(Integer, ForeignKey("surfaces.id"), nullable=False)
    parameter_id = Column(Integer, ForeignKey("parameters.id"), nullable=False)    
    value = Column(String) 
    
    #surface = relationship("Surface", back_populates="surface_parameters")
    #parameter = relationship("Parameter", foreign_keys=[parameter_id])

    def __repr__(self):
        return f"SurfaceParameter(id={self.id!r}, surface_id={self.surface_id!r}, parameter_id={self.parameter_id!r}"    


class Pair(Base):
    __tablename__ = "pairs"
    id = Column(Integer, primary_key=True)
    atom_id_1 = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    atom_id_2 = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    
    @property
    def pair(self):
        return tuple(sorted([self.atom_id_1, self.atom_id_2]))
    
    def __repr__(self):
        return f"Pair(id={self.id!r}, atom_id_1={self.atom_id_1!r}, atom_id_2={self.atom_id_2!r})"


class Distance(Base):
    __tablename__ = "distances"
    id = Column(Integer, primary_key=True)
    pair_id = Column(Integer, ForeignKey("pairs.id"), nullable=False)
    frame_id = Column(Integer, ForeignKey("frames.id"), nullable=False)
    distance = Column(Float)

    #pair = relationship("Pair", foreign_keys=[pair_id])

    def __repr__(self):
        return f"Distance(id={self.id!r}, name={self.pair_id!r}, distance={self.distance!r})"


class Connection(Base):
    __tablename__ = "connections"
    id = Column(Integer, primary_key=True)
    pair_id = Column(Integer, ForeignKey("pairs.id"), nullable=False)
    frame_id = Column(Integer, ForeignKey("frames.id"), nullable=False)
    experiment_id = Column(Integer, ForeignKey("experiments.id"), nullable=False)
    connection_type_id = Column(Integer, ForeignKey("connection_types.id"), nullable=False)
    
    def __repr__(self):
        return f"Connection(id={self.id!r}, pair_id={self.pair_id!r}, frame_id={self.frame_id!r}, connection_type_id={self.connection_type_id!r})"


class ConnectionType(Base):
    __tablename__ = "connection_types"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String)

    def __repr__(self):
        return f"ConnectionType(id={self.id!r}, name={self.name!r})"


class Property(Base):
    __tablename__ = "properties"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    display_name = Column(String)
    description = Column(String)
    datatype = Column(String, nullable=False)

    def __repr__(self):
        return f"Property(id={self.id!r}, name={self.name!r})"
    

class ProvenanceProperty(Base):
    __tablename__ = "provenance_properties"
    id = Column(Integer, primary_key=True)
    provenance_id = Column(Integer, ForeignKey("provenances.id"), nullable=False)
    property_id = Column(Integer, ForeignKey("properties.id"), nullable=False)
    value = Column(String)

    def __repr__(self):
        return f"ProvenanceProperty(id={self.id!r}, name={self.name!r})"


class SurfaceDistance(Base):
    __tablename__ = "surface_distances"
    id = Column(Integer, primary_key=True)
    surface_id = Column(Integer, ForeignKey("surfaces.id"), nullable=False)
    frame_id = Column(Integer, ForeignKey("frames.id"), nullable=False)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    distance = Column(Float)

    def __repr__(self):
        return f"SurfaceDistance(id={self.id!r}, name={self.name!r})"


class Fragment(Base):
    __tablename__ = "fragments"
    id = Column(Integer, primary_key=True)
    fragment_type_id = Column(Integer, ForeignKey("fragment_types.id"), nullable=False)

    def __repr__(self):
        return f"Fragment(id={self.id!r}, fragment_type_id={self.fragment_type_id!r})"


class FragmentType(Base):
    __tablename__ = "fragment_types"
    id = Column(Integer, primary_key=True)
    smiles = Column(String, unique=True)
    name = Column(String, unique=True)
    note = Column(String)

    def __repr__(self):
        return f"FragmentType(id={self.id!r}, smiles={self.smiles!r}, name={self.name!r})"


class FragmentAtom(Base):
    __tablename__ = "fragment_atoms"
    id = Column(Integer, primary_key=True)
    fragment_id = Column(Integer, ForeignKey("fragments.id"), nullable=False)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)

    def __repr__(self):
        return f"FragmentAtom(id={self.id!r}, fragment_id={self.fragment_id!r}, atom_id={self.atom_id!r})"


class AtomTag(Base):
    __tablename__ = "atom_tags"
    id = Column(Integer, primary_key=True)
    atom_id = Column(Integer, ForeignKey("atoms.id"), nullable=False)
    experiment_id = Column(Integer, ForeignKey("experiments.id"), nullable=False)
    property_id = Column(Integer, ForeignKey("properties.id"), nullable=False)
    value = Column(String)
    
    def __repr__(self):
        return f"AtomTag(id={self.id!r}, atom_id={self.atom_id!r}, property_id={self.property_id!r})"


def check_existance(engine_path, overwrite):
    if engine_path is None:
        engine_path = "sqlite+pysqlite:///:memory:?check_same_thread=False"
    elif Path(engine_path).exists():
        if overwrite:
            Path(engine_path).unlink()
        else:
            raise FileExistsError(f"Database file already exists: {engine_path}")
    
    return engine_path


def create_db(engine_path=None, echo=False, overwrite=False):
    """
    Create the database and tables.

    Parameters
    ----------
    engine_path : str, optional
        The path to the database. If not provided, an in-memory database will be created.
    echo : bool, optional
        If True, the SQL statements will be printed to the console.

    Returns
    -------
    engine : sqlalchemy.engine.Engine
        The database engine.

    Notes
    -----
    The database engine is created using the `sqlite+pysqlite` dialect. If you want to use a different dialect, you can
    pass the engine path as a string, e.g. ``engine_path='postgresql+psycopg2://user:password@host:port/database'``.
    
    Tested with:
    - sqlite+pysqlite
    - postgresql+psycopg2

    """
    if engine_path is not None:
        engine_path = str(engine_path)

    engine_path = check_existance(engine_path, overwrite)
        
    engine = _sa.create_engine(engine_path, echo=echo)
    if not database_exists(engine.url):
        create_database(engine.url)

    if engine.dialect.name == 'postgresql':
        from psycopg2.extensions import register_adapter, AsIs

        def addapt_numpy_float64(numpy_float64):
            return AsIs(numpy_float64)

        def addapt_numpy_int64(numpy_int64):
            return AsIs(numpy_int64)
        
        register_adapter(float64, addapt_numpy_float64)
        register_adapter(int64, addapt_numpy_int64)

    metadata = _sa.MetaData()

    Base.metadata.create_all(engine)

    return engine


if __name__ == "__main__":
    pass
