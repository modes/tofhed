from tofhed.db.schema import *
import mendeleev


def initialize_parameters(session):
    """Initializes the parameters table with default values.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    
    """
    parameters = [Parameter(
            name="hinuma_cutoff",
            description="""The distance which atoms to consider within the hinuma algorithm""",
            datatype="Float"),

        Parameter(
            name="mic",
            description="""Boolean if the Minimum Image Convention should be applied""",
            datatype="Boolean"),

        Parameter(
            name="approach_cutoff",
            description="""The distance from the surface which acts as the approach zone""",
            datatype="Float"),

        Parameter(
            name="bond_cutoff",
            description="""The minimal distance to filter for bond-distance calculations""",
            datatype="Float"),

        Parameter(
            name="zero_vec_tolerance",
            description="""The tolerance for the hinuma calculation to not consider a vector""",
            datatype="Float"),

        Parameter(
            name="normal_vec_x",
            description="""X Component of the normal vector to the plane""",
            datatype="Float"),

        Parameter(
            name="normal_vec_y",
            description="""Y Component of the normal vector to the plane""",
            datatype="Float"),

        Parameter(
            name="normal_vec_z",
            description="""Z Component of the normal vector to the plane""",
            datatype="Float"),

        Parameter(
            name="position_id_max",
            description="""Position ID of the furthest atom to the plane in positive direction""",
            datatype="Integer"),

        Parameter(
            name="position_id_min",
            description="""Position ID of the furthest atom to the plane in negative direction""",
            datatype="Integer"),

        Parameter(
            name="fudge_factor",
            description="""Factor which is multiplied onto the sum of radii to determine the bond cutoff""",
            datatype="Float"),

        Parameter(
            name="init_frame_number",
            description="""Initial frame_number for the import of structural properties""",
            datatype="Integer"),

        Parameter(
            name="init_frame_id",
            description="""Initial frame_id for the import of structural properties""",
            datatype="Integer"),

        Parameter(
            name="xyz_filter",
            description="""Hinuma filter name to filter out surfaces facing a direction""",
            datatype="String"),

        Parameter(
            name="solid_angle_limit",
            description="""Surface area or solid angle to be treated as a surface atom. See Hinuma Algorithm""",
            datatype="Float"),

        Parameter(
            name="adsorbed_duration",
            description="""The duration an atom has to be connected to be considered as adsorbed""",
            datatype="Integer"),
        
        Parameter(
            name="base_type_algorithm",
            description="""The algorithm to determine the base type of an atom""",
            datatype="String"),

        Parameter(
            name="max_molecule_size",
            description="""The maximum size of a molecule to be considered""",
            datatype="Integer"),

        Parameter(
            name="chemical_elements",
            description="""The chemical elements to be considered in string format separated by a comma""",
            datatype="String"),
        
        Parameter(
            name="chemical_tag",
            description="""The chemical tag to be considered for the base type algorithm""",
            datatype="Integer"),
        
        Parameter(
            name="duration_cutoff",
            description="""The duration an atom has to be connected to be considered as adsorbed""",
            datatype="Integer"),

        Parameter(
            name="dynamic_cutoff",
            description="""The dynamic cutoff uses Pyykko distances to further filter out atoms be used for the Hinuma calculation""",
            datatype="Boolean"),

        Parameter(
            name="pyykko_fudge",
            description="""Factor which is multiplied onto the sum of radii to filter out atoms for the dynamic cutoff""",
            datatype="Float"),

            ]

    session.add_all(parameters)
    session.commit()


def initialize_properties(session):
    """Initializes the properties table with default values.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    """

    properties = [
        Property(
            name="in_approach_to",
            display_name="In approach to",
            description="Tag of atoms within the approach distance. Value is the surface_id.",
            datatype="Integer"), 

        Property(
            name="init_base_type",
            display_name="Initialize Base Type",
            description="The initial base type of the atom. Currently only 'chemical' and 'catalyst' are supported.",
            datatype="Integer"),
    ]

    session.add_all(properties)
    session.commit()


def initialize_connection_types(session):
    """Initializes the connection_types table with default values.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    
    """

    connection_types = [
        ConnectionType(
            name="pyykko",
            description="Connected by a covalent bond according to Pyykko single bond radii.",
            )

    ]

    session.add_all(connection_types)
    session.commit()


def initialize_base_types(session):
    """Initializes the base_types table with default values.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    
    """

    base_types = [
        BaseType(name='catalyst'),
        BaseType(name='chemical'),
        BaseType(name='all'),
    ]
    session.add_all(base_types)
    session.commit()


def initialize_event_types(session):
    """
    Initializes the event_types table with default values.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    """

    event_types = [
        EventType(
            name='adsorption',
            description='An atom has been adsorbed on a surface',),

        EventType(
            name='desorption',
            description='An atom has been desorbed from a surface',),

        EventType(
            name='ping-in',
            description='An atom has pinged-in on a surface',),

        EventType(
            name='ping-out',
            description='An atom has pinged-out off a surface',),

        EventType(
            name='hopping',  # surface diffusion
            description='An atom has changed a connecting atom on the surface',), 

        EventType(
            name='migration',  # bulk diffusion
            description='A surface atom has changed a connecting atom in the bulk',),

        EventType(
            name='uptake',
            description='An atom has been taken up by a catalyst',),

        EventType(
            name='release',
            description='An atom has been released from a catalyst',),
    ]

    session.add_all(event_types)
    session.commit()


def initialize_elements(session):
    """
    Initializes the elements table with default values from the mendeleev package.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.
    
    """
    
    elements = []
    for element in mendeleev.elements.get_all_elements():
        elements.append(Element(
            id=element.atomic_number,
            name=element.name,
            covalent_radius_pyykko=element.covalent_radius_pyykko / 100,
            # covalent_radius_pyykko_double': element.covalent_radius_pyykko_double*0.01,
            # 'covalent_radius_pyykko_triple': element.covalent_radius_pyykko_triple*0.01,
            # 'atomic_radius': element.atomic_radius*0.01,
            # 'vdw_radius': element.vdw_radius*0.01,
            cpk_color=element.cpk_color,
            jmol_color=element.jmol_color,
            chemical_symbol=element.symbol))
    session.add_all(elements)
    session.commit()


def initialize_surface_types(session):
    """
    Initializes the surface_types table with default values.

    Parameters
    ----------
    session : sqlalchemy.orm.Session
        The session to the database.

    """

    surface_types = [
        SurfaceType(name='hinuma'),
        SurfaceType(name='simple_2d'),
        SurfaceType(name='plane_2d'),
                     ]

    session.add_all(surface_types)
    session.commit()


if __name__ == "__main__":
    pass
