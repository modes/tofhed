# tofhed

> **WARNING**: This code is in active developement!

'tofhed' is a tool for handling heterogeneous atom trajectories. Its functionalities include:
- Importing atom trajectories (from xyz, extxyz, cif, etc.)
- Building a SQL database for data collection and subsequent analysis (only SQLite and Postgres have been tested)
- Analysing frames using the Hinuma algorithm
- Identifying surface atoms and their connectivities
- Finding connected atoms based on their Pyykko-Distance
- Detecting events such as pinging, adsorption, and desorption


## Installation

### Prerequisites

#### Python (Version 3.8 - 3.11)
Ensure Python (within version 3.8 to 3.11) is installed on your system. For an isolated and conflict-free setup, we advocate using a virtual environment.

Link: [Setting up a Virtual Python Environment](https://docs.python.org/3/library/venv.html)

#### Git
Since 'tofhed' is currently in its developmental alpha stage, it isn't available on PyPI. Thus, Git is essential for the installation process.

Link: [Git Installation Guide](https://www.atlassian.com/git/tutorials/install-git)

### Installation Steps

Installing 'tofhed' is a straightforward process, be it through the terminal or Jupyter Lab. The tool takes care of all required dependencies during installation.

#### Option 1: Terminal Installation
It's prudent (though not mandatory) to activate a virtual environment prior to the installation. If you're unfamiliar with setting up a virtual environment, please revisit the Python prerequisites above. With your virtual environment active, enter the following command:

```bash
pip install git+https://git.rwth-aachen.de/modes/tofhed.git
```

While a virtual environment prevents potential package conflicts, choosing not to use one will install 'tofhed' and its dependencies system-wide.

#### Option 2: Jupyter Lab Installation
Ensure your desired virtual environment is selected within Jupyter Lab. Then, proceed with the installation command highlighted in the terminal section.

Link: [Guide on Using Jupyter Notebook in a Virtual Environment](https://www.geeksforgeeks.org/using-jupyter-notebook-in-virtual-environment/)



```python
%pip install git+https://git.rwth-aachen.de/modes/tofhed.git
```

## Quickstart

- [Quickstart Guide](quickstart.ipynb)
