import datetime
# import sphinx_rtd_theme
import os
import sys
sys.path.insert(0, os.path.abspath('..'))
sys.path.append(os.path.abspath("./_pygments"))

#pygments_style = 'style.MonokaiStyle'

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'tofhed'
copyright = f'{datetime.date.today().year}, MODES, Thomas Bakaj'
author = 'MODES, Thomas Bakaj'
release = '0.1.2'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    # 'myst_parser',
    'sphinx_rtd_theme',
    'sphinx.ext.autodoc',
    'sphinx.ext.extlinks',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    'sphinx.ext.intersphinx',
    'sphinx.ext.coverage',
    # 'sphinx.ext.autosectionlabel',
    # 'sphinx.ext.autosummary',
    'sphinx_copybutton',
    # 'myst_nb',
    'nbsphinx',
    'nbsphinx_link',
    ]

# Prefix document path to section labels, to use:
# `path/to/file:heading` instead of just `heading`
autosectionlabel_prefix_document = True

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

language = 'en'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

myst_enable_extensions = [
    "amsmath",
    "colon_fence",
    "deflist",
    "dollarmath",
    "fieldlist",
    "html_admonition",
    "html_image",
    "replacements",
    "smartquotes",
    "strikethrough",
    "substitution",
    "tasklist",
    ]

nbsphinx_execute = 'never'
# nb_execution_mode = "off"  # for myst_nb

html_theme = "sphinx_rtd_theme"
html_title = "tofhed Documentation"
html_static_path = ['_static']
html_logo = "_static/logo_bg-w.svg"
html_theme_options = {
    'logo_only': False,
    'display_version': True,
    }
html_favicon = '_static/favicon-196x196.png'
html_style = 'style.css'
html_last_updated_fmt = '%a, %d %b %Y %H:%M:%S'

latex_elements = {'papersize': 'a4paper'}
latex_show_urls = 'inline'
latex_show_pagerefs = True
