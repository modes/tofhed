.. module:: tofhed.analysis

tofhed
======

Thats an example fot the class called :class:`PyykkoDistances`:

.. autoclass:: PyykkoDistances
   :members:

This is a test.

.. note::

   The ``position`` and ``momentum`` attributes refer to mutable
   objects, so in some cases, you may want to use
   ``a1.position.copy()`` in order to avoid changing the position of
   ``a1`` by accident.

.. autofunction:: get_surface_atom_ids_mda

.. seealso::

   :mod:`tofhed.db`:
     More information about how to use collections of atoms.
