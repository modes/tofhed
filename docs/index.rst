.. autodoc documentation master file, created by
   sphinx-quickstart on Sun Jan  8 23:32:22 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Contents
========

.. toctree::
    :glob:
    :maxdepth: 1

    content/installation
    content/quickstart
    content/about
    **/*
    api
    


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
