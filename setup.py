from setuptools import setup, find_packages

with open('requirements.in') as f:
    required = f.read().splitlines()

setup(
    name="tofhed",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    packages=find_packages(),
    # metadata for upload to PyPI
    author="Thomas Bakaj",
    author_email="thomas.bakaj@rwth-aachen.de",
    description="This is a Python tool called tofhed",
    license="MIT",
    keywords="python tool tofhed",
    url="https://git.rwth-aachen.de/modes/tofhed",  # project home page
    install_requires=required,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers, Scientists",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
    ],
)
